CREATE DATABASE  IF NOT EXISTS `dmdb2014` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dmdb2014`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: dmdb2014
-- ------------------------------------------------------
-- Server version	5.6.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `all_cases`
--

DROP TABLE IF EXISTS `all_cases`;
/*!50001 DROP VIEW IF EXISTS `all_cases`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `all_cases` (
  `CID` tinyint NOT NULL,
  `DateTime` tinyint NOT NULL,
  `Location` tinyint NOT NULL,
  `Description` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `Summary` tinyint NOT NULL,
  `ConCatName` tinyint NOT NULL,
  `ConMainCat` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `belongs`
--

DROP TABLE IF EXISTS `belongs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `belongs` (
  `Case_CID` int(10) unsigned NOT NULL DEFAULT '0',
  `Category_CatName` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`Case_CID`,`Category_CatName`),
  KEY `fk_Belongs_Category1_idx` (`Category_CatName`),
  KEY `fk_Belongs_Case1_idx` (`Case_CID`),
  CONSTRAINT `fk_Belongs_Case1` FOREIGN KEY (`Case_CID`) REFERENCES `case` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Belongs_Category1` FOREIGN KEY (`Category_CatName`) REFERENCES `category` (`CatName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `belongs`
--

LOCK TABLES `belongs` WRITE;
/*!40000 ALTER TABLE `belongs` DISABLE KEYS */;
INSERT INTO `belongs` VALUES (1,'Kinslaying'),(2,'Kinslaying'),(2,'Oathbreaking'),(2,'Rebellion'),(2,'Regicide'),(3,'Kinslaying'),(4,'Kinslaying'),(5,'Knight\'s Oath'),(5,'Oathbreaking'),(5,'Regicide'),(6,'Incest'),(7,'Incest'),(7,'Rape'),(8,'Guest Right violation'),(8,'Kinslaying'),(8,'Regicide'),(9,'Guest Right violation'),(10,'Kinslaying'),(10,'Regicide'),(11,'Oathbreaking'),(12,'Theft');
/*!40000 ALTER TABLE `belongs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case`
--

DROP TABLE IF EXISTS `case`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case` (
  `CID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateTime` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `Location` varchar(90) DEFAULT NULL,
  `Description` text,
  `Status` tinyint(1) NOT NULL,
  `Summary` varchar(90) NOT NULL,
  PRIMARY KEY (`CID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case`
--

LOCK TABLES `case` WRITE;
/*!40000 ALTER TABLE `case` DISABLE KEYS */;
INSERT INTO `case` VALUES (1,'2014-04-14 17:47:24','Red Keep','',1,'King Aegon II Targaryen, who fed his sister Rhaenyra Targaryen to his dragon.'),(2,'1999-12-11 23:12:00','King\'s Landing','Robert Baratheon killed Rhaegar Targaryen: Considered Kinslaying since Robert\'s grandmother was Rhaegar\'s great-aunt. Full Story:                                                  The war was sparked when Prince Rhaegar Targaryen allegedly kidnapped Lyanna Stark. When Lyanna\'s father and eldest brother protested this act to King Aerys II Targaryen, the Mad King had them both killed in front of the entire Royal Court. Aerys then called for the heads of Lyanna\'s betrothed, Robert Baratheon, and her second brother Eddard Stark, who were under the care of Jon Arryn. Lord Arryn raised his banners in rebellion against these unjust acts and sent Robert and Eddard back to their lands to rally their men.\r\n\r\nHouse Tully joined Robert\'s side after Eddard and Jon married Catelyn and Lysa Tully, respectively, whilst House Tyrell and House Martell supported the Targaryens. While House Tully did support Robert, the always fractious lords of the Riverlands were heavily divided in the war, and many stayed loyal to the Targaryens. House Lannister sat out most of the war, only entering - on Robert\'s side - after Robert killed Rhaegar and his victory was a foregone conclusion. It is never stated which side House Greyjoy was on, but it is possible that they did not fight at all and remained neutral, saving their strength for their failed attempt to secede outright from the realm a few years later in the Greyjoy Rebellion.\r\n\r\nThe rebellion climaxed with the Battle of the Trident, when the rebel armies destroyed the royalist host and killed Prince Rhaegar. Lannister forces, apparently having decided to support the king, arrived at King\'s Landing to help defend the capital, but then turned on the Targaryens, brutally sacking the city and killing King Aerys II and Rhaegar\'s children. Aerys\' other heirs, Viserys and Daenerys Targaryen, were spirited to safety across the sea by Targaryen loyalists before the garrison of Dragonstone (where the two children were being kept for their safety) could hand them over to Robert Baratheon. House Tyrell, whose main force had been held up in a lengthy siege at Storm\'s End, proclaimed loyalty to Robert soon after the sacking of King\'s Landing when Eddard Stark arrived with an army to relieve the siege.\r\n\r\nIn the aftermath of the war it was discovered that Lyanna Stark had died, so Robert married Cersei Lannister in thanks for her father delivering him victory and to secure an alliance between the two houses.\r\n\r\nElia Martell\'s brutal murder by the Lannisters during the Sack of King\'s Landing earned them the lasting enmity of the Martells. As a result of Elia\'s unnecessary and easily avoidable murder, Dorne became a hotbed of pro-Targaryen sentiment and it took lengthy negotiations on the part of Jon Arryn for them to grudgingly swear fealty to Robert.',0,'Rober\'s Rebellion'),(3,'1999-12-11 23:12:00','Clegane\'s Keep','',0,'Ser Gregor Clegane, suspected to have murdered his young sister as well as his father.'),(4,'1988-02-21 23:12:00','Craster\'s Keep, Beyond the Wall','',0,'Craster, who sacrifices his male offspring to the White Walkers.'),(5,'1977-12-11 23:12:00','King\'s Landing','Ser Jaime Lannister, perhaps the most notable example of a kingslayer, who killed Aerys II Targaryen during the Sack of King\'s Landing. Jaime\'s actions were considered especially heinous, as he was in fact a member of Aerys\' own Kingsguard, and took a holy vow to lay down his life in defence of his king Jaime killed Aerys in order to foil his scheme to destroy King\'s Landing, but since he never revealed it to anyone (except recently to Brienne) - everyone assumed he killed Aerys so the Lannisters would seize the throne. It is unknown whether Jaime would still bear the stigma of kingslayer if he revealed his real motive.',0,'Ser Jaime Lannister killed Aerys II Targaryen'),(6,'1988-02-11 23:12:00','','Queen Cersei and her brother Ser Jaime Lannister have carried an illicit romance since childhood. Cersei\'s children: Joffrey, Myrcella, and Tommen, are all born of their affair. Similar to the Targaryens, this incestuous bloodline has apparently produced severe mental health defects in Joffrey. While he doesn\'t hear voices or see hallucinations of things that aren\'t real, Joffrey is a sadistic, megalomaniacal sociopath. Cersei and Tyrion explicitly discuss that the Targaryens experienced similar mental and behavioral problems after generations of incestuous inbreeding. Myrcella and Tommen, however, beat the odds and possess no (biological) mental health problems.',0,'Incest of Queen Cersei & her brother Ser jaime Lannister'),(7,'1988-12-12 01:32:00','Craster\'s Keep, Beyond the Wall','',0,'Craster takes his daughters as wives and does the same on the daughters he sires on them.'),(8,'1996-06-06 16:06:00','The Twins, the Riverlands','The Red Wedding is a massacre during the War of the Five Kings arranged by Lord Walder Frey as revenge against King Robb Stark for breaking the marriage pact between House Stark and House Frey. During the massacre, King Robb, his wife, Queen Talisa, his mother, Lady Catelyn, and most of his bannermen and men-at-arms are murdered following the marriage feast and bedding of Edmure Tully and Roslin Frey.\r\n\r\nPrelude\r\n\"My honored guests, be welcome within my walls and at my table. I extend to you my hospitality and protection in the light of the Seven.\"\r\nLord Walder to King Robb and his entourage\r\nKing Robb was lulled into a sense of security by Walder Frey because he had extended guest right to the Starks - formally eating salt and bread from the same bowl as his guests. To break guest right is to break all the laws of gods and men, thus while Robb and Catelyn were always wary of Walder Frey\'s intentions, they never thought that even such a despicable man as he would sink so low as to break such a sacred pact.\r\n\r\nThe betrayal was scheduled to occur after the formal ceremony and the bedding, with Edmure and Roslin safely away in another part of the castle to consummate their marriage.\r\n\r\nThe door of the great hall is closed and barred by Black Walder. Meanwhile, Roose Bolton and his men were to position themselves around the hall, secretly armed and armored. The signal for massacre to begin was for the musicians to play an instrumental version of The Rains of Castamere.\r\n\r\nThe Massacre\r\n\"Your Grace, I feel I\'ve been remiss in my duties. I\'ve given you meat and wine and music, but I haven\'t shown you the hospitality you deserve. My King has married and I owe my new Queen a wedding gift.\"\r\nÂLord Walder signals for the massacre to begin.[src]\r\nTalisa Stabbed\r\nTalisa is stabbed by Lame Lothar.\r\nAt Walder Frey\'s signal, the Northern leadership with Robb Stark gathered in the main hall is fired upon from the balcony by a group of assassins armed with crossbows who were disguised as musicians during the wedding. After the first volley the Northern guests are attacked in the main hall by armed Frey and Bolton men, as the crossbowmen continue to pick off survivors. Some of the Northerners killed by the Freys fall dead into their own meals, which the same Frey men had set there earlier as their guests. Meanwhile, Frey and Bolton men turn on the other Northern soldiers in the camps who had been heavily drinking during the celebrations, taking them completely by surprise.\r\n\r\nRoose kills Robb S3 Ep9\r\nRoose Bolton murders Robb Stark.\r\nRobb\'s direwolf had been penned up and not allowed into the castle, as it would have defended Robb during the betrayal. Instead, half a dozen Frey crossbowmen shoot Grey Wind dead while he is trapped inside a pen.\r\n\r\nRobb, while wounded with several crossbow bolts, is still able to get back onto his feet. Catelyn Stark desperately takes Walder Frey\'s eighth wife, Joyeuse Erenford, hostage with a knife to the throat, but Walder glibly refuses, claiming he can simply find another one.\r\n\r\nCatelyn dies\r\nCatelyn is killed by \"Black\" Walder Frey.\r\nAt this point, Roose Bolton personally kills Robb by stabbing him through the heart while saying \"the Lannisters send their regards\". True to her word, Catelyn slits Joyeuse\'s throat and lets out a wail of grief. Catelyn then remains silent, staring at Robb\'s corpse in shock and utter despair, not reacting as her own throat is slit from behind by Black Walder Frey. Her throat was cut almost to the bone.\r\n\r\nRobb Wind\r\n\"King in the North! King in the North! Here he comes, the King in the North!\"\r\nAfterward, as the massacre of the Stark army encamped outside the Twins rages on, the Freys horrifically desecrate Robb\'s corpse by decapitating it and sewing the head of his direwolf Grey Wind in its place, then parade it around the keep atop a horse; a final insult against the King in the North.\r\n\r\nCatelyn Stark\'s corpse was also desecrated: in cruel mockery of traditional House Tully funeral customs, which involve cremating a body on a burning boat set adrift in the Trident River, the Freys unceremoniously flung Lady Catelyn\'s corpse from the battlements of the Twins, throwing it into the river to rot as if it was trash.\r\n\r\nAftermath\r\n\"The Northerners will never forget.\"\r\nNot only was Robb Stark himself killed in the betrayal, but the entire Northern army that Robb Stark led to southern Westeros was also destroyed - save only for those forces of House Karstark which had earlier abandoned Robb to return home, and the forces of House Bolton which turn on the other Northern Houses. Thus the Red Wedding ends the conflict between House Stark and House Lannister in a decisive victory for King Joffrey Baratheon and House Lannister. The War of the Five Kings continues, however, as Balon Greyjoy still fights for the Iron Islands\' independence and to hold on to his conquest in the North, while Stannis Baratheon continues to dispute Joffrey Baratheon\'s right to the Iron Throne.\r\n\r\nLord Walder has his men capture Edmure Tully out of his marriage bed alive. As Riverrun has not yet fallen to Lannister forces Lord Edmure is a valuable hostage to hopefully negotiate its surrender in the near future. As a reward for betraying Robb Stark, Roose Bolton is made Warden of the North, while Walder Frey is given Riverrun.\r\n\r\nBrynden Tully had been present for the wedding but fortuitously, he had left the keep to relieve himself on a tree outside before the massacre in the main hall began. He then managed to fight his way out of the assault on the camps and slip away from the Twins during the confusion of the night-time ambush. Roose Bolton ruefully notes to Walder Frey the next day that he has escaped. Lord Walder is dismissive and says he won\'t get far, but Bolton is clearly concerned that the Blackfish will manage to reach the safety of Riverrun before he can be found.\r\n\r\nGreatjon Umber, one of the most powerful and loyal bannermen of House Stark, is not present at the Twins for the wedding, making him one of the few bannermen of House Stark that remains alive and free.',0,'The Red Wedding'),(9,'1988-12-11 23:12:00','Winterfell','Although few are aware of what really happened, Jaime Lannister violated guest right when he attempted to kill Bran Stark while he was a guest of Bran\'s father, Eddard Stark, at Winterfell. Jaime attempted to kill a member of his host\'s family, however, not the host himself.',1,'Ser Jaime Lannister attempts to kill Bran Stark'),(10,'1988-12-11 23:12:00','King\'s Landing','Queen Cersei Lannister, along with her cousin Lancel, conspired to bring about the death of her husband King Robert Baratheon. On Cersei\'s instruction Lancel replaced the wine in the king\'s flask with a stronger, fortified vintage, causing Robert to quickly descend into an intoxicated state, dulling his reflexes, and ultimately leading him to suffer a fatal wound upon a boar\'s tusk.[3]',1,'Queen Cersei Lannister conspires the death of her Husband.'),(11,'1988-12-11 23:12:00','The Wall','Will, a ranger of the Night\'s Watch, who fled south after witnessing White Walkers kill his sworn brothers Gared and Waymar Royce. Captured by men of House Stark and executed by Lord Eddard Stark, Warden of the North.',0,'Will flees from the Night\'s Watch'),(12,'1998-02-15 13:22:00','City of Qarth','The dragons are later stolen by the warlocks of Qarth and hidden in the House of the Undying. The warlocks and Xaro Xhoan Daxos carry out a coup to seize control of the city in the wake of the theft. Pyat Pree invites Daenerys to come to the House of the Undying and be reunited with her \'babies\'.\r\n\r\nDany\'s dragons breathe fire in \"Valar Morghulis.\"Once Daenerys is inside the citadel she is enthralled by two specific illusions; the Iron Throne room with a destroyed roof and snow falling in and the illusion of her dead husband and child. The sound of the crying dragons pulls Daenerys out of the temptation to stay in either illusion. Continuing her search for her dragons she finds them all chained beside each other, and they scream with excitement when she nears them. Daenerys goes to them, only to find herself chained as well. With her arms stretched to either side of her she becomes a captive. However with a bit of a twisted smile, Daenerys speaks the High Valyrian word for fire, and Drogon first answers her call, but isn\'t very effective by himself. However, both Rhaegal and Viserion then join in, setting Pyat Pree ablaze.',0,'Warlocks of Qarth steal Daenerys\' dragons');
/*!40000 ALTER TABLE `case` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `CatName` varchar(45) NOT NULL DEFAULT '',
  `MainCategory_MCatName` varchar(45) NOT NULL,
  PRIMARY KEY (`CatName`),
  KEY `fk_Category_MainCategory_idx` (`MainCategory_MCatName`),
  CONSTRAINT `fk_Category_MainCategory` FOREIGN KEY (`MainCategory_MCatName`) REFERENCES `maincategory` (`MCatName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES ('Theft','Crimes against property'),('Hodor Hodor Hodor','Hodor'),('Kinslaying','Murder'),('Regicide','Murder'),('Desertion','Oathbreaking'),('Guest Right violation','Oathbreaking'),('Knight\'s Oath','Oathbreaking'),('Oathbreaking','Oathbreaking'),('Incest','Sexual Delict'),('Rape','Sexual Delict'),('Rebellion','Treason');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `involve`
--

DROP TABLE IF EXISTS `involve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `involve` (
  `Case_CID` int(10) unsigned NOT NULL DEFAULT '0',
  `Person_PID` int(10) unsigned NOT NULL DEFAULT '0',
  `Role` varchar(90) NOT NULL,
  PRIMARY KEY (`Case_CID`,`Person_PID`),
  KEY `fk_Case_has_Person_Person1_idx` (`Person_PID`),
  KEY `fk_Case_has_Person_Case1_idx` (`Case_CID`),
  CONSTRAINT `fk_Case_has_Person_Case1` FOREIGN KEY (`Case_CID`) REFERENCES `case` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Case_has_Person_Person1` FOREIGN KEY (`Person_PID`) REFERENCES `person` (`PID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `involve`
--

LOCK TABLES `involve` WRITE;
/*!40000 ALTER TABLE `involve` DISABLE KEYS */;
INSERT INTO `involve` VALUES (1,1,'Suspect'),(1,2,'Victim'),(2,11,'Suspect'),(2,13,'Suspect'),(2,14,'Victim'),(3,15,'Suspect'),(3,16,'Victim'),(3,17,'Victim'),(4,3,'Suspect'),(5,8,'Suspect'),(5,18,'Victim'),(6,8,'Suspect'),(6,19,'Suspect'),(6,20,'Witness'),(7,3,'Suspect'),(7,21,'Witness'),(7,22,'Victim'),(8,7,'Victim'),(8,11,'Suspect'),(8,23,'Suspect'),(9,8,'Suspect'),(9,20,'Victim'),(10,13,'Victim'),(10,19,'Suspect'),(11,24,'Suspect'),(12,25,'Victim'),(12,26,'Suspect');
/*!40000 ALTER TABLE `involve` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maincategory`
--

DROP TABLE IF EXISTS `maincategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maincategory` (
  `MCatName` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`MCatName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maincategory`
--

LOCK TABLES `maincategory` WRITE;
/*!40000 ALTER TABLE `maincategory` DISABLE KEYS */;
INSERT INTO `maincategory` VALUES ('Crimes against property'),('Hodor'),('Murder'),('Oathbreaking'),('Sexual Delict'),('Treason');
/*!40000 ALTER TABLE `maincategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note` (
  `NID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Content` text NOT NULL,
  `Case_CID` int(10) unsigned DEFAULT NULL,
  `Person_PID` int(10) unsigned DEFAULT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `User_Username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`NID`),
  KEY `fk_Note_Case1_idx` (`Case_CID`),
  KEY `fk_Note_Person1_idx` (`Person_PID`),
  KEY `fk_Note_User1_idx` (`User_Username`),
  CONSTRAINT `fk_Note_Case1` FOREIGN KEY (`Case_CID`) REFERENCES `case` (`CID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_Note_Person1` FOREIGN KEY (`Person_PID`) REFERENCES `person` (`PID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_Note_User1` FOREIGN KEY (`User_Username`) REFERENCES `user` (`Username`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note`
--

LOCK TABLES `note` WRITE;
/*!40000 ALTER TABLE `note` DISABLE KEYS */;
INSERT INTO `note` VALUES (1,'daugther of Craster, forced to lie with him',7,22,'2014-04-14 12:17:50','Hodor'),(2,'Doesn\'t deny his deeds. Sees it as his obligation to \"care\" for his daughters.',7,3,'2014-04-14 12:18:39','Hodor'),(3,'Believed to have made a deal with Walter Frey.',8,11,'2014-04-14 12:24:45','Hodor'),(4,'Attempted to cover up his incest by murdering Bran.',9,8,'2014-04-14 12:26:53','Hodor'),(5,'<iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/J5iS3tULXMQ\" frameborder=\"0\" allowfullscreen></iframe>',9,NULL,'2014-04-14 13:00:23','Hodor'),(6,'Known to have helped but \"pardoned\" by Robert Baratheon.',2,11,'2014-04-14 16:03:53','Hodor'),(7,'I went to a wedding and all I got was this bloody T-Shirt',8,NULL,'2014-04-14 17:31:37','George');
/*!40000 ALTER TABLE `note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `PID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  `Birthdate` datetime DEFAULT NULL,
  `Address` varchar(90) DEFAULT NULL,
  `PhoneNumber` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`PID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'King Aegon II Targaryen','1908-06-04 00:00:00','Red Keep, King\'s landing',''),(2,'Rhaenyra Targaryen','1948-08-05 00:00:00','Red Keep, King\'s landing',''),(3,'Craster','2019-08-05 00:00:00','Craster\'s Keep, Beyond the Wall',''),(4,'King Stannis Baratheon','2019-08-05 00:00:00','Storm\'s End',''),(5,'Renly Baratheon','2019-08-05 00:00:00','Storm\'s End',''),(6,'Melisandre','2019-08-05 00:00:00','Asshai (Essos)',''),(7,'King Robb Stark','1933-08-05 00:00:00','Winterfell',''),(8,'Jaime Lannister','1954-08-05 00:00:00','Red Keep, King\'s landing',''),(9,'Alton Lannister','1955-08-05 00:00:00','Red Keep, King\'s landing',''),(10,'Walder Frey','1933-08-05 00:00:00','The Twins, the Riverlands',''),(11,'Tywin Lannister','1968-08-05 00:00:00','Red Keep, King\'s landing',''),(12,'Edmure Tully','1955-08-05 00:00:00','Riverrun, the Riverlands',''),(13,'Robert Baratheon','1958-09-21 00:00:00','Storm\'s End',''),(14,'Rhaegar Targaryen','1955-12-12 00:00:00','King\'s Landing',''),(15,'Gregor Clegane','1988-12-12 00:00:00','Clegane\'s Keep',''),(16,'Gregor Clegane\'s sister','1999-12-12 00:00:00','Clegane\'s Keep',''),(17,'Gregor Clegane\'s father','1955-12-12 00:00:00','Clegane\'s Keep',''),(18,'King Aerys II Targaryen','1966-12-12 00:00:00','King\'s Landing',''),(19,'Queen Cersei Lannister','1966-12-12 00:00:00','King\'s Landing, Red Keep',''),(20,'Bran Stark','1999-12-12 00:00:00','Winterfell',''),(21,'John Snow','1977-12-12 00:00:00','Winterfell, currently at Castle Back',''),(22,'Gilly','1988-12-12 00:00:00','Craster\'s Keep, Beyond the Wall',''),(23,'Lord Walder Frey','1912-12-12 00:00:00','The Twins, the Riverlands',''),(24,'Will (from the Night\'s Watch)','1977-12-12 00:00:00','The Wall, Castle Black',''),(25,'Daenerys Targaryen','1988-03-15 00:00:00','Essos',''),(26,'Pyat Pree','1890-08-02 00:00:00','House of the Undying, Qarth','');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result` (
  `Person_PID` int(10) unsigned NOT NULL DEFAULT '0',
  `Case_CID` int(10) unsigned NOT NULL DEFAULT '0',
  `ConvictionType` varchar(45) NOT NULL,
  `Evict` text NOT NULL,
  `DateEvict` timestamp NULL DEFAULT NULL,
  `DateEnforcement` timestamp NULL DEFAULT NULL,
  `DateRelease` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Person_PID`,`Case_CID`),
  KEY `fk_Person_has_Case_Case1_idx` (`Case_CID`),
  KEY `fk_Person_has_Case_Person1_idx` (`Person_PID`),
  CONSTRAINT `fk_Person_has_Case_Person1` FOREIGN KEY (`Person_PID`) REFERENCES `person` (`PID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Person_has_Case_Case1` FOREIGN KEY (`Case_CID`) REFERENCES `case` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
INSERT INTO `result` VALUES (1,1,'guilty but above the law','Was never officially trialed as nobody has a higher authority than King Aegon himself.',NULL,NULL,NULL),(13,2,'known to be guilty, but above the law','He won the Rebellion and thus became king, placing him above any other authority and making him untouchable for his crimes.',NULL,NULL,NULL),(24,11,'Death, by beheading','Guilty to deserting from his post at The Wall. Tried by Lord Edd Stark, Warden of the North.','1988-12-11 23:00:00','1988-12-11 23:00:00',NULL),(25,12,'Guilty for thievery, death by burning.','Trialed by Daenerys herself to death. Set aflame by the dragons Pyat Pree dies.','2008-08-14 22:00:00','2008-08-14 22:00:00',NULL);
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribe`
--

DROP TABLE IF EXISTS `subscribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribe` (
  `User_Username` varchar(45) NOT NULL DEFAULT '',
  `Case_CID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`User_Username`,`Case_CID`),
  KEY `fk_Subscribe_Case1_idx` (`Case_CID`),
  KEY `fk_Subscribe_User1_idx` (`User_Username`),
  CONSTRAINT `fk_Subscribe_User1` FOREIGN KEY (`User_Username`) REFERENCES `user` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Subscribe_Case1` FOREIGN KEY (`Case_CID`) REFERENCES `case` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribe`
--

LOCK TABLES `subscribe` WRITE;
/*!40000 ALTER TABLE `subscribe` DISABLE KEYS */;
INSERT INTO `subscribe` VALUES ('Hodor',1),('Hodor',3),('Hodor',4),('Hodor',5),('Hodor',6),('Hodor',7),('Hodor',8),('Hodor',11),('Hodor',12);
/*!40000 ALTER TABLE `subscribe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `Username` varchar(45) NOT NULL DEFAULT '',
  `Password` varchar(45) NOT NULL,
  `Email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('George','ilovegot',''),('Hodor','Hodor','');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `all_cases`
--

/*!50001 DROP TABLE IF EXISTS `all_cases`*/;
/*!50001 DROP VIEW IF EXISTS `all_cases`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `all_cases` AS select `case`.`CID` AS `CID`,`case`.`DateTime` AS `DateTime`,`case`.`Location` AS `Location`,`case`.`Description` AS `Description`,`case`.`Status` AS `status`,`case`.`Summary` AS `Summary`,group_concat(distinct `category`.`CatName` separator ', ') AS `ConCatName`,group_concat(distinct `category`.`MainCategory_MCatName` separator ', ') AS `ConMainCat` from ((`case` join `belongs`) join `category`) where ((`belongs`.`Case_CID` = `case`.`CID`) and (`category`.`CatName` = `belongs`.`Category_CatName`)) group by `case`.`CID` union select `case`.`CID` AS `CID`,`case`.`DateTime` AS `DateTime`,`case`.`Location` AS `Location`,`case`.`Description` AS `Description`,`case`.`Status` AS `Status`,`case`.`Summary` AS `Summary`,NULL AS `NULL`,NULL AS `NULL` from `case` where (not(exists(select `belongs`.`Case_CID` from `belongs` where (`belongs`.`Case_CID` = `case`.`CID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-14 22:03:35
