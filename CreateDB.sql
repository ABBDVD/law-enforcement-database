SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `dmdb2014` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `dmdb2014` ;

-- -----------------------------------------------------
-- Table `dmdb2014`.`Person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dmdb2014`.`person` (
  `PID` INT UNSIGNED NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Birthdate` DATETIME NULL,
  `Address` VARCHAR(90) NULL,
  `PhoneNumber` VARCHAR(20) NULL,
  PRIMARY KEY (`PID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dmdb2014`.`Case`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dmdb2014`.`case` (
  `CID` INT UNSIGNED NULL AUTO_INCREMENT,
  `DateTime` TIMESTAMP NOT NULL DEFAULT '2000-01-01 00:00:00',
  `Location` VARCHAR(90) NULL,
  `Description` TEXT NULL,
  `Status` TINYINT(1) NOT NULL,
  `Summary` VARCHAR(90) NOT NULL,
  PRIMARY KEY (`CID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dmdb2014`.`MainCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dmdb2014`.`maincategory` (
  `MCatName` VARCHAR(45) NULL,
  PRIMARY KEY (`MCatName`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dmdb2014`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dmdb2014`.`category` (
  `CatName` VARCHAR(45) NULL,
  `MainCategory_MCatName` VARCHAR(45) NOT NULL,
  INDEX `fk_Category_MainCategory_idx` (`MainCategory_MCatName` ASC),
  PRIMARY KEY (`CatName`),
  CONSTRAINT `fk_Category_MainCategory`
    FOREIGN KEY (`MainCategory_MCatName`)
    REFERENCES `dmdb2014`.`maincategory` (`MCatName`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dmdb2014`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dmdb2014`.`user` (
  `Username` VARCHAR(45) NULL,
  `Password` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NULL,
  PRIMARY KEY (`Username`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dmdb2014`.`Note`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dmdb2014`.`note` (
  `NID` INT UNSIGNED NULL AUTO_INCREMENT,
  `Content` TEXT NOT NULL,
  `Case_CID` INT UNSIGNED NULL,
  `Person_PID` INT UNSIGNED NULL,
  `Timestamp` TIMESTAMP NOT NULL,
  `User_Username` VARCHAR(45) NULL,
  PRIMARY KEY (`NID`),
  INDEX `fk_Note_Case1_idx` (`Case_CID` ASC),
  INDEX `fk_Note_Person1_idx` (`Person_PID` ASC),
  INDEX `fk_Note_User1_idx` (`User_Username` ASC),
  CONSTRAINT `fk_Note_Case1`
    FOREIGN KEY (`Case_CID`)
    REFERENCES `dmdb2014`.`case` (`CID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Note_Person1`
    FOREIGN KEY (`Person_PID`)
    REFERENCES `dmdb2014`.`person` (`PID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Note_User1`
    FOREIGN KEY (`User_Username`)
    REFERENCES `dmdb2014`.`user` (`Username`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dmdb2014`.`Involve`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dmdb2014`.`involve` (
  `Case_CID` INT UNSIGNED NULL,
  `Person_PID` INT UNSIGNED NULL,
  `Role` VARCHAR(90) NOT NULL,
  PRIMARY KEY (`Case_CID`, `Person_PID`),
  INDEX `fk_Case_has_Person_Person1_idx` (`Person_PID` ASC),
  INDEX `fk_Case_has_Person_Case1_idx` (`Case_CID` ASC),
  CONSTRAINT `fk_Case_has_Person_Case1`
    FOREIGN KEY (`Case_CID`)
    REFERENCES `dmdb2014`.`case` (`CID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Case_has_Person_Person1`
    FOREIGN KEY (`Person_PID`)
    REFERENCES `dmdb2014`.`person` (`PID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dmdb2014`.`Result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dmdb2014`.`result` (
  `Person_PID` INT UNSIGNED NULL,
  `Case_CID` INT UNSIGNED NULL,
  `ConvictionType` VARCHAR(45) NOT NULL,
  `Evict` TEXT NOT NULL,
  `DateEvict` TIMESTAMP NULL,
  `DateEnforcement` TIMESTAMP NULL,
  `DateRelease` TIMESTAMP NULL,
  PRIMARY KEY (`Person_PID`, `Case_CID`),
  INDEX `fk_Person_has_Case_Case1_idx` (`Case_CID` ASC),
  INDEX `fk_Person_has_Case_Person1_idx` (`Person_PID` ASC),
  CONSTRAINT `fk_Person_has_Case_Person1`
    FOREIGN KEY (`Person_PID`)
    REFERENCES `dmdb2014`.`person` (`PID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Person_has_Case_Case1`
    FOREIGN KEY (`Case_CID`)
    REFERENCES `dmdb2014`.`case` (`CID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dmdb2014`.`Belongs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dmdb2014`.`belongs` (
  `Case_CID` INT UNSIGNED NULL,
  `Category_CatName` VARCHAR(45) NULL,
  PRIMARY KEY (`Case_CID`, `Category_CatName`),
  INDEX `fk_Belongs_Category1_idx` (`Category_CatName` ASC),
  INDEX `fk_Belongs_Case1_idx` (`Case_CID` ASC),
  CONSTRAINT `fk_Belongs_Case1`
    FOREIGN KEY (`Case_CID`)
    REFERENCES `dmdb2014`.`case` (`CID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Belongs_Category1`
    FOREIGN KEY (`Category_CatName`)
    REFERENCES `dmdb2014`.`category` (`CatName`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dmdb2014`.`Subscribe`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dmdb2014`.`subscribe` (
  `User_Username` VARCHAR(45) NULL,
  `Case_CID` INT UNSIGNED NULL,
  PRIMARY KEY (`User_Username`, `Case_CID`),
  INDEX `fk_Subscribe_Case1_idx` (`Case_CID` ASC),
  INDEX `fk_Subscribe_User1_idx` (`User_Username` ASC),
  CONSTRAINT `fk_Subscribe_User1`
    FOREIGN KEY (`User_Username`)
    REFERENCES `dmdb2014`.`user` (`Username`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Subscribe_Case1`
    FOREIGN KEY (`Case_CID`)
    REFERENCES `dmdb2014`.`case` (`CID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

USE `dmdb2014` ;

-- -----------------------------------------------------
-- Placeholder table for view `dmdb2014`.`all_cases`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dmdb2014`.`all_cases` (`CID` INT, `DateTime` INT, `Location` INT, `Description` INT, `status` INT, `Summary` INT, `ConCatName` INT, `ConMainCat` INT);

-- -----------------------------------------------------
-- View `dmdb2014`.`all_cases`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dmdb2014`.`all_cases`;
USE `dmdb2014`;
CREATE  OR REPLACE VIEW all_cases AS
SELECT CID, DateTime, Location, Description, status, Summary, GROUP_CONCAT(DISTINCT CatName SEPARATOR ', ') as ConCatName, 
GROUP_CONCAT(DISTINCT MainCategory_MCatName SEPARATOR ', ') as ConMainCat FROM dmdb2014.case, belongs, category
WHERE Case_CID = CID && CatName = Category_CatName GROUP BY CID UNION
SELECT *,  null, null FROM dmdb2014.case
WHERE NOT EXISTS (SELECT Case_CID FROM belongs WHERE Case_CID = CID);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
