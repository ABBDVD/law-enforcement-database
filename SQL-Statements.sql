-- -----------------------------------------------------
-- View
-- -----------------------------------------------------

-- Create view to bundle cases with and without category

CREATE OR REPLACE VIEW all_cases AS
SELECT CID, DateTime, Location, Description, status, Summary, GROUP_CONCAT(DISTINCT CatName SEPARATOR ', ') as ConCatName, 
GROUP_CONCAT(DISTINCT MainCategory_MCatName SEPARATOR ', ') as ConMainCat
FROM dmdb2014.case, belongs, category
WHERE Case_CID = CID && CatName = Category_CatName GROUP BY CID
    UNION
SELECT *,  null, null
FROM dmdb2014.case
WHERE NOT EXISTS (SELECT Case_CID
                  FROM belongs
                  WHERE Case_CID = CID);

-- Example: Get oldest unsolved cases

SELECT * FROM all_cases WHERE Status = false ORDER BY DateTime;

-- -----------------------------------------------------
-- Notes
-- -----------------------------------------------------

-- Outer Join to get Notes with and without person entry

SELECT CID, summary, NID, Content, Timestamp, PID, Name, User_Username as Username
FROM dmdb2014.case, note LEFT OUTER JOIN person ON PID = Person_PID
WHERE CID = Case_CID AND CID = ?
ORDER BY PID, Timestamp
LIMIT ?, 5;

-- Count Notes

SELECT COUNT(*) as number
FROM dmdb2014.case, note LEFT OUTER JOIN person ON PID = Person_PID
WHERE CID = Case_CID AND CID = ?;

-- -----------------------------------------------------
-- Search
-- -----------------------------------------------------

SELECT * FROM all_cases WHERE Summary LIKE ?;
SELECT * FROM person WHERE Name LIKE ?;
SELECT DISTINCT PID, Name, Birthdate, Address, PhoneNumber FROM person, involve WHERE Person_PID = PID AND Role LIKE ?;
...

-- -----------------------------------------------------
-- Update
-- -----------------------------------------------------

INSERT INTO involve VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE Role = ?;
UPDATE person SET Name = ?, Birthdate = ?, Address = ?, PhoneNumber = ? WHERE PID = ?;
DELETE FROM subscribe WHERE Case_CID = ? AND User_Username = ?;

