<%@page import="ch.ethz.inf.dbproject.model.Result"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h1><%if (session.getAttribute("editing") != null) {%>
<%=((Boolean)session.getAttribute("editing")) ? "Edit " : "Create " %>
<%} %>Conviction</h1>

<% // show the form for adding new cases
	final Result aResult = (Result) session.getAttribute("aResult");
	if (username != null) {%>
		<form action="Conviction?cid=<%=session.getAttribute("cid")%>&pid=<%=session.getAttribute("pid")%>" method="post">
		<input type="hidden" name="action" value="Conv" />
		<table style="text-align: right">
            <tr>
                <th>Case Summary</th>
                <td style="text-align: left;" width="300"><%=aResult.getSummary()%></td>
            </tr>
            <tr>
                <th>Person</th>
                <td style="text-align: left;" width="300"><%=aResult.getName()%></td>
            </tr>
			<tr>
				<th>Conviction Type</th>
				<td><input type="text" size="46" name="convType" value="<%=aResult.getConvictionType()%>"/></td>
			</tr>
			<tr>
				<th>Date of Evict</th>
				<td><input type="date" size="10" name="dateEvict" value="<%=aResult.getUSDateEvict()%>"/></td>
			</tr>
			<tr>
				<th>Date of Enforcement</th>
				<td><input type="date" size="10" name="dateEnforcement" value="<%=aResult.getUSDateEnforcement()%>"/></td>
			</tr>
			<tr>
				<th>Date of Release</th>
				<td><input type="date" size="10" name="dateRelease" value="<%=aResult.getUSDateRelease()%>" /></td>
			</tr>
			<tr>
				<th>Evict</th>
				<td><textarea rows="8" cols="60" name="evict"/><%=aResult.getEvict()%></textarea></td>
			</tr>
			<tr>
				<th colspan="2">
					<input type="submit" value="Edit Conviction" name="submit"/>
					<% if ((Boolean) session.getAttribute("delete")) { %><input type="submit" value="Confirm" name="submit"/> <% } else {%>
					<input type="submit" value="Delete Conviction" name="submit"/>
				<%} %>
				</th>
			</tr>
		</table>
		</form>	
		<%if (session.getAttribute("ConvFormError") != null){ %>
		<%= "<h4>" + ((String)session.getAttribute("ConvFormError")) + "</h4>" %>
	<%} %>
<%
	}
	else {
	%><h4>Please login to edit the case.</h4>
	<%} %>

<%@ include file="Footer.jsp" %>