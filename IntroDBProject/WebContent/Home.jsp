<%@page import="ch.ethz.inf.dbproject.HomeServlet"%>
<%@page import="ch.ethz.inf.dbproject.UserServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<%
if (username != null) {
	// There is a user logged in! Display a greeting!
%>
	Welcome back <%=username%>
<%
} else {
	// No user logged in.%>
	Welcome!
<%
}
%>

<br />
<br />
<img src="http://theresilientearth.com/files/images-2012/Winter_is_coming-Game_of_thrones.jpg" alt="Winter is Coming!">
<%@ include file="Footer.jsp" %>