<%@page import="ch.ethz.inf.dbproject.model.Person"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h1>Edit Person</h1>

<% // show the form for adding new person
	final Person person = (Person) session.getAttribute("person");
	if (username != null) {%>
		<form action="EditPerson?id=<%=session.getAttribute("id")%>" method="post">
		<input type="hidden" name="action" value="editPerson" />
		<table style="text-align: right">
			<tr>
				<th>Name</th>
				<td><input type="text" size="46" name="name" value="<%=person.getName()%>"/></td>
			</tr>
			<tr>
				<th></th>
				<td><b>Birthdate</b> <input type="date" size="10" name="birthdate" value="<%=person.getBirthdateUSFormat()%>"/></td>
			</tr>
			<tr>
				<th>Address</th>
				<td><input type="text" size="46" name="address" value="<%=person.getAddress()%>"/></td>
			</tr>
			<tr>
				<th>Phone Number</th>
				<td><input type="text" size="46" name="phonenumber" value="<%=person.getPhonenumber()%>"/></td>
			</tr>
			<tr>
				<th colspan="2"><input type="submit" value="Edit Person" /></th>
			</tr>
		</table>
		</form>	
		<%if (session.getAttribute("newPersonFormError") != null){ %>
		<%= "<h4>" + ((String)session.getAttribute("newPersonFormError")) + "</h4>" %>
	<%} %>
<%
	}
	else {
	%><h4>Please login to edit the person.</h4>
	<%} %>

<%@ include file="Footer.jsp" %>