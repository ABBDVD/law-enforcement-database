<%@page import="ch.ethz.inf.dbproject.UserServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h2>Your Account</h2>

<%
if (username != null) {
	// User is logged in. Display the details:
%>

<% if (session.getAttribute("subCases") != null) { %>
	<h2>Subscribed Cases</h2>
	<%=session.getAttribute("subCases")%>
<% } %>

<% if (session.getAttribute("newsTables") != null) { %>
	<h2>News Feed</h2>
	<%=session.getAttribute("newsTables")%>
<% } %>

<a name="profile"></a>
<h2>Change profile data</h2>
	<form action="User#profile" method="post">
	<input type="hidden" name="action" value="changeProfile" />
	<table style="text-align: right">
		<tr>
			<th>Username</th>
			<td><input style="background: #F5F1DE" type="text" name="username" value="<%=username%>" readonly/></td>
		</tr>
		<tr>
			<th>old Password</th>
			<td><input type="password" name="password" value="" /></td>
		</tr>
		<tr>
			<th>Password</th>
			<td><input type="password" name="password1" value="" /></td>
		</tr>
		<tr>
			<th>Password<br>confirmation</th>
			<td><input type="password" name="password2" value="" /></td>
		</tr>
		<tr>
			<th>Email</th>
			<td><input type="text" name="email" value="<%=session.getAttribute("results")%>" /></td>
		</tr>
		<tr>
			<th colspan="2">
				<input type="submit" value="Change" name="submit"/>
				<% if ((Boolean) session.getAttribute("delete")) { %><input type="submit" value="Confirm" name="submit"/> <% } else {%>
					<input type="submit" value="Delete User" name="submit"/>
				<%} %>
			</th>
		</tr>
	</table>
	</form>	
	<% if (session.getAttribute("Message2") != null){ %>
		<%= "<h4>" + ((String)session.getAttribute("Message2")) + "</h4>" %>
	<%} %>

<%
} else {
	// User not logged in. Display the login form.
%>

	<form action="User#profile" method="post">
	<input type="hidden" name="action" value="login" />
	<table>
		<tr>
			<th>Username</th>
			<td><input type="text" name="username" value="" /></td>
		</tr>
		<tr>
			<th>Password</th>
			<td><input type="password" name="password" value="" /></td>
		</tr>
		<tr>
			<th colspan="2">
				<input type="submit" value="Login" />
			</th>
		</tr>
	</table>
	</form>
	<% if (session.getAttribute("results") != null){ %>
		<%= session.getAttribute("results") %>
	<%} %>
	<h2>Register</h2>
	<form action="User#profile" method="post">
	<input type="hidden" name="action" value="register" />
	<table style="text-align: right">
		<tr>
			<th>Username</th>
			<td><input type="text" name="username" value="" /></td>
		</tr>
		<tr>
			<th>Password</th>
			<td><input type="password" name="password1" value="" /></td>
		</tr>
		<tr>
			<th>Password<br>confirmation</th>
			<td><input type="password" name="password2" value="" /></td>
		</tr>
		<tr>
			<th>Email</th>
			<td><input type="text" name="email" value="" /></td>
		</tr>
		<tr>
			<th colspan="2">
				<input type="submit" value="Register" />
			</th>
		</tr>
	</table>
	</form>
	<% if (session.getAttribute("Message2") != null){ %>
		<%= "<h4>" + ((String)session.getAttribute("Message2")) + "</h4>" %>
	<%} 
}
%>

<%@ include file="Footer.jsp" %>