<%@page import="ch.ethz.inf.dbproject.UserServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h1>Detailed case view</h1>

<%=session.getAttribute("caseTable")%>

<% if (username != null && session.getAttribute("caseStatus") != null) { %>
    <br />
<%  boolean Status = ((String) session.getAttribute("caseStatus")).equals("true");
    if (!Status){ %>
    <a href="Note?cid=<%=session.getAttribute("id")%>">Add Note</a> &nbsp;&nbsp;|
    &nbsp;&nbsp;<a href="AddPerson?cid=<%=session.getAttribute("id")%>&action=selectPerson">Add Person</a> &nbsp;&nbsp;|
    &nbsp;&nbsp;<a href="EditCase?id=<%=session.getAttribute("id")%>">Edit Case</a> &nbsp;&nbsp;|
    <form action="Case?id=<%=session.getAttribute("id")%>" method="post" id="changeCat" style="display:inline-block">
        <input type="hidden" name="action" value="addCategory" />
        <input type="submit" class="submitLink" value="Add to Category ">
    </form>
    <select name="category" form="changeCat">
    <%=session.getAttribute("categories") %>
    </select> &nbsp;&nbsp;|
    <form action="Case?id=<%=session.getAttribute("id")%>" method="post" id="changeRemCat" style="display:inline-block">
        <input type="hidden" name="action" value="remCategory" />
        <input type="submit" class="submitLink" value="Remove from Category ">
    </form>
    <select name="category" form="changeRemCat">
    <%=session.getAttribute("remCategories") %>
    </select> &nbsp;&nbsp;|
    <% if (session.getAttribute("canSubscribe") != null && (Boolean) session.getAttribute("canSubscribe")) { %>
         &nbsp;&nbsp;<a href="Case?id=<%=session.getAttribute("id")%>&subscribe=true">Subscribe</a>
    <% } else if (session.getAttribute("canSubscribe") != null && !(Boolean) session.getAttribute("canSubscribe")) {%>
         &nbsp;&nbsp;<a href="Case?id=<%=session.getAttribute("id")%>&subscribe=false">Unsubscribe</a>
    <% }%> &nbsp;&nbsp;|&nbsp;&nbsp;
    <% } %>
    <a href="Case?id=<%=session.getAttribute("id")%>&changeStatusTo=<%=!Status%>"><%=Status ? "Open Case" : "Close Case"%></a>
    <h4><%=session.getAttribute("ChangeMess")%></h4>
<%  }%>

<% if (session.getAttribute("involvementTable") != null) { %>
<h2>Involved Persons</h2>
<%=session.getAttribute("involvementTable")%>
<% } %>

<% if (session.getAttribute("resultTable") != null) { %>
<h2>Convictions</h2>
<%=session.getAttribute("resultTable")%>
<% } if (session.getAttribute("Mess") != null) {%>
		<h4><%=session.getAttribute("Mess")%></h4>
	<%} %>


<% if (session.getAttribute("noteTables") != null) { %>
<h2>Notes</h2>
<a name="notes"></a>
<%=session.getAttribute("noteTables")%>
<%=session.getAttribute("pageButtons")%>
<% } %>

<%@ include file="Footer.jsp"%>
