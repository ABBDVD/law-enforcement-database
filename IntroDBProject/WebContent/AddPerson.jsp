<%@page import="ch.ethz.inf.dbproject.UserServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>
<% final String user = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS); %>

<h1>Add Person to Case</h1>

<% if (session.getAttribute("action") == null) { %>

<% } else if (session.getAttribute("action").equals("selectPerson")) { %>

    <h2>Add new Person</h2>
	<form action="AddPerson?cid=<%=session.getAttribute("cid")%>&action=selectPerson" method="post">
	<input type="hidden" name="addPerson" value="true" />
	<table style="text-align: right">
		<tr>
			<th>Name</th>
			<td><input type="text" size="46" name="name" value=""/></td>
		</tr>
		<tr>
			<th>Birthdate</th>
			<td><input type="date" size="10" name="birthdate" value="yyyy-mm-dd" /></td>
		</tr>
		<tr>
			<th>Address</th>
			<td><input type="text" size="46" name="address" value=""/></td>
		</tr>
		<tr>
			<th>Phone Number</th>
			<td><input type="text" size="46" name="phonenumber" value=""/></td>
		</tr>
		<tr>
			<th colspan="2">
				<input type="submit" value="Add Person" />
			</th>
		</tr>
	</table>
	</form>
	<% if (session.getAttribute("newPersonFormError") != null) { %>
		<%= "<h4>" + ((String)session.getAttribute("newPersonFormError")) + "</h4>" %>
	<% } %>

	<h2>Search existing person</h2>
	
	<form action="AddPerson?cid=<%=session.getAttribute("cid")%>&action=selectPerson" method="post">
		Search by Name
		<input type="text" name="filter" />
		<input type="submit" value="Search" title="Search" />
	</form>
	
	<br />
	
	<% if (session.getAttribute("table") != null) { %>
		<%=session.getAttribute("table")%>
	<% } %>

<% } else if (session.getAttribute("action").equals("selectRole")) { %>

	<h2>Choose Role</h2>
	
	<form action="AddPerson?cid=<%=session.getAttribute("cid")%>&action=insertPerson&pid=<%=session.getAttribute("pid")%>" method="post">
		<input type="text" name="role"
		    <% if(session.getAttribute("involved") != null) { %>
		        value="<%=session.getAttribute("involved")%>"
		    <% } %>
		 />
		<input type="submit" value="Select" title="Select Role" />
		<% if(session.getAttribute("involved") != null) { %>
			<input type="submit" name="removePerson" value="Remove" title="Remove from Case" />
		<% } %>
	</form>

<% } %>

<%@ include file="Footer.jsp"%>