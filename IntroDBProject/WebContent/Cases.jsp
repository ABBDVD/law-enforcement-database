<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h1>Cases</h1>

<%= session.getAttribute("cases") %>

<% // show the form for adding new cases
	final String filter = request.getParameter("filter");
	final String category = request.getParameter("category");
	if (username != null && filter == null && category == null) {%>
	    <h2>Add new case</h2>
		<form action="Cases" method="post">
		<input type="hidden" name="action" value="addCase" />
		<table style="text-align: right">
			<tr>
				<th>Case summary</th>
				<td><input type="text" size="46" name="caseSum" value=""/></td>
			</tr>
			<tr>
				<th>Location</th>
				<td><input type="text" size="46" name="location" value="" /></td>
			</tr>
			<tr>
				<th></th>
				<td><b>Time</b> <input type="time" size="8" name="time" value="--:--"/> <b>Date</b> <input type="date" size="10" name="date" value="yyyy-mm-dd"/></td>
			</tr>
			<tr>
				<th>Case description</th>
				<td><textarea rows="8" cols="60" name="caseDescr"/></textarea>
			</tr>
			<tr>
				<th colspan="2">
					<input type="submit" value="Add Case" />
				</th>
			</tr>
		</table>
		</form>
		<% if (session.getAttribute("newCaseFormError") != null){ %>
		<%= "<h4>" + ((String)session.getAttribute("newCaseFormError")) + "</h4>" %>
	<% } %>
<% } %>

<%@ include file="Footer.jsp" %>