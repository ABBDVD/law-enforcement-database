<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h1>Search Case</h1>
<hr/>

<table style="text-align: left">
	<tr>
		<form method="get" action="Search">
		<input type="hidden" name="filter" value="Case" />
			<th style="width:200px">Search By Description</th>
			<td><input type="text" size="46" name="Description"/></td>
			<td><input type="submit" value="Search" title="Search by Description" /></td>
		</form>
	</tr>
	<tr>
		<form method="get" action="Search">
		<input type="hidden" name="filter" value="Case" />
			<th style="width:200px">Search By Category</th>
			<td><input type="text" size="46" name="Category"/></td>
			<td><input type="submit" value="Search" title="Search by Category" /></td>
		</form>
	</tr>
	<tr>
		<form method="get" action="Search">
		<input type="hidden" name="filter" value="Case" />
			<th style="width:200px">Search By Location</th>
			<td><input type="text" size="46" name="Location"/></td>
			<td><input type="submit" value="Search" title="Search by Location" /></td>
		</form>
	</tr>
</table>

<hr />
<h1>Search Person</h1>
<hr />

<table style="text-align: left">
	<tr>
		<form method="get" action="Search">
		<input type="hidden" name="filter" value="Person" />
			<th style="width:200px">Search By Name</th>
			<td><input type="text" size="46" name="Name"/></td>
			<td><input type="submit" value="Search" title="Search by Name" /></td>
		</form>
	</tr>
	<tr>
		<form method="get" action="Search">
		<input type="hidden" name="filter" value="Person" />
			<th style="width:200px">Search By Role</th>
			<td><input type="text" size="46" name="Role"/></td>
			<td><input type="submit" value="Search" title="Search by Role" /></td>
		</form>
	</tr>
</table>

<hr/>
<h1>Search Convictions</h1>
<hr/>

<table style="text-align: left">
	<tr>
		<form method="get" action="Search">
		<input type="hidden" name="filter" value="Result" />
			<th style="width:200px">Search By Conviction Type</th>
			<td><input type="text" size="46" name="Conviction"/></td>
			<td><input type="submit" value="Search" title="Search by Conviction Type" /></td>
		</form>
	</tr>
	<tr>
		<form method="get" action="Search">
		<input type="hidden" name="filter" value="Result" />
			<th style="width:200px">Search By Evict</th>
			<td><input type="text" size="46" name="Evict"/></td>
			<td><input type="submit" value="Search" title="Search by Evict" /></td>
		</form>
	</tr>
</table>

<hr/>
<br/>
<%=session.getAttribute("results")%>

<%@ include file="Footer.jsp" %>