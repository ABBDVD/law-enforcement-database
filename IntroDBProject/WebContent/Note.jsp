<%@page import="ch.ethz.inf.dbproject.UserServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>
<% final String user = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS); %>

<h1>Add Note</h1>

<form action="Note?cid=<%=session.getAttribute("cid")%>&pid=<%=session.getAttribute("pid")%>" method="post">
	<input type="hidden" name="nid" value="<%=session.getAttribute("nid")%>" />
	Add Comment
	<br />
	<textarea rows="4" cols="50" name="content"><%=session.getAttribute("content")%></textarea>
	<br />
	<input type="submit" value="Submit" />
</form>

<%@ include file="Footer.jsp"%>