<%@page import="ch.ethz.inf.dbproject.UserServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
	
	<head>
	    <link href="style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Records of the Law</title>
	</head>

	<body>

		<!-- Header -->
		
		<table id="masterTable" style="broder-collapse: collapse; border-spacing: 0">
			<tr>
				<th id="masterHeader" colspan="2">
					<h1>Records of the Law</h1>
					<div style="float: left; width: 33.33333%; text-align:left; visibility:hidden">Easteregg</div>
					<div style="float: left; width: 33.33333%; text-align:center">Project by Melanie H&#252;sser &#38; Christopher Signer</div>
					<div style="float: left; width: 33.33333%; text-align:right">
					<% String username = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);
					if (username != null) {
					%>Logged in as: 
					<%= username %>
					<form method="post" action="User#profile" style="display:inline-block">
					<div>
						<input type="hidden" name="Logout" value="true" /> 
						<input type="submit" class="submitLink" value="Logout" title="Logout" />
					</div>
					</form>
<% } else { %>
					<a href="User#profile">Not logged in</a>&nbsp;&nbsp;
					<%} %>
					</div>
				</th>
			</tr>
			<tr id="masterContent">
			
				<td id="masterContentMenu">
					
					<div class="menuDiv1"></div>
					<div class="menuDiv1"><a href="Home">Home</a></div>
					<div class="menuDiv1"><a href="Cases">All cases</a></div>
					<div class="menuDiv2"><a href="Cases?filter=open">Open</a></div>
					<div class="menuDiv2"><a href="Cases?filter=closed">Closed</a></div>
					<div class="menuDiv2"><a href="Cases?filter=recent">Recent</a></div>
					<div class="menuDiv2"><a href="Cases?filter=oldest">Oldest Unsolved</a></div>
					<div class="menuDiv1">Main Categories</div>
					<div class="menuDiv2"><a href="Cases?category=Crimes against property">Crimes against property</a></div>
					<div class="menuDiv2"><a href="Cases?category=Hodor">Hodor</a></div>
					<div class="menuDiv2"><a href="Cases?category=Murder">Murder</a></div>
					<div class="menuDiv2"><a href="Cases?category=Oathbreaking">Oathbreaking</a></div>
					<div class="menuDiv2"><a href="Cases?category=Sexual Delict">Sexual Delict</a></div>
					<div class="menuDiv2"><a href="Cases?category=Treason">Treason</a></div>
					<div class="menuDiv1"><a href="Search">Search</a></div>
					<div class="menuDiv1"><a href="User">User Profile</a></div>
					
				</td>
				
				<td id="masterContentPlaceholder">
				
		