<%@page import="ch.ethz.inf.dbproject.UserServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h1>Person Details</h1>

<%=session.getAttribute("personTable")%>

<% if (username != null) { %>
    <br />
    <a href="Note?pid=<%=session.getAttribute("id")%>">Add Note</a> &nbsp;&nbsp;|
    &nbsp;&nbsp;<a href="EditPerson?id=<%=session.getAttribute("id")%>">Edit Person</a>
<% } %>

<% if (session.getAttribute("involvementTable") != null) { %>
<h2>Involved Cases</h2>
<%=session.getAttribute("involvementTable")%>
<% } %>

<% if (session.getAttribute("resultTable") != null) { %>
<h2>Criminal Record</h2>
<%=session.getAttribute("resultTable")%>
<% } if (session.getAttribute("Mess") != null) {%>
		<h4><%=session.getAttribute("Mess")%></h4>
	<%} %>

<% if (session.getAttribute("noteTables") != null) { %>
<h2>Notes</h2>
<a name="notes"></a>
<%=session.getAttribute("noteTables")%>
<%=session.getAttribute("pageButtons")%>
<% } %>

<%@ include file="Footer.jsp"%>