<%@page import="ch.ethz.inf.dbproject.model.Case"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h1>Edit Case</h1>

<% // show the form for adding new cases
	final Case aCase = (Case) session.getAttribute("aCase");
	if (username != null) {%>
		<form action="EditCase?id=<%=session.getAttribute("id")%>" method="post">
		<input type="hidden" name="action" value="editCase" />
		<table style="text-align: right">
			<tr>
				<th>Case summary</th>
				<td><input type="text" size="46" name="caseSum" value="<%=aCase.getSummary()%>"/></td>
			</tr>
			<tr>
				<th>Location</th>
				<td><input type="text" size="46" name="location" value="<%=aCase.getLocation()%>" /></td>
			</tr>
			<tr>
				<th></th>
				<td><b>Time</b> <input type="time" size="8" name="time" value="<%=aCase.getDatetime().substring(0, 5)%>"/> <b>Date</b> <input type="date" size="10" name="date" value="<%=aCase.getDateUSFormat()%>"/></td>
			</tr>
			<tr>
				<th>Case description</th>
				<td><textarea rows="8" cols="60" name="caseDescr"/><%=aCase.getDescription()%></textarea></td>
			</tr>
			<tr>
				<th colspan="2"><input type="submit" value="Edit Case" /></th>
			</tr>
		</table>
		</form>	
		<%if (session.getAttribute("newCaseFormError") != null){ %>
		<%= "<h4>" + ((String)session.getAttribute("newCaseFormError")) + "</h4>" %>
	<%} %>
<%
	}
	else {
	%><h4>Please login to edit the case.</h4>
	<%} %>

<%@ include file="Footer.jsp" %>