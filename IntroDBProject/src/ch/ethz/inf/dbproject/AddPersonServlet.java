package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Person;
import ch.ethz.inf.dbproject.model.Involvement;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class of Case Details Page
 */
@WebServlet(description = "Adds a person to a case.", urlPatterns = { "/AddPerson" })
public final class AddPersonServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private final DatastoreInterface dbInterface = new DatastoreInterface();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddPersonServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        
        doPost(request, response);
        
    }
    
    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected final void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        
        final HttpSession session = request.getSession(true);
        
        String loggedUser = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);
        
        final String cidString = request.getParameter("cid");
        if (cidString == null || loggedUser == null) {
            response.sendRedirect("./Cases");
            return;
        }

        try {
            
            final int cid = Integer.parseInt(cidString);
            session.setAttribute("cid", cid);
            
            final String action = request.getParameter("action");
            session.setAttribute("action", action);
            
            if (action != null)
            if (action.equals("selectPerson")) {
                
                session.setAttribute("newPersonFormError", null);
                if (request.getParameter("addPerson") != null && request.getParameter("addPerson").equals("true")) {
                    final String name = request.getParameter("name");
                    final String birthdate = request.getParameter("birthdate");
                    final String address = request.getParameter("address");
                    final String phonenumber = request.getParameter("phonenumber");
                    
                    if (name.length() == 0)
                        session.setAttribute("newPersonFormError", "Name is a required field.");
                    else if (name.length() > 45)
                        session.setAttribute("newPersonFormError", "Name may be no longer than 45 characters (currently " + name.length() + ").");  
                    else if (address.length() > 90)
                        session.setAttribute("newPersonFormError", "Address may be no longer than 90 characters (currently " + address.length() + ").");
                    else if (phonenumber.length() > 20)
                        session.setAttribute("newPersonFormError", "Phone number may be no longer than 20 characters (currently " + phonenumber.length() + ").");
                    else {
                        // convert time and date to Timestamp
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Date parsedDate = new Date();
                        try {
                            Timestamp t = null;
                            if (birthdate.length() > 0) {
                                parsedDate = dateFormat.parse(birthdate);
                                t = new Timestamp(parsedDate.getTime());
                            }
                            int pid = dbInterface.addPerson(name, t, address, phonenumber);
                            if (pid == -1) session.setAttribute("newPersonFormError", "Unhandled SQL Error when submitting this case.");
                            else {
                                response.sendRedirect("AddPerson?cid=" + cid + "&action=selectRole&pid=" + pid);
                                return;
                            }
                        } catch (ParseException e) {
                            session.setAttribute("newPersonFormError", "Date format is wrong.");
                            e.printStackTrace();
                        }
                    }
                }
                
                // Person list
                List<Person> persons = null;
                if (request.getParameter("filter") != null)
                    persons = dbInterface.searchByName(request.getParameter("filter"));
                // Hide already involved persons
                List<Involvement> involved = dbInterface.getInvolvementByCID(cid);
                if (persons != null && involved != null) {
                    for (Involvement i: involved)
                        for (Person p: persons)
                            if (p.getId() == i.getPid()) {
                                persons.remove(p);
                                break;
                            }
                }
                if (persons != null && !persons.isEmpty()) {
                    final BeanTableHelper<Person> table = new BeanTableHelper<Person>("cases", "casesTable", Person.class);
                    table.addBeanColumn("Name", "name");
                    table.addBeanColumn("Birthdate", "birthdate");
                    table.addBeanColumn("Address", "address");
                    table.addBeanColumn("Phone number", "phonenumber");
                    table.addLinkColumn("", "Add to case", "AddPerson?cid=" + cid + "&action=selectRole&pid=", "id");
                    table.addObjects(persons);
                    session.setAttribute("table", table);
                }
                else
                    session.setAttribute("table", null);
            }
            else if (action.equals("selectRole")) {
                final String pidString = request.getParameter("pid");
                if (pidString == null) {
                    response.sendRedirect("./AddPerson?cid=" + cid + "&action=selectPerson");
                    return;
                }
                final int pid = Integer.parseInt(pidString);
                session.setAttribute("pid", pid);
                
                // Check if already involved
                session.setAttribute("involved", null);
                List<Involvement> involvements = dbInterface.getInvolvementByCID(cid);
                for (Involvement i: involvements) {
                    if (i.getPid() == pid) {
                        session.setAttribute("involved", i.getRole());
                        break;
                    }
                }
            }
            else if (action.equals("insertPerson")) {
                final String pidString = request.getParameter("pid");
                if (pidString == null) {
                    response.sendRedirect("./AddPerson?cid=" + cid + "&action=selectPerson");
                    return;
                }
                final int pid = Integer.parseInt(pidString);
                
                // Check if already involved
                session.setAttribute("involved", null);
                List<Involvement> involvements = dbInterface.getInvolvementByCID(cid);
                for (Involvement i: involvements) {
                    if (i.getPid() == pid) {
                        session.setAttribute("involved", i.getRole());
                        break;
                    }
                }
                
                // Remove person
                if (session.getAttribute("involved") != null && request.getParameter("removePerson") != null) {
                    dbInterface.removeInvolvement(cid, pid);
                    response.sendRedirect("./Case?id=" + cid);
                    return;
                }
                
                final String role = request.getParameter("role");
                if (role == null || role.isEmpty()) {
                    response.sendRedirect("./AddPerson?cid=" + cid + "&action=selectRole&pid=" + pid);
                    return;
                }
                
                // Add involvement
                dbInterface.addInvolve(cid, pid, role);
                response.sendRedirect("./Case?id=" + cid);
                return;
            }
            else {
                response.sendRedirect("./Case?cid=" + cid);
                return;
            }
            
        } catch (final Exception ex) {
            ex.printStackTrace();
            response.sendRedirect("./Cases");
            return;
        }

        this.getServletContext().getRequestDispatcher("/AddPerson.jsp").forward(request, response);
    }
}