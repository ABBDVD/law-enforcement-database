package ch.ethz.inf.dbproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Involvement;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Note;

@WebServlet(description = "Page to add Notes", urlPatterns = { "/Note" })
public final class NoteServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private final DatastoreInterface dbInterface = new DatastoreInterface();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NoteServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
        
    }
    
    protected final void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
    
        final HttpSession session = request.getSession(true);
        
        String loggedUser = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);
        
        final String pidString = request.getParameter("pid");
        final int pid;
        if (pidString != null) pid = Integer.parseInt(pidString);
        else pid = 0;

        final String cidString = request.getParameter("cid");
        final int cid;
        if (cidString != null) cid = Integer.parseInt(cidString);
        else cid = 0;
        
        final String nidString = request.getParameter("nid");
        final int nid;
        Note note = null;
        if (nidString != null) {
            nid = Integer.parseInt(nidString);
            note = dbInterface.getNoteByNID(nid);
        }
        else nid = 0;
        
        final String action = request.getParameter("action");
        boolean allowed;
        if (loggedUser != null && action != null && note != null && (action.equals("editNote") || action.equals("deleteNote")) && !loggedUser.equals(note.getUsername()))
            allowed = false;
        else
            allowed = true;
        
        // Is Person and Case related?
        boolean related = false;
        if (pid != 0 && cid != 0) {
            for (Involvement i: dbInterface.getInvolvementByPID(pid))
                if (cid == i.getCid())
                    related = true;
        }
        else
            related = true;
        
        // Exit if not logged in, no context set or context not related
        if (loggedUser == null || (pid == 0 && cid == 0) || !related || !allowed) {
            if (pid != 0)
                response.sendRedirect("./Person?id=" + pid);
            else if (cid != 0)
                response.sendRedirect("./Case?id=" + cid);
            else
                response.sendRedirect("./Cases");
            return;
        }
        
        session.setAttribute("pid", pid);
        session.setAttribute("cid", cid);
        session.setAttribute("nid", nid);
        
        final String content = request.getParameter("content");
        
        try {
            // Is note set
            if (content != null) {
                if (nid == 0)
                    dbInterface.addNote(cid, pid, loggedUser, content);
                else
                    dbInterface.editNote(content, nid);
                
                if (pid != 0)
                    response.sendRedirect("./Person?id=" + pid + "#notes");
                else
                    response.sendRedirect("./Case?id=" + cid + "#notes");
                return;
            }
        } catch (final Exception ex) {
            ex.printStackTrace();
            response.sendRedirect("./Cases");
            return;
        }
        
        if (action != null && action.equals("deleteNote")) {
            dbInterface.removeNote(nid);
            
            if (pid != 0)
                response.sendRedirect("./Person?id=" + pid + "#notes");
            else
                response.sendRedirect("./Case?id=" + cid + "#notes");
            return;
        }
        else if (action != null && action.equals("editNote")) {
            session.setAttribute("content", note.getContent());
        }
        else
            session.setAttribute("content", "");
        
        this.getServletContext().getRequestDispatcher("/Note.jsp").forward(request, response);
    }

}
