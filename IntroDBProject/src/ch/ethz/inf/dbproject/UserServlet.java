package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Note;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

@WebServlet(description = "Page that displays the user login / logout options.", urlPatterns = { "/User" })
public final class UserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();
	
	public final static String SESSION_USER_DETAILS = "userDetails";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserServlet() {
		super();
	}

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected final void doGet(final HttpServletRequest request, 
            final HttpServletResponse response) throws ServletException, IOException {
        
        doPost(request, response);
        
    }
    
    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected final void doPost(final HttpServletRequest request, 
            final HttpServletResponse response) throws ServletException, IOException {

		final HttpSession session = request.getSession(true);
		session.setAttribute("results", null);
		session.setAttribute("Message2", null);
		session.setAttribute("delete", false);
		
	    //logout?
		final String logout = request.getParameter("Logout");
		if (logout != null && logout.equals("true")){
		    session.setAttribute(SESSION_USER_DETAILS, null);
		    session.setAttribute("subCases", null);
		}
		
		
		String loggedUser = (String) session.getAttribute(SESSION_USER_DETAILS);
        
		String action = request.getParameter("action");
		
		// change profile information? / delete User
		if (action != null && action.trim().equals("changeProfile") && loggedUser!=null){
		    String output = "";
		    final String password = request.getParameter("password");
		    final String oldPw = dbInterface.getPassword(loggedUser);
		    action = request.getParameter("submit");
		    
		    //Change
		    if (action.equals("Change")) {
    		    String password1 = request.getParameter("password1");
    		    final String password2 = request.getParameter("password2");
    		    String email = request.getParameter("email");
    		    
    		    final String oldMail = dbInterface.getEmail(loggedUser);
    		    
    		    boolean updNecessary = false;
    		    if (password1== null || password2 == null || !(password1.equals(password2))) {
    		        output = " The new passwords don't match up.<br>";
    		        password1 = oldPw;
    		    }
    		    else if (!password1.equals("") && password1.length()<6) {
    		        output = "The new password doesn't have a length of minimum 6 characters.<br>";
    		        password1 = oldPw;
    		    }
    		    else if (password.equals(oldPw)){ // password change necessary?
    		        updNecessary = true;
    		        output = "Password was changed.<br>";
    		    }
    		    else {
    		        output = "Please make sure that the old Password entered is correct.";
    		        password1 = oldPw;
    		    }
    		    
    		    if (email!=null && !email.equals("") && (oldMail ==null || !(oldMail.equals(email)))) { // is the entered email NOT different?
    		        updNecessary = true;
    		        output += "Email was changed";
    		    }
    		    else email = oldMail;
    		    if (updNecessary) {
    		        int retCode = dbInterface.regUser(loggedUser, password1, email);
    		        if (retCode != 2) output = "Some unknown SQL Error occured.";
    		    }
    		    else output = "No change was done.<br>" + output;
		    }
		    //Delete user
		    else if (action.equals("Delete User")) {
		        session.setAttribute("delete", true);
		        output = "Please enter your old password and click the Confirm button.";
		    }
		    else if (action.equals("Confirm")) { // delete user and logout
		        if (password!= null && password.equals(oldPw)) {
    		        if (dbInterface.remUser(loggedUser) == 1) {
    		            output = "User was deleted.";
        		        session.setAttribute(SESSION_USER_DETAILS, null);
        	            session.setAttribute("subCases", null);
        	            loggedUser = null;
    		        }
    		        else output = "Some error occured when attempting to delete your user profile. Please click 'User Profile' in the main menu and try again.";
		        }
		        else {
		            output = "Your (old) password is incorrect.";
		            session.setAttribute("delete", true);
		        }
		    }
		    else output = "The URL was modified incorrectly. Please click 'User Profile' in the main menu and try again. ";
		    session.setAttribute("Message2", output);
		}
		
		
		
		// do we login?
		else if (action != null && action.trim().equals("login") && loggedUser == null) {

			final String username = request.getParameter("username");
			// Note: It is really not safe to use HTML get method to send passwords.
			// However for this project, security is not a requirement.
			final String password = request.getParameter("password");
			
			String queriedDB = this.dbInterface.getPassword(username);
			if (queriedDB != null && queriedDB.equals(password)) {
                session.setAttribute(SESSION_USER_DETAILS, username);
                String email = dbInterface.getEmail(username);
                if (email == null) email = "";
                session.setAttribute("results", email);
                loggedUser = username;
			}
			else session.setAttribute("results", "<h4>Wrong credentials!</h4>");

		}
		// do we register?
		else if (action != null && action.trim().equals("register") && loggedUser == null) {
		    final String username = request.getParameter("username");
		    final String password1 = request.getParameter("password1");
		    final String password2 = request.getParameter("password2");
		    String email = request.getParameter("email");
		    String output;
		    if (username == null || password1 == null ||password2 == null) output = "Please enter a Username, a password and confirm the password.";
		    else if (dbInterface.getPassword(username) != null) output = "This username is already taken. Please enter another.";
		    else if (!password1.equals(password2)) output = "The passwords are not the same.";
		    else if (password1.length()<6) output = "The password must have a length of minimum 6 characters.";
		    else if (dbInterface.regUser(username, password1, email) != 1) output = "Some unknown error occured.";
		    else {
		        // login
		        output = null;
                session.setAttribute(SESSION_USER_DETAILS, username);
                if (email == null) email = "";
                session.setAttribute("results", email);
                loggedUser = username;
		    }
		    session.setAttribute("Message2", output);
		}
		
		if (loggedUser != null) {
            // Logged in
            String email = dbInterface.getEmail(loggedUser);
            if (email== null) email = "";
            session.setAttribute("results", email);
            
            List<Case> subscribedCases = this.dbInterface.getSubscribedCases(loggedUser);
            if (!subscribedCases.isEmpty()) {
                final BeanTableHelper<Case> table = new BeanTableHelper<Case>("cases", "casesTable", Case.class);
                table.addBeanColumn("Case Summary", "summary");
                table.addBeanColumn("Location", "location");
                table.addBeanColumn("Status", "status");
                table.addBeanColumn("Category", "category");
                table.addBeanColumn("Maincategory", "maincategory");
                table.addBeanColumn("Occured at", "datetime");
                table.addLinkColumn("", "View Case", "Case?id=", "id");
                table.addObjects(subscribedCases);
                session.setAttribute("subCases", table);
            }
            else
                session.setAttribute("subCases", null);
            
            // Notes
            List<Note> news = dbInterface.getNewsFeed(loggedUser);
            if (!news.isEmpty()) {
                String newsTables = "";
                for (Note n: news) {
                    BeanTableHelper<Note> t = new BeanTableHelper<Note>("notes", "notesTable", Note.class);
                    t.addBeanColumn("Author", "username");
                    t.addBeanColumn("Date", "timestamp");
                    t.addBeanColumn("<a href=\"Case?id=" + n.getCid() + "\">Case</a>", "summary");
                    if (n.getPid() != 0)
                        t.addBeanColumn("<a href=\"Person?id=" + n.getPid() + "\">Person</a>", "name");
                    t.addBeanColumn("Note", "content");
                    t.addObject(n);
                    t.setVertical(true);
                    newsTables += t + "<br />";
                }
                session.setAttribute("newsTables", newsTables);
            }
            else
                session.setAttribute("newsTables", null);
        }
		// Finally, proceed to the User.jsp page which will renden the profile
		this.getServletContext().getRequestDispatcher("/User.jsp").forward(request, response);
		session.setAttribute("delete", false);

	}

}
