package ch.ethz.inf.dbproject.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Misc {
    
    public static Timestamp formatUS(String t) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (t != null && t.length()>0)
            try {
                return new Timestamp((dateFormat.parse(t)).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        else return null;
    }
    
    public static boolean validDate(Timestamp t) {
        return t == null || t.after(Timestamp.valueOf("1970-01-02 00:00:00")) && t.before(Timestamp.valueOf("2038-01-18 00:00:00"));
    }
    
}
