package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Person;
import ch.ethz.inf.dbproject.model.Result;
import ch.ethz.inf.dbproject.util.Misc;

/**
 * Servlet implementation class of EditCase page
 */
@WebServlet(description = "The home page of the project", urlPatterns = { "/Conviction" })
public final class ConvictionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConvictionServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) 
			throws ServletException, IOException {

		final HttpSession session = request.getSession(true);

		/*******************************************************
		 * Construct a table to present all our results
		 *******************************************************/
		
		String loggedUser = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);

        final String cidString = request.getParameter("cid");
        final String pidString = request.getParameter("pid");
        if (cidString == null && pidString != null) {
            response.sendRedirect("./Person?id=" + pidString);
            return;
        }
        else if (cidString != null && pidString == null) {
            response.sendRedirect("./Case?id=" + cidString);
            return;
        }
        else if (cidString == null || loggedUser == null || pidString == null) {
            response.sendRedirect("./Cases");
            return;
        }
        final Integer pid = Integer.parseInt(pidString);
        final Integer cid = Integer.parseInt(cidString);
        session.setAttribute("pid", pid);
        session.setAttribute("cid", cid);
        
        try {
            Result aResult = this.dbInterface.getSpecResult(cid, pid);
            if (aResult == null) {
                // load the summary for the case that we don't have a conviction yet
                Case aCase = dbInterface.getCaseById(cid);
                Person person = dbInterface.getPersonById(pid);
                if (aCase != null && person != null)
                    aResult = new Result(cid, aCase.getSummary(), "", "", null, null, null, pid, person.getName());
                else {
                    response.sendRedirect("./Cases");
                    return;
                }
                session.setAttribute("editing", false);
            }
            else session.setAttribute("editing", true);
            session.setAttribute("aResult", aResult);
        } catch (final Exception ex) {
            ex.printStackTrace();
            response.sendRedirect("./Cases");
            return;
        }
        
		// Finally, proceed to the Projects.jsp page which will render the Projects
		this.getServletContext().getRequestDispatcher("/Conviction.jsp").forward(request, response);
	}
	
	// do POST
	@Override
	protected final void doPost(final HttpServletRequest request, final HttpServletResponse response) 
            throws ServletException, IOException {

        final HttpSession session = request.getSession(true);
        final String loggedUser = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);
        
        final String cidString = request.getParameter("cid");
        final String pidString = request.getParameter("pid");
        if (cidString == null && pidString != null) {
            response.sendRedirect("./Person?id=" + pidString);
            return;
        }
        else if (cidString != null && pidString == null) {
            response.sendRedirect("./Case?id=" + cidString);
            return;
        }
        else if (cidString == null || loggedUser == null || pidString == null) {
            response.sendRedirect("./Cases");
            return;
        }
        final Integer pid = Integer.parseInt(pidString);
        final Integer cid = Integer.parseInt(cidString);
        session.setAttribute("pid", pid);
        session.setAttribute("cid", cid);
        
        try {
            Result aResult = this.dbInterface.getSpecResult(cid, pid);
            if (aResult == null) {
                // load the summary for the case that we don't have a conviction yet
                Case aCase = dbInterface.getCaseById(cid);
                if (aCase != null)
                aResult = new Result(cid, aCase.getSummary(), "", "", null, null, null, pid, "");
                else {
                    response.sendRedirect("./Cases");
                    return;
                }
                session.setAttribute("editing", false);
            }
            else session.setAttribute("editing", true);
            session.setAttribute("aResult", aResult);
        } catch (final Exception ex) {
            ex.printStackTrace();
            response.sendRedirect("./Cases");
            return;
        }
        
        session.setAttribute("ConvFormError", null);
        String action = request.getParameter("action");
        if (action != null && action.trim().equals("Conv") && loggedUser != null) {
            action = request.getParameter("submit");
            
            if (action.equals("Edit Conviction")) {
                final String convType = request.getParameter("convType");
                final String evict = request.getParameter("evict");
                final String dateEvict = request.getParameter("dateEvict");
                final String dateEnforcement = request.getParameter("dateEnforcement");
                final String dateRelease = request.getParameter("dateRelease");
                
                if (convType.length()==0 || evict.length()==0)
                    session.setAttribute("ConvFormError", "Conviction time and evct are required fields.");
                else if (convType.length()>45)
                    session.setAttribute("ConvFormError", "Case summary may be no longer than 45 characters (currently " + convType.length() + ").");  
               else if (evict.length()>60000)
                    session.setAttribute("ConvFormError", "Case description may be no longer than 60000 characters (currently " + evict.length() + ").");
                else {
                    // convert time and date to Timestamp
                    Timestamp TEvict = Misc.formatUS(dateEvict);
                    Timestamp TEnf = Misc.formatUS(dateEnforcement);
                    Timestamp TRelease = Misc.formatUS(dateRelease);
                    
                    // check if all 3 dates are in the valid date range (or null)
                    if (Misc.validDate(TEvict) && Misc.validDate(TEnf) && Misc.validDate(TRelease)) {
                        int result = dbInterface.Conv(pid, cid, convType, evict, TEvict, TEnf, TRelease);
                        if (result==1) {
                            session.setAttribute("Mess", "Conviction was sucessfully added.");
                            response.sendRedirect("Person?id=" + pid);
                            return;
                        }
                        else if (result==2){
                            session.setAttribute("Mess", "Conviction was sucessfully edited.");
                            response.sendRedirect("Person?id=" + pid);
                            return;
                        }
                        else if (result==0) {
                            session.setAttribute("Mess", "No change done to conviction. All submitted data was the same as before.");
                            response.sendRedirect("Person?id=" + pid);
                            return;
                        }
                        else session.setAttribute("ConvFormError", "Unhandled SQL Error when submitting this case.");
                    }
                    else {
                        session.setAttribute("ConvFormError", "Please make sure the dates are in the correct range between 02.01.1970 and 18.01.2038 or empty.");
                    }
                }
            }
            else if (action.equals("Delete Conviction")) {
                session.setAttribute("delete", true);
                session.setAttribute("ConvFormError", "Please click the Confirm button to delete this conviction.");
            }
            else if (action.equals("Confirm")) { // delete user and logout
                int result = dbInterface.remConv(pid, cid); 
                if (result == 1) {
                    session.setAttribute("Mess", "Conviction was deleted.");
                    response.sendRedirect("Person?id=" + pid);
                    return;
                }
                else if (result == 0) session.setAttribute("ConvFormError", "Conviction wasn't found.");
                else session.setAttribute("ConvFormError", "Some error occured when attempting to delete this convction. Please try again.");
            }
            else session.setAttribute("ConvFormError", "The URL was modified incorrectly. Please navigate anew to this page and try again." + action);
        }
        else session.setAttribute("ConvFormError", "Some error occured. Please reload the page (without submitting the from data and try again).");
        this.getServletContext().getRequestDispatcher("/Conviction.jsp").forward(request, response);
        session.setAttribute("ConvFormError", null);
        session.setAttribute("delete", false);
        session.setAttribute("aResult", null);
	}
}
