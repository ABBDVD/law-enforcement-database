package ch.ethz.inf.dbproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.model.Person;
import ch.ethz.inf.dbproject.model.Result;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class Search
 */
@WebServlet(description = "The search page for cases", urlPatterns = { "/Search" })
public final class SearchServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();
		
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		final HttpSession session = request.getSession(true);
		
		/*******************************************************
		 * Construct a table to present all our search results
		 *******************************************************/

		final String filter = request.getParameter("filter");

        if (filter != null && filter.equals("Case")) {
            final BeanTableHelper<Case> table = new BeanTableHelper<Case>("cases", "casesTable", Case.class);
    		table.addBeanColumn("Case Summary", "summary");
    		table.addBeanColumn("Location", "location");
            table.addBeanColumn("Status", "status");
            table.addBeanColumn("Category", "category");
            table.addBeanColumn("Maincategory", "maincategory");
    		table.addLinkColumn("", "View Case", "Case?id=", "id");
    		table.addBeanColumn("Occured at", "datetime");
    		if (request.getParameter("Description") != null) {
                table.addObjects(dbInterface.searchBySummary(request.getParameter("Description")));
			}
    		else if (request.getParameter("Category") != null) {
                table.addObjects(dbInterface.searchByCategory(request.getParameter("Category")));
            }
    		else if (request.getParameter("Location") != null) {
                table.addObjects(dbInterface.searchByLocation(request.getParameter("Location")));
            }
            session.setAttribute("results", table);
            
			// check if we actually found any search result
			Object results = session.getAttribute("results");
	        String resultsStr = results.toString();
	        if (!resultsStr.contains("<td>")) session.setAttribute("results", "<h4>No search results found!</h4>");
		}
        else if (filter != null && filter.equals("Person")) {
            final BeanTableHelper<Person> table = new BeanTableHelper<Person>("cases", "casesTable", Person.class);
            table.addBeanColumn("Name", "name");
            table.addBeanColumn("Birthdate", "birthdate");
            table.addBeanColumn("Address", "address");
            table.addBeanColumn("Phone number", "phonenumber");
            table.addLinkColumn("", "View Person", "Person?id=", "id");
            if (request.getParameter("Name") != null) {
                table.addObjects(dbInterface.searchByName(request.getParameter("Name")));
            }
            else if (request.getParameter("Role") != null) {
                table.addObjects(dbInterface.searchByRole(request.getParameter("Role")));
            }
            session.setAttribute("results", table);
            
            // check if we actually found any search result
            Object results = session.getAttribute("results");
            String resultsStr = results.toString();
            if (!resultsStr.contains("<td>")) session.setAttribute("results", "<h4>No search results found!</h4>");
        }
        else if (filter != null && filter.equals("Result")) {
            final BeanTableHelper<Result> table = new BeanTableHelper<Result>("cases", "casesTable", Result.class);
            table.addBeanColumn("Case Summary", "summary");
            table.addBeanColumn("Convict's Name", "name");
            table.addBeanColumn("Conviction Type", "convictionType");
            table.addBeanColumn("Evict", "evict");
            table.addBeanColumn("Date of Evict", "dateEvict");
            table.addBeanColumn("Date of Enforcement", "dateEnforcement");
            table.addBeanColumn("Date of Release", "dateRelease");
            table.addLinkColumn("", "View Case", "Case?id=", "cid");
            table.addLinkColumn("", "View Person", "Person?id=", "pid");
            if (request.getParameter("Conviction") != null) {
                table.addObjects(dbInterface.searchByConviction(request.getParameter("Conviction")));
            }
            else if (request.getParameter("Evict") != null) {
                table.addObjects(dbInterface.searchByEvict(request.getParameter("Evict")));
            }
            session.setAttribute("results", table);
            
            // check if we actually found any search result
            Object results = session.getAttribute("results");
            String resultsStr = results.toString();
            if (!resultsStr.contains("<td>")) session.setAttribute("results", "<h4>No search results found!</h4>");
        }
        else session.setAttribute("results", "<h4>Please enter a keyword and click \"Search\".</h4>");

		// Finally, proceed to the Seaech.jsp page which will render the search results
	    
        this.getServletContext().getRequestDispatcher("/Search.jsp").forward(request, response);	        
	}
}