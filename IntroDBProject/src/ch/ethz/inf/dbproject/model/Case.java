package ch.ethz.inf.dbproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;

public final class Case {
	
	/**
	 * case properties
	 */
	private final int id;
	private final Date datetime;
	private final String location;
	private final String description;
	private final boolean status;
	private final String summary;
	private final String category;
	private final String maincategory;
	
	/**
	 * Construct a new case.
	 * 
	 * @param description		The name of the case
	 */
	public Case(final int id, final Date datetime, final String location, 
	        final String description, final boolean status, final String summary, final String category, final String maincategory) {
		this.id = id;
		this.datetime = datetime;
		this.location = location;
		this.description = description;
		this.status = status;
		this.summary = summary;
		this.category = category;
		this.maincategory = maincategory;
	}
	
	public Case(final Tuple t) {
        this.id = t.getInt("CID");
        this.datetime = t.getTimestamp("DateTime");
        this.location = t.getText("Location");
        this.description = t.getText("Description");
        this.status  = t.getBoolean("Status");
        this.summary = t.getText("Summary");
        this.category = t.getText("ConCatName") == null ? "" : t.getText("ConCatName");
        this.maincategory = t.getText("ConMainCat") == null ? "" : t.getText("ConMainCat");
	}
	
	public Case(	final ResultSet rs) throws SQLException {
		this.id = rs.getInt("CID");
		this.datetime = rs.getTimestamp("DateTime");
		this.location = rs.getString("Location");
		this.description = rs.getString("Description");
		this.status  = rs.getBoolean("Status");
		this.summary = rs.getString("Summary");
		this.category = rs.getString("ConCatName") == null ? "" : rs.getString("ConCatName");
		this.maincategory = rs.getString("ConMainCat") == null ? "" : rs.getString("ConMainCat");
	}

	/**
	 * HINT: In eclipse, use Alt + Shirt + S menu and choose:
	 * "Generate Getters and Setters to auto-magically generate
	 * the getters. 
	 */
	public String getDescription() {
		return description;
	}

	public String getMaincategory() {
        return maincategory;
    }

    public int getId() {
		return id;
	}

	public String getDatetime() {
	    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm, dd.MM.yyyy");
	    return sdf.format(datetime);
    }
	
	public String getDateUSFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(datetime);
    }

    public String getLocation() {
        return location;
    }

    public String getStatus() {
        return status ? "Closed" : "Opened";
    }
    public boolean getBoolStatus() {
        return status;
    }

    public String getSummary() {
        return summary;
    }

    public String getCategory() {
        return category;
    }	
}
