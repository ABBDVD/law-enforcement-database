package ch.ethz.inf.dbproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;

/**
 * Object that represents a conviction.
 */
public class Result {

    // Case Properties
    private final int cid;
    private final String summary;
    
    // Result Properties
    private final String convictionType;
    private final String evict;
    private final Date dateEvict;
    private final Date dateEnforcement;
    private final Date dateRelease;
    
    // Person Properties
    private final int pid;
    private final String name;
    
    public Result(final int cid, final String summary, final String convictionType, final String evict,
            final Date dateEvict, final Date dateEnforcement, final Date dateRelease, final int pid, final String name) {
        this.cid = cid;
        this.summary = summary;
        this.convictionType = convictionType;
        this.evict = evict;
        this.dateEvict = dateEvict;
        this.dateEnforcement = dateEnforcement;
        this.dateRelease = dateRelease;
        this.pid = pid;
        this.name = name;
    }
    
    public Result(final Tuple t) {
        this.cid = t.getInt("CID");
        this.summary = t.getText("Summary");
        this.convictionType = t.getText("ConvictionType");
        this.evict = t.getText("Evict");
        this.dateEvict = t.getTimestamp("DateEvict");
        this.dateEnforcement = t.getTimestamp("DateEnforcement");
        this.dateRelease = t.getTimestamp("DateRelease");
        this.pid = t.getInt("PID");
        this.name = t.getText("Name");
    }
    
    public Result(final ResultSet rs) throws SQLException {
        this.cid = rs.getInt("CID");
        this.summary = rs.getString("Summary");
        this.convictionType = rs.getString("ConvictionType");
        this.evict = rs.getString("Evict");
        this.dateEvict = rs.getTimestamp("DateEvict");
        this.dateEnforcement = rs.getTimestamp("DateEnforcement");
        this.dateRelease = rs.getTimestamp("DateRelease");
        this.pid = rs.getInt("PID");
        this.name = rs.getString("Name");
    }

    public int getCid() {
        return cid;
    }

    public String getSummary() {
        return summary;
    }

    public String getConvictionType() {
        return convictionType;
    }

    public String getEvict() {
        return evict;
    }

    public String getDateEvict() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        if (dateEvict != null)
            return sdf.format(dateEvict);
        else
            return "";
    }
    public String getUSDateEvict() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (dateEvict != null)
            return sdf.format(dateEvict);
        else
            return "";
    }

    public String getDateEnforcement() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        if (dateEnforcement != null)
            return sdf.format(dateEnforcement);
        else
            return "";
    }
    public String getUSDateEnforcement() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (dateEnforcement != null)
            return sdf.format(dateEnforcement);
        else
            return "";
    }

    public String getDateRelease() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        if (dateRelease != null)
            return sdf.format(dateRelease);
        else
            return "";
    }
    public String getUSDateRelease() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (dateRelease != null)
            return sdf.format(dateRelease);
        else
            return "";
    }

    public int getPid() {
        return pid;
    }
    
    public String getName() {
        return name == null ? "": name;
    }
    
}
