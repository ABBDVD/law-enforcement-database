package ch.ethz.inf.dbproject.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ch.ethz.inf.dbproject.util.Pair;
import ch.ethz.inf.dbproject2.model.simpleDatabase.TableMetadata;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Group;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Join;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Limit;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Minus;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.OuterJoin;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Project;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Rename;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Scan;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Search;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Select;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Sort;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Union;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator.Aggregator;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator.Concat;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator.Count;
import ch.ethz.inf.dbproject2.model.simpleDatabase.statement.Delete;
import ch.ethz.inf.dbproject2.model.simpleDatabase.statement.Insert;
import ch.ethz.inf.dbproject2.model.simpleDatabase.statement.Update;

/**
 * This class should be the interface between the web application
 * and the database. Keeping all the data-access methods here
 * will be very helpful for part 2 of the project.
 */
public final class DatastoreInterface {
    
	public DatastoreInterface () {
	    TableMetadata.rootDirectory = "db/";
	}
	
    /* VIEWS */
    
    // View all_cases
    private final Operator all_cases() {
        return new Project(new Rename(new OuterJoin(new Scan("case"), new Group(
                    new Join(new Scan("belongs"), new Scan("category"), "Category_CatName", "CatName"), "Case_CID",
                    new Aggregator[] { new Concat(null, "CatName", ", ", true), new Concat(null, "MainCategory_MCatName", ", ", true) }),
                "CID", "Case_CID"), // OuterJoin case:belongs
                new String[] { "Concat0", "Concat1" }, new String[] { "ConCatName", "ConMainCat" }), // Rename
                new String[] { "CID", "DateTime", "Location", "Description", "Status", "Summary", "ConCatName", "ConMainCat" }); // Project
    }
	
    /* CASE */
    
    // SELECT * FROM all_cases WHERE CID = ?;
	public final Case getCaseById(final int id) {
	    Operator op = new Select<Integer>(all_cases(), "CID", id);
	    
	    if (op.moveNext())
	        return new Case(op.current());
	    return null;
	}
	
	// SELECT * FROM all_cases;
	public final List<Case> getAllCases() {
	    Operator op = all_cases();
	    
        final List<Case> cases = new ArrayList<Case>();
	    while (op.moveNext())
	        cases.add(new Case(op.current()));
	    return cases;
	}
	
	// SELECT * FROM all_cases, subscribe WHERE User_Username = ? AND Case_CID = CID && status = false;
	public final List<Case> getSubscribedCases(String username) {
	    Operator op = new Join(
	                      new Select<String>(all_cases(), "Status", "0"),
	                      new Select<String>(new Scan("subscribe"), "User_Username", username),
	                      "CID", "Case_CID"); // Join case:subscribe
	    
        final List<Case> cases = new ArrayList<Case>();
        while (op.moveNext())
            cases.add(new Case(op.current()));
        return cases;
	}
	
	// SELECT CID, summary, NID, Content, Timestamp, PID, Name, n.User_Username as Username
    // FROM dmdb2014.case, subscribe s, note n LEFT OUTER JOIN person ON PID = Person_PID
    // WHERE CID = n.Case_CID AND CID = s.Case_CID AND Status = false AND s.User_Username = ? ORDER BY Timestamp DESC LIMIT 0, 3;
    public final List<Note> getNewsFeed(final String username) {
        Operator op = new Limit(new Sort(new Project(new OuterJoin(new Join(
                          new Rename(new Scan("note"), new String[] { "User_Username", "Case_CID" }, new String[] { "Username", "Note_CID" }),
                          new Join(
                              new Select<String>(new Scan("case"), "Status", "0"),
                              new Select<String>(new Scan("subscribe"), "User_Username", username),
                              "CID", "Case_CID"), // Join case:subscribe
                          "Note_CID", "CID"), // Join note:case
                      new Scan("person"), "Person_PID", "PID"), // OuterJoin note:person
                      new String[] { "CID", "Summary", "NID", "Content", "Timestamp", "PID", "Name", "Username" }), // Project
                      "Timestamp", false), 3); // Sort; Limit
        
        final List<Note> notes = new ArrayList<Note>();
        while (op.moveNext())
            notes.add(new Note(op.current()));
        return notes;
    }
	
    // SELECT * FROM all_cases WHERE status = ?;
	public final List<Case> getCasesByStatus(boolean status) {
        Operator op = new Select<String>(all_cases(), "Status", status? "1": "0");
        
        final List<Case> cases = new ArrayList<Case>();
        while (op.moveNext())
            cases.add(new Case(op.current()));
        return cases;
    }
	
	// SELECT * FROM all_cases ORDER BY DateTime DESC;
	public final List<Case> getMostRecentCases() {
        Operator op = new Sort(all_cases(), "DateTime", false);
        
        final List<Case> cases = new ArrayList<Case>();
        while (op.moveNext())
            cases.add(new Case(op.current()));
        return cases;
    }
	
	// SELECT * FROM all_cases WHERE Status = false ORDER BY DateTime;
	public final List<Case> getOldestUnsolvedCases() {
        Operator op = new Sort(new Select<String>(all_cases(), "Status", "0"), "DateTime", true);
        
        final List<Case> cases = new ArrayList<Case>();
        while (op.moveNext())
            cases.add(new Case(op.current()));
        return cases;
	}
	
	/* PERSON */
	
	// SELECT * FROM person WHERE PID = ?;
	public final Person getPersonById(final int id) {
        Operator op = new Select<Integer>(new Scan("person"), "PID", id);
        
        if (op.moveNext())
            return new Person(op.current());
        return null;
	}
    
	// SELECT * FROM all_cases, involve, person
    // WHERE Case_CID = CID AND Person_PID = PID AND PID = ? ORDER BY DateTime DESC;
	public final List<Involvement> getInvolvementByPID(final int id) {
	    Operator op = new Sort(new Join(
                          all_cases(), new Join(
                              new Scan("involve"), new Select<Integer>(new Scan("person"), "PID", id),
                              "Person_PID", "PID"), // Join involve:person
                          "CID", "Case_CID"), // Join case:involve
                      "DateTime", false); // Sort
	    
        final List<Involvement> involvements = new ArrayList<Involvement>();
        while (op.moveNext())
            involvements.add(new Involvement(op.current()));
        return involvements;
    }
    
	// SELECT * FROM all_cases, involve, person
    // WHERE Case_CID = CID AND Person_PID = PID AND CID = ? ORDER BY Role;
	public final List<Involvement> getInvolvementByCID(final int id) {
        Operator op = new Sort(new Join(
                          new Scan("person"), new Join(
                              new Scan("involve"), new Select<Integer>(all_cases(), "CID", id),
                              "Case_CID", "CID"), // Join involve:case
                          "PID", "Person_PID"), // Join person:involve
                      "Role"); // Sort
        
        final List<Involvement> involvements = new ArrayList<Involvement>();
        while (op.moveNext())
            involvements.add(new Involvement(op.current()));
        return involvements;
	}
	
	// SELECT CID, Summary, ConvictionType, Evict, DateEvict, DateEnforcement,
    // DateRelease, Person_PID as PID, Name FROM dmdb2014.case, result, person WHERE Case_CID = CID AND
    // Person_PID = ? AND Person_PID = PID ORDER BY DateEvict DESC;
	public final List<Result> getResultByPID(final int id) {
	    Operator op = new Sort(new Join(
	                      new Scan("case"), new Join(
                              new Scan("result"), new Select<Integer>(new Scan("person"), "PID", id),
                              "Person_PID", "PID"), // Join result:person
                          "CID", "Case_CID"), // Join case:result
	                  "DateEvict", false); // Sort
	    
        final List<Result> results = new ArrayList<Result>();
        while (op.moveNext())
            results.add(new Result(op.current()));
        return results;
    }
	
	// SELECT CID, Summary, ConvictionType, Evict, DateEvict, DateEnforcement,
    // DateRelease, Person_PID as PID, Name FROM dmdb2014.case, result, person
    // WHERE Case_CID = CID AND Person_PID = PID AND Case_CID = ? ORDER BY DateEvict DESC;
	public final List<Result> getResultByCID(final int id) {
        Operator op = new Sort(new Join(
                          new Scan("person"), new Join(
                              new Scan("result"), new Select<Integer>(new Scan("case"), "CID", id),
                              "Case_CID", "CID"), // Join result:case
                          "PID", "Person_PID"), // Join person:result
                      "DateEvict", false); // Sort
        
        final List<Result> results = new ArrayList<Result>();
        while (op.moveNext())
            results.add(new Result(op.current()));
        return results;
    }
	
	// SELECT CID, Summary, ConvictionType, Evict, DateEvict, DateEnforcement, DateRelease, Person_PID as PID, Name
    // FROM dmdb2014.case, result, person WHERE Case_CID = CID AND Person_PID = PID AND Case_CID = ? AND Person_PID = ? ORDER BY DateEvict DESC;
	public final Result getSpecResult (final int cid, final int pid) {
        Operator op = new Sort(new Join(
                          new Join(
                              new Scan("result"), new Select<Integer>(new Scan("case"), "CID", cid),
                              "Case_CID", "CID"), // Join result:case
                          new Select<Integer>(new Scan("person"), "PID", pid),
                          "Person_PID", "PID"), // Join result:case
                      "DateEvict", false); // Sort
        
        if (op.moveNext())
            return new Result(op.current());
        return null;
	}
	
    /* NOTE */
	
	// SELECT Case_CID as CID, null as summary, NID, Content, Timestamp, Person_PID as PID,
    // null as name, User_Username as Username FROM note WHERE NID = ?;
	public final Note getNoteByNID(final int id) {
	    Operator op = new OuterJoin(new OuterJoin(
                          new Rename(new Select<Integer>(new Scan("note"), "NID", id), "User_Username", "Username"),
                          new Scan("case"), "Case_CID", "CID"), // OuterJoin note:case
                      new Scan("person"),
                      "Person_PID", "PID"); // OuterJoin note:person
	    
	    if (op.moveNext())
	        return new Note(op.current());
	    return null;
    }
	
	// SELECT CID, summary, NID, Content, Timestamp, PID, Name, User_Username as Username
    // FROM dmdb2014.case, note LEFT OUTER JOIN person ON PID = Person_PID WHERE CID = Case_CID AND CID = ? ORDER BY PID, Timestamp LIMIT ?, 5;
	//
	// SELECT COUNT(*) as number FROM dmdb2014.case, note LEFT OUTER JOIN person ON PID = Person_PID
    // WHERE CID = Case_CID AND CID = ?;
	public final Pair<List<Note>, Integer> getNoteByCID(final int id, final int start) {
        Operator op = new Limit(new Sort(new Sort(new Project(new OuterJoin(
                          new Join(
                              new Rename(new Scan("note"), "User_Username", "Username"),
                              new Select<Integer>(new Scan("case"), "CID", id),
                              "Case_CID", "CID"), // Join note:case
                          new Scan("person"),
                          "Person_PID", "PID"), // OuterJoin note:person
                      new String[] { "CID", "Summary", "NID", "Content", "Timestamp", "PID", "Name", "Username" }), // Project
                      "Timestamp", false), "PID", false), 5, start); // Sort; Limit
        
        final List<Note> notes = new ArrayList<Note>();
        while (op.moveNext())
            notes.add(new Note(op.current()));
        
        Operator count = new Count(new Select<Integer>(new Scan("note"), "Case_CID", id));
        
        if (count.moveNext())
            return new Pair<List<Note>, Integer>(notes, count.current().getInt("Count"));
        return new Pair<List<Note>, Integer>(notes, 0);
	}
    
	// SELECT CID, summary, NID, Content, Timestamp, PID, Name, User_Username as Username "
    // FROM dmdb2014.case RIGHT OUTER JOIN note ON CID = Case_CID, person WHERE PID = Person_PID AND PID = ? ORDER BY CID, Timestamp LIMIT ?, 5;");
    //
    // SELECT COUNT(*) as number FROM dmdb2014.case RIGHT OUTER JOIN note ON CID = Case_CID, person "
    // WHERE PID = Person_PID AND PID = ?;
	public final Pair<List<Note>, Integer> getNoteByPID(final int id, final int start) {
        Operator op = new Limit(new Sort(new Sort(new Project(new OuterJoin(
                          new Join(
                              new Rename(new Scan("note"), "User_Username", "Username"),
                              new Select<Integer>(new Scan("person"), "PID", id),
                              "Person_PID", "PID"), // Join note:person
                          new Scan("case"),
                          "Case_CID", "CID"), // OuterJoin note:case
                      new String[] { "CID", "Summary", "NID", "Content", "Timestamp", "PID", "Name", "Username" }), // Project
                      "Timestamp", false), "CID", false), 5, start); // Sort; Limit
        
        final List<Note> notes = new ArrayList<Note>();
        while (op.moveNext())
            notes.add(new Note(op.current()));
        
        Operator count = new Count(new Select<Integer>(new Scan("note"), "Person_PID", id));
        
        if (count.moveNext())
            return new Pair<List<Note>, Integer>(notes, count.current().getInt("Count"));
        return new Pair<List<Note>, Integer>(notes, 0);
	}
	
	// SELECT CatName FROM category WHERE NOT exists
    // (SELECT * FROM belongs WHERE Case_CID = ? AND CatName = Category_CatName) ORDER BY MainCategory_MCatName;");
    public final List<String> getCategories(int id) {
        Operator op = new Sort(new Minus(
                        new Scan("category"), new Project(new Join(
                              new Scan("category"),
                              new Select<Integer>(new Scan("belongs"), "Case_CID", id),
                              "CatName", "Category_CatName"), // Join category:belongs
                      new String[] { "CatName", "MainCategory_MCatName" })), // Project
                      "MainCategory_MCatName"); // Sort
        
        final List<String> categories = new ArrayList<String>();
        while (op.moveNext())
            categories.add(op.current().getText("CatName"));
        return categories;
    }
    
    // SELECT CatName FROM category WHERE exists
    // (SELECT * FROM belongs WHERE Case_CID = ? AND CatName = Category_CatName) ORDER BY MainCategory_MCatName;
    public final List<String> getRemCategories(int id) {
        Operator op = new Project(new Sort(new Join(
                          new Scan("category"),
                          new Select<Integer>(new Scan("belongs"), "Case_CID", id),
                          "CatName", "Category_CatName"), // Join category:belongs
                      "MainCategory_MCatName"), // Sort
                      new String[] { "CatName" }); // Project
        
        final List<String> categories = new ArrayList<String>();
        while (op.moveNext())
            categories.add(op.current().getText("CatName"));
        return categories;
    }
    
    // SELECT password from user where Username = ?;
    public final String getPassword(String name) {
        Operator op = new Select<String>(new Scan("user"), "Username", name);
        
        if (op.moveNext())
            return op.current().getText("Password");
        return null;
    }
    
    // SELECT email from user where Username = ?;
    public final String getEmail(String name) {
        Operator op = new Select<String>(new Scan("user"), "Username", name);
        
        if (op.moveNext())
            return op.current().getText("Email");
        return null;
    }
    
    /* SEARCH */
    
    // SELECT * FROM all_cases WHERE Summary LIKE ? OR Description LIKE ?;
    public final List<Case> searchBySummary(String name) {
        Operator op = new Group(new Union(
                          new Search<String>(all_cases(), "Summary", name),
                          new Search<String>(all_cases(), "Description", name)),
                      "CID"); // Distinct
        
        final List<Case> cases = new ArrayList<Case>();
        while (op.moveNext())
            cases.add(new Case(op.current()));
        return cases;
    }
    
    // SELECT DISTINCT * FROM all_cases WHERE ConCatName LIKE ? OR ConMainCat LIKE ?;
   public final List<Case> searchByCategory(String category) {
       Operator op = new Group(new Union(
                         new Search<String>(all_cases(), "ConCatName", category),
                         new Search<String>(all_cases(), "ConMainCat", category)),
                     "CID"); // Distinct
       
       final List<Case> cases = new ArrayList<Case>();
       while (op.moveNext())
           cases.add(new Case(op.current()));
       return cases;
    }
    
    // SELECT * FROM all_cases WHERE Location LIKE ?;
    public final List<Case> searchByLocation(String name) {
        Operator op = new Search<String>(all_cases(), "Location", name);
        
        final List<Case> cases = new ArrayList<Case>();
        while (op.moveNext())
            cases.add(new Case(op.current()));
        return cases;
    }
    
    // SELECT * FROM person WHERE Name LIKE ?;
    public final List<Person> searchByName(String name) {
        Operator op = new Search<String>(new Scan("person"), "Name", name);
        
        final List<Person> persons = new ArrayList<Person>();
        while (op.moveNext())
            persons.add(new Person(op.current()));
        return persons;
    }
    
    // SELECT DISTINCT PID, Name, Birthdate, Address, PhoneNumber FROM person, involve WHERE Person_PID = PID AND Role LIKE ?;
    public final List<Person> searchByRole(String role) {
        Operator op = new Group(new Project(new Join(
                          new Scan("person"),
                          new Search<String>(new Scan("involve"), "Role", role),
                          "PID", "Person_PID"), // Join person:involve
                      new String[] { "PID", "Name", "Birthdate", "Address", "PhoneNumber" }), // Project
                      "PID"); // Distinct
        
        final List<Person> persons = new ArrayList<Person>();
        while (op.moveNext())
            persons.add(new Person(op.current()));
        return persons;
    }
    
    // SELECT CID, Summary, ConvictionType, Evict, DateEvict, DateEnforcement, DateRelease, Person_PID as PID, Name
    // FROM dmdb2014.case, result, person WHERE Case_CID = CID AND Person_PID = PID AND ConvictionType LIKE ?;
    public final List<Result> searchByConviction(String conviction) {
        Operator op = new Project(new Join(
                          new Scan("case"), new Join(
                              new Scan("person"),
                              new Search<String>(new Scan("result"), "ConvictionType", conviction),
                              "PID", "Person_PID"), // Join person:involve
                          "CID", "Case_CID"), // Join case:involve
                      new String[] { "CID", "Summary", "ConvictionType", "Evict", "DateEvict", "DateEnforcement", "DateRelease", "PID", "Name" }); // Project
        
        final List<Result> results = new ArrayList<Result>();
        while (op.moveNext())
            results.add(new Result(op.current()));
        return results;
    }
    
    // SELECT CID, Summary, ConvictionType, Evict, DateEvict, DateEnforcement, DateRelease, Person_PID as PID, Name
    // FROM dmdb2014.case, result, person WHERE Case_CID = CID AND Person_PID = PID AND Evict LIKE ?;
    public final List<Result> searchByEvict(String evict) {
        Operator op = new Project(new Join(
                          new Scan("case"), new Join(
                              new Scan("person"),
                              new Search<String>(new Scan("result"), "Evict", evict),
                              "PID", "Person_PID"), // Join person:involve
                          "CID", "Case_CID"), // Join case:involve
                      new String[] { "CID", "Summary", "ConvictionType", "Evict", "DateEvict", "DateEnforcement", "DateRelease", "PID", "Name" }); // Project
        
        final List<Result> results = new ArrayList<Result>();
        while (op.moveNext())
            results.add(new Result(op.current()));
        return results;
    }
    
    /* INSERTS */
    
    // INSERT INTO note (Case_CID, Person_PID, User_Username, Content) VALUES (?, ?, ?, ?);
    public final int addNote(final int cid, final int pid, final String username, final String content) {
        String caseId = cid == 0? null: Integer.toString(cid);
        String personId = pid == 0? null: Integer.toString(pid);
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
        return new Insert("note").append(new String[][] {{ null, content, caseId, personId, format.format(new Date()), username }});
    }
	
    // INSERT INTO dmdb2014.case VALUES (null, ?, ?, ?, ?, ?);
    // INSERT INTO subscribe (User_Username, Case_CID) VALUES (?, ?);
    public final int addCase(final String username , final String caseSum, final Date date, final String location, final String caseDescr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
        String formatDate = date != null? format.format(date): null;
        Insert ins = new Insert("case");
        if (ins.append(new String[][] {{ null, formatDate, location, caseDescr, "0", caseSum }}) == 1) {
            int id = ins.getAutoInc().pollFirst();
            if (new Insert("subscribe").append(new String[][] {{ username, Integer.toString(id) }}) == 1)
                return id;
        }
        return -1;
    }
    
    // INSERT INTO person VALUES (null, ?, ?, ?, ?);
    public final int addPerson(final String name , final Date date, final String address, final String phonenumber) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
        String formatDate = date != null? format.format(date): null;
        Insert ins = new Insert("person");
        if (ins.append(new String[][] {{ null, name, formatDate, address, phonenumber }}) == 1)
            return ins.getAutoInc().pollFirst();
        return -1;
    }
    
    // INSERT INTO subscribe (User_Username, Case_CID) VALUES (?, ?);
    public final int addSubscribe(String username, int id) {
        return new Insert("subscribe").append(new String[][] {{ username, Integer.toString(id) }});
    }
    
    // INSERT INTO belongs VALUES (?, ?);
    public final int addToCat(String category, int id) {
        return new Insert("belongs").append(new String[][] {{ Integer.toString(id), category }});
    }
    
    /* REPLACE */
    
    // INSERT INTO user VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE Email = ?, Password = ?;
    public final int regUser(String username, String pw, String email) {
        // TODO change to "on duplicate key update"
        
        // Count occurrences
        Operator count = new Select<String>(new Scan("user"), "Username", username);
        int result = -1;
        if (count.moveNext()) {
            // Duplicate key
            Operator op = new Select<String>(new Scan("user"), "Username", username);
            HashMap<String, String> colVal = new HashMap<String, String>();
            colVal.put("Password", pw);
            colVal.put("Email", email);
            result = new Update("user").update(op, colVal);
            return result == 1 ? 2 : result;
        }
        else
            return new Insert("user").append(new String[][] {{ username, pw, email }});
    }
    
    // INSERT INTO involve VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE Role = ?;
    public final int addInvolve(int cid, int pid, String role) {
        // TODO change to "on duplicate key update"
        
        // Count occurrences
        Operator count = new Select<Integer>(new Select<Integer>(
                             new Scan("involve"), "Case_CID", cid),
                             "Person_PID", pid);
        
        if (count.moveNext()) {
            // Duplicate key
            Operator op = new Select<Integer>(new Select<Integer>(
                              new Scan("involve"), "Case_CID", cid),
                              "Person_PID", pid);
            HashMap<String, String> colVal = new HashMap<String, String>();
            colVal.put("Role", role);
            return new Update("involve").update(op, colVal);
        }
        else
            return new Insert("involve").append(new String[][] {{ Integer.toString(cid), Integer.toString(pid), role }});
    }
    
    // INSERT INTO result VALUES (?, ?, ?, ?, ?, ?, ?)
    // ON DUPLICATE KEY UPDATE ConvictionType = ?, Evict = ?, DateEvict = ?, DateEnforcement = ?, DateRelease = ?;
    public int Conv(Integer pid, Integer cid, String convType, String evict,
            Date dateEvict, Date dateEnf, Date dateRelease) {
        // TODO change to "on duplicate key update"
        
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
        String formatEvict = dateEvict != null? format.format(dateEvict): null;
        String formatEnf = dateEnf != null? format.format(dateEnf): null;
        String formatRelease = dateRelease != null? format.format(dateRelease): null;
        
        // Count occurrences
        Operator count = new Select<Integer>(new Select<Integer>(
                             new Scan("result"), "Person_PID", pid),
                             "Case_CID", cid);
        
        if (count.moveNext()) {
            // Duplicate key
            Operator op = new Select<Integer>(new Select<Integer>(
                              new Scan("result"), "Person_PID", pid),
                              "Case_CID", cid);
            HashMap<String, String> colVal = new HashMap<String, String>();
            colVal.put("ConvictionType", convType);
            colVal.put("Evict", evict);
            colVal.put("DateEvict", formatEvict);
            colVal.put("DateEnforcement", formatEnf);
            colVal.put("DateRelease", formatRelease);
            return new Update("result").update(op, colVal);
        }
        else
            return new Insert("result").append(new String[][] {{ Integer.toString(pid), Integer.toString(cid), convType, evict,
                                                                 formatEvict, formatEnf, formatRelease }});
    }
    
    /* UPDATE */
    
    // UPDATE dmdb2014.case SET Status = ? WHERE CID = ?;
    public final int setStatus(int id, boolean status) {
        Operator op = new Select<Integer>(new Scan("case"), "CID", id);
        HashMap<String, String> colVal = new HashMap<String, String>();
        colVal.put("Status", status? "1": "0");
        return new Update("case").update(op, colVal);
    }
    
    // UPDATE dmdb2014.case SET Summary = ?, DateTime = ?, Location = ?, Description = ? WHERE CID = ?;
    public final int editCase(String caseSum, Date date, String location, String caseDescr, int id) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
        String formatDate = date != null? format.format(date): null;
        Operator op = new Select<Integer>(new Scan("case"), "CID", id);
        HashMap<String, String> colVal = new HashMap<String, String>();
        colVal.put("Summary", caseSum);
        colVal.put("DateTime", formatDate);
        colVal.put("Location", location);
        colVal.put("Description", caseDescr);
        return new Update("case").update(op, colVal);
    }
    
    // UPDATE person SET Name = ?, Birthdate = ?, Address = ?, PhoneNumber = ? WHERE PID = ?;
    public final int editPerson(String name, Date date, String address, String phonenumber, int id) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
        String formatDate = date != null? format.format(date): null;
        Operator op = new Select<Integer>(new Scan("person"), "PID", id);
        HashMap<String, String> colVal = new HashMap<String, String>();
        colVal.put("Name", name);
        colVal.put("Birthdate", formatDate);
        colVal.put("Address", address);
        colVal.put("PhoneNumber", phonenumber);
        return new Update("person").update(op, colVal);
    }
    
    // UPDATE note SET Content = ? WHERE NID = ?;
    public final int editNote(String content, int id) {
        Operator op = new Select<Integer>(new Scan("note"), "NID", id);
        HashMap<String, String> colVal = new HashMap<String, String>();
        colVal.put("Content", content);
        return new Update("note").update(op, colVal);
    }
    
    /* DELETE */
    
    // DELETE FROM belongs WHERE Case_CID = ? AND Category_CatName=?;
    public final int removeFromCat(String category, int id) {
        return new Delete(new Select<String>(new Select<Integer>(
                   new Scan("belongs"), "Case_CID", id),
                   "Category_CatName", category)).deleteAll();
    }
    
    // DELETE FROM subscribe WHERE Case_CID = ? AND User_Username = ?;
    public final int removeSubscribe(String username, int id) {
        return new Delete(new Select<String>(new Select<Integer>(
                   new Scan("subscribe"), "Case_CID", id),
                   "User_Username", username)).deleteAll();
    }
    
    // DELETE FROM involve WHERE Case_CID = ? AND Person_PID = ?
    public final int removeInvolvement(int cid, int pid) {
        return new Delete(new Select<Integer>(new Select<Integer>(
                   new Scan("involve"), "Person_PID", pid),
                   "Case_CID", cid)).deleteAll();
    }
    
    // DELETE FROM note WHERE NID = ?;
    public final int removeNote(int id) {
        return new Delete(new Select<Integer>(new Scan("note"), "NID", id)).deleteAll();
    }
    
    // DELETE FROM user WHERE username = ?;
    public final int remUser(String username) {
        return new Delete(new Select<String>(new Scan("user"), "Username", username)).deleteAll();
    }
    
    // DELETE FROM result WHERE Person_PID = ? AND Case_CID = ?;
    public int remConv(Integer pid, Integer cid) {
        return new Delete(new Select<Integer>(new Select<Integer>(
                new Scan("result"), "Person_PID", pid),
                "Case_CID", cid)).deleteAll();
    }
}
