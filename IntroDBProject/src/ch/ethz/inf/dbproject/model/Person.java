package ch.ethz.inf.dbproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;

public final class Person {
    
    private final int id;
    private final String name;
    private final Date birthdate;
    private final String address;
    private final String phonenumber;
    
    /**
     * Construct a new person.
     * 
     * @param description       The name of the person
     */
    public Person(final int id, final String name, final Date birthdate, final String address, final String phonenumber) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.address = address;
        this.phonenumber = phonenumber;
    }
    
    public Person(final Tuple t) {
        this.id = t.getInt("PID");
        this.name = t.getText("Name");
        this.birthdate = t.getTimestamp("Birthdate");
        this.address = t.getText("Address");
        this.phonenumber = t.getText("PhoneNumber");
    }
    
    public Person(final ResultSet rs) throws SQLException {
        this.id = rs.getInt("PID");
        this.name = rs.getString("Name");
        this.birthdate = rs.getTimestamp("Birthdate");
        this.address = rs.getString("Address");
        this.phonenumber = rs.getString("PhoneNumber");
    }
    
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBirthdate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        if (birthdate != null)
            return sdf.format(birthdate);
        else return "";
    }
    
    public String getBirthdateUSFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (birthdate != null)
            return sdf.format(birthdate);
        else return "";
    }

    public String getAddress() {
        return address;
    }

    public String getPhonenumber() {
        return phonenumber;
    }
    
}