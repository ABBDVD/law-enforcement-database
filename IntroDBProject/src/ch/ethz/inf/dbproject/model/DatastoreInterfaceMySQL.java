package ch.ethz.inf.dbproject.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ch.ethz.inf.dbproject.database.MySQLConnection;
import ch.ethz.inf.dbproject.util.Pair;

/**
 * This class should be the interface between the web application
 * and the database. Keeping all the data-access methods here
 * will be very helpful for part 2 of the project.
 */
public final class DatastoreInterfaceMySQL {
    
	private Connection sqlConnection;

	public DatastoreInterfaceMySQL() {
	    
		this.sqlConnection = MySQLConnection.getInstance().getConnection();
		try {
            getCase = sqlConnection.prepareStatement("SELECT * FROM all_cases;");
            getSubCase = sqlConnection.prepareStatement("SELECT * FROM all_cases, subscribe WHERE User_Username = ? AND Case_CID = CID && status = false;");
            getNewsFeed = sqlConnection.prepareStatement("SELECT CID, summary, NID, Content, Timestamp, PID, Name, n.User_Username as Username "
                    + "FROM dmdb2014.case, subscribe s, note n LEFT OUTER JOIN person ON PID = Person_PID "
                    + "WHERE CID = n.Case_CID AND CID = s.Case_CID AND Status = false AND s.User_Username = ? ORDER BY Timestamp DESC LIMIT 0, 3;");
            getCaseByID = sqlConnection.prepareStatement("SELECT * FROM all_cases WHERE CID = ?;");
            
            // filter status
            getCasesByStatus = sqlConnection.prepareStatement("SELECT * FROM all_cases WHERE status = ?;");
            // order by DateTime
            getRecentCases = sqlConnection.prepareStatement("SELECT * FROM all_cases ORDER BY DateTime desc;");
            // Get open cases, order by DateTime
            getOldOpenCases = sqlConnection.prepareStatement("SELECT * FROM all_cases WHERE Status = false ORDER BY DateTime;");
            
            // Get person by PID
            getPersonByID = sqlConnection.prepareStatement("SELECT PID, Name, Birthdate, Address, PhoneNumber "
                    + "FROM person WHERE PID = ?;");
            // Get Involvement by CID
            getInvolvementByCID = sqlConnection.prepareStatement("SELECT * FROM all_cases, involve i, person "
                    + "WHERE i.Case_CID = CID AND Person_PID = PID AND CID = ? GROUP BY PID ORDER BY Role;");
            // Get Involvement by PID
            getInvolvementByPID = sqlConnection.prepareStatement("SELECT * FROM all_cases, involve i, person "
                    + "WHERE i.Case_CID = CID AND Person_PID = PID AND PID = ? GROUP BY CID ORDER BY DateTime DESC;");
            // Get Result by PID
            getResultByPID = sqlConnection.prepareStatement("SELECT CID, Summary, ConvictionType, Evict, DateEvict, DateEnforcement, "
                    + "DateRelease, Person_PID as PID, Name FROM dmdb2014.case, result, person WHERE Case_CID = CID AND "
                    + "Person_PID = ? AND Person_PID = PID ORDER BY DateEvict DESC;");
            // Get Result by CID
            getResultByCID = sqlConnection.prepareStatement("SELECT CID, Summary, ConvictionType, Evict, DateEvict, DateEnforcement, "
                    + "DateRelease, Person_PID as PID, Name FROM dmdb2014.case, result, person "
                    + "WHERE Case_CID = CID AND Person_PID = PID AND Case_CID = ? ORDER BY DateEvict DESC;");
            // Get Result by PID & CID
            getSpecResult = sqlConnection.prepareStatement("SELECT CID, Summary, ConvictionType, Evict, DateEvict, DateEnforcement, DateRelease, Person_PID as PID, Name "
                    + "FROM dmdb2014.case, result, person WHERE Case_CID = CID AND Person_PID = PID AND Case_CID = ? AND Person_PID = ? ORDER BY DateEvict DESC;");
            // Get Note by NID
            getNoteByNID = sqlConnection.prepareStatement("SELECT Case_CID as CID, null as summary, NID, Content, Timestamp, Person_PID as PID, "
                    + "null as name, User_Username as Username FROM note WHERE NID = ?;");
            // Get Note by CID
            getNoteByCID = sqlConnection.prepareStatement("SELECT CID, summary, NID, Content, Timestamp, PID, Name, User_Username as Username "
                    + "FROM dmdb2014.case, note LEFT OUTER JOIN person ON PID = Person_PID WHERE CID = Case_CID AND CID = ? ORDER BY PID, Timestamp LIMIT ?, 5;");
            // Get Note by PID
            getNoteByPID = sqlConnection.prepareStatement("SELECT CID, summary, NID, Content, Timestamp, PID, Name, User_Username as Username "
                    + "FROM dmdb2014.case RIGHT OUTER JOIN note ON CID = Case_CID, person WHERE PID = Person_PID AND PID = ? ORDER BY CID, Timestamp LIMIT ?, 5;");
            // Count Note by CID
            countNoteByCID = sqlConnection.prepareStatement("SELECT COUNT(*) as number FROM dmdb2014.case, note LEFT OUTER JOIN person ON PID = Person_PID "
                    + "WHERE CID = Case_CID AND CID = ?;");
            // Count Note by CID
            countNoteByPID = sqlConnection.prepareStatement("SELECT COUNT(*) as number FROM dmdb2014.case RIGHT OUTER JOIN note ON CID = Case_CID, person "
                    + "WHERE PID = Person_PID AND PID = ?;");
            
            // get all category names
            getCats = sqlConnection.prepareStatement("SELECT CatName FROM category WHERE NOT exists "
                    + "(SELECT * FROM belongs WHERE Case_CID = ? AND CatName = Category_CatName) ORDER BY MainCategory_MCatName;");
            getRemCats = sqlConnection.prepareStatement("SELECT CatName FROM category WHERE exists "
                    + "(SELECT * FROM belongs WHERE Case_CID = ? AND CatName = Category_CatName) ORDER BY MainCategory_MCatName;");
            // Insert Note
            addNote = sqlConnection.prepareStatement("INSERT INTO note (Case_CID, Person_PID, User_Username, Content)"
                    + "VALUES (?, ?, ?, ?);");
            
            // get Password of certain user:
            getPw = sqlConnection.prepareStatement("SELECT password from user where Username = ?;");
            getEmail = sqlConnection.prepareStatement("SELECT email from user where Username = ?;");
            
            //search
            searchBySummary = sqlConnection.prepareStatement("SELECT * FROM all_cases WHERE Summary LIKE ? OR Description LIKE ?;");
            searchByCat = sqlConnection.prepareStatement("SELECT DISTINCT * FROM all_cases WHERE ConCatName LIKE ? OR ConMainCat LIKE ?;");
            searchByLocation = sqlConnection.prepareStatement("SELECT * FROM all_cases WHERE Location LIKE ?;");
            searchByName = sqlConnection.prepareStatement("SELECT * FROM person WHERE Name LIKE ?;");
            searchByRole = sqlConnection.prepareStatement("SELECT DISTINCT PID, Name, Birthdate, Address, PhoneNumber FROM person, involve WHERE Person_PID = PID AND Role LIKE ?;");
            searchByConviction = sqlConnection.prepareStatement("SELECT CID, Summary, ConvictionType, Evict, DateEvict, DateEnforcement, "
                    + "DateRelease, Person_PID as PID, Name FROM dmdb2014.case, result, person WHERE Case_CID = CID AND Person_PID = PID AND ConvictionType LIKE ?;");
            searchByEvict = sqlConnection.prepareStatement("SELECT CID, Summary, ConvictionType, Evict, DateEvict, DateEnforcement, "
                    + "DateRelease, Person_PID as PID, Name FROM dmdb2014.case, result, person WHERE Case_CID = CID AND Person_PID = PID AND Evict LIKE ?;");
            
            // "User operations"
            regUser = sqlConnection.prepareStatement("INSERT INTO user VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE Email = ?, Password = ?;");
            
            // add
            addCase = sqlConnection.prepareStatement("INSERT INTO dmdb2014.case "
                    + "VALUES (null, ?, ?, ?, "
                    + "?, ?);", Statement.RETURN_GENERATED_KEYS);
            addPerson = sqlConnection.prepareStatement("INSERT INTO person "
                    + "VALUES (null, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            addSubscribe = sqlConnection.prepareStatement("INSERT INTO subscribe (User_Username, Case_CID) "
                    + "VALUES (?, ?);");
            addInvolve = sqlConnection.prepareStatement("INSERT INTO involve VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE Role = ?;");
            addToCat = sqlConnection.prepareStatement("INSERT INTO belongs VALUES (?, ?);");
            
            // remove
            removeFromCat = sqlConnection.prepareStatement("DELETE FROM belongs WHERE Case_CID = ? AND Category_CatName=?;");
            removeSubscribe = sqlConnection.prepareStatement("DELETE FROM subscribe WHERE Case_CID = ? AND User_Username = ?;");
            removeInvolvement = sqlConnection.prepareStatement("DELETE FROM involve WHERE Case_CID = ? AND Person_PID = ?;");
            removeNote = sqlConnection.prepareStatement("DELETE FROM note WHERE NID = ?;");
            remUser = sqlConnection.prepareStatement("DELETE FROM user WHERE username = ?;");
            
            // set ("edit")
            setStatus = sqlConnection.prepareStatement("UPDATE dmdb2014.case SET Status = ? WHERE CID = ?;");
            editCase = sqlConnection.prepareStatement("UPDATE dmdb2014.case SET Summary = ?, DateTime = ?, Location = ?, Description = ? WHERE CID = ?;");
            editPerson = sqlConnection.prepareStatement("UPDATE person SET Name = ?, Birthdate = ?, Address = ?, PhoneNumber = ? WHERE PID = ?;");
            editNote = sqlConnection.prepareStatement("UPDATE note SET Content = ? WHERE NID = ?;");
            Conv = sqlConnection.prepareStatement("INSERT INTO result VALUES (?, ?, ?, ?,?, ?, ?) "
                    + "ON DUPLICATE KEY UPDATE ConvictionType = ?, Evict = ?, "
                    + "DateEvict = ?, DateEnforcement = ?, DateRelease = ?;");
            remConv = sqlConnection.prepareStatement("DELETE FROM result WHERE Person_PID = ? AND Case_CID = ?;");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	// prepared statements:
	PreparedStatement getCase = null;
    PreparedStatement getSubCase = null;
    PreparedStatement getNewsFeed = null;
	PreparedStatement getCaseByID = null;
	PreparedStatement getCasesByStatus = null;
	PreparedStatement getRecentCases = null;
	PreparedStatement getOldOpenCases = null;
	PreparedStatement getPw = null;
	PreparedStatement getEmail = null;
	
	PreparedStatement getPersonByID = null;
	PreparedStatement getInvolvementByPID = null;
	PreparedStatement getInvolvementByCID = null;
	PreparedStatement getResultByPID = null;
	PreparedStatement getResultByCID = null;
	PreparedStatement getSpecResult = null;

    PreparedStatement getNoteByNID = null;
    PreparedStatement getNoteByPID = null;
    PreparedStatement getNoteByCID = null;
    PreparedStatement countNoteByCID = null;
    PreparedStatement countNoteByPID = null;
    
    PreparedStatement getCats = null;
    PreparedStatement getRemCats = null;
    
	PreparedStatement addNote = null;
    PreparedStatement addCase = null;
    PreparedStatement addPerson = null;
	PreparedStatement addSubscribe = null;
	PreparedStatement addInvolve = null;
	PreparedStatement addToCat = null;
	
	PreparedStatement removeFromCat = null;
	PreparedStatement removeSubscribe = null;
    PreparedStatement removeInvolvement = null;
    PreparedStatement removeNote = null;
	PreparedStatement remUser = null;
	
	PreparedStatement searchByCat = null;
	PreparedStatement searchByMainCat = null;
	PreparedStatement searchBySummary = null;
	PreparedStatement searchByLocation = null;
    PreparedStatement searchByName = null;
    PreparedStatement searchByRole = null;
    PreparedStatement searchByConviction = null;
    PreparedStatement searchByEvict = null;
	
	PreparedStatement regUser = null;
	
	PreparedStatement setStatus = null;
    PreparedStatement editCase = null;
    PreparedStatement editPerson = null;
    PreparedStatement editNote = null;
    PreparedStatement Conv = null;
    PreparedStatement remConv = null;
	
	
	public final Case getCaseById(final int id) {
	
	    try {
	        getCaseByID.setInt(1, id);
            final ResultSet rs = getCaseByID.executeQuery();
            while (rs.next()){
                return new Case(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	    return null;
		
	}
	
	public final List<Case> getAllCases() {

			try {
			    final List<Case> cases = new ArrayList<Case>();
	            final ResultSet rs = getCase.executeQuery();
                while (rs.next()){
                    cases.add(new Case(rs));
                }
                return cases;
            } catch (SQLException e) {
                e.printStackTrace();
            }
			return null;
    }
	
	public final List<Case> getSubscribedCases(String username) {

        try {
            final List<Case> cases = new ArrayList<Case>();
            getSubCase.setString(1, username);
            final ResultSet rs = getSubCase.executeQuery();
            while (rs.next()){
                cases.add(new Case(rs));
            }
            return cases;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
	}
	
    public final List<Note> getNewsFeed(final String username) {
        try {
            final List<Note> notes = new ArrayList<Note>();
            getNewsFeed.setString(1, username);
            final ResultSet rs = getNewsFeed.executeQuery();
            while (rs.next()) {
                notes.add(new Note(rs));
            }
            return notes;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
	
	public final List<Case> getCasesByStatus(boolean stat) {

            try {
                final List<Case> cases = new ArrayList<Case>();
                getCasesByStatus.setBoolean(1, stat);
                final ResultSet rs = getCasesByStatus.executeQuery();
                while (rs.next()){
                    cases.add(new Case(rs));
                }
                return cases;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
    }
	
	public final List<Case> getMostRecentCases() {

            try {
                final List<Case> cases = new ArrayList<Case>();
                final ResultSet rs = getRecentCases.executeQuery();
                while (rs.next()){
                    cases.add(new Case(rs));
                }
                return cases;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
    }
	
	public final List<Case> getOldestUnsolvedCases() {

        try {
            final List<Case> cases = new ArrayList<Case>();
            final ResultSet rs = getOldOpenCases.executeQuery();
            while (rs.next()){
                cases.add(new Case(rs));
            }
            return cases;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
	}
	
	// PERSON
   public final Person getPersonById(final int id) {
        try {
            getPersonByID.setInt(1, id);
            final ResultSet rs = getPersonByID.executeQuery();
            while (rs.next()){
                return new Person(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
   
   public final List<Involvement> getInvolvementByPID(final int id) {
       try {
           final List<Involvement> involvements = new ArrayList<Involvement>();
           getInvolvementByPID.setInt(1, id);
           final ResultSet rs = getInvolvementByPID.executeQuery();
           while (rs.next()) {
               involvements.add(new Involvement(rs));
           }
           return involvements;
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
    }
   
   public final List<Involvement> getInvolvementByCID(final int id) {
       try {
           final List<Involvement> involvements = new ArrayList<Involvement>();
           getInvolvementByCID.setInt(1, id);
           final ResultSet rs = getInvolvementByCID.executeQuery();
           while (rs.next()) {
               involvements.add(new Involvement(rs));
           }
           return involvements;
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
    }
   
   public final List<Result> getResultByPID(final int id) {
       try {
           final List<Result> results = new ArrayList<Result>();
           getResultByPID.setInt(1, id);
           final ResultSet rs = getResultByPID.executeQuery();
           while (rs.next()) {
               results.add(new Result(rs));
           }
           return results;
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
    }
   
   public final List<Result> getResultByCID(final int id) {
       try {
           final List<Result> results = new ArrayList<Result>();
           getResultByCID.setInt(1, id);
           final ResultSet rs = getResultByCID.executeQuery();
           while (rs.next()) {
               results.add(new Result(rs));
           }
           return results;
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
    }
   
   public final Result getSpecResult (final int cid, final int pid) {
       try {
           
           getSpecResult.setInt(1, cid);
           getSpecResult.setInt(2, pid);
           final ResultSet rs = getSpecResult.executeQuery();
           if (rs != null && rs.next()) {
               return new Result(rs);
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
   }
   
    // NOTE
   public final Note getNoteByNID(final int id) {
       try {
           getNoteByNID.setInt(1, id);
           final ResultSet rs = getNoteByNID.executeQuery();
           while (rs.next()) {
               return new Note(rs);
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
    }
   
   public final Pair<List<Note>, Integer> getNoteByCID(final int id, final int start) {
       try {
           final List<Note> notes = new ArrayList<Note>();
           getNoteByCID.setInt(1, id);
           getNoteByCID.setInt(2, start);
           final ResultSet rs = getNoteByCID.executeQuery();
           while (rs.next()) {
               notes.add(new Note(rs));
           }
           countNoteByCID.setInt(1, id);
           final ResultSet count = countNoteByCID.executeQuery();
           while (count.next()) {
               return new Pair<List<Note>, Integer>(notes, count.getInt("number"));
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
    }
   
   public final Pair<List<Note>, Integer> getNoteByPID(final int id, final int start) {
        try {
            final List<Note> notes = new ArrayList<Note>();
            getNoteByPID.setInt(1, id);
            getNoteByPID.setInt(2, start);
            final ResultSet rs = getNoteByPID.executeQuery();
            while (rs.next()) {
                notes.add(new Note(rs));
            }
            countNoteByPID.setInt(1, id);
            final ResultSet count = countNoteByPID.executeQuery();
            while (count.next()) {
                return new Pair<List<Note>, Integer>(notes, count.getInt("number"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
     }
   
   public final List<String> getCategories(int id) {

       try {
           getCats.setInt(1, id);
           final List<String> categories = new ArrayList<String>();
           final ResultSet rs = getCats.executeQuery();
           while (rs.next()){
               categories.add(rs.getString(1));
           }
           return categories;
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
   }
   public final List<String> getRemCategories(int id) {

       try {
           getRemCats.setInt(1, id);
           final List<String> categories = new ArrayList<String>();
           final ResultSet rs = getRemCats.executeQuery();
           while (rs.next()){
               categories.add(rs.getString(1));
           }
           return categories;
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
   }
   
   
   public final int addNote(final int cid, final int pid, final String username, final String content) {
       try {
           if (cid != 0) addNote.setInt(1, cid);
           else addNote.setNull(1, java.sql.Types.INTEGER);
           if (pid != 0) addNote.setInt(2, pid);
           else addNote.setNull(2, java.sql.Types.INTEGER);
           addNote.setString(3, username);
           addNote.setString(4, content);
           return addNote.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return 0;
    }
	
   // Add Case
   public final int addCase(final String username , final String caseSum, final Timestamp t, final String location, final String caseDescr) {
       try {
           addCase.setTimestamp(1, t);
           addCase.setString(2, location);
           addCase.setString(3, caseDescr);
           addCase.setBoolean(4, false);
           addCase.setString(5, caseSum);
           
           addSubscribe.setString(1, username);
           int ret;
           ret = addCase.executeUpdate();
           if (ret==1) {
               ResultSet rs = addCase.getGeneratedKeys();
               int id;
               if (rs != null && rs.next()) {
                   id = (int) rs.getLong(1);
               }
               else id = -1;
               addSubscribe.setString(1, username);
               addSubscribe.setInt(2, id);
               ret = addSubscribe.executeUpdate();
               if (ret==1) return id;
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return -1;
    }
   
   // Add Person
   public final int addPerson(final String name , final Timestamp t, final String address, final String phonenumber) {
       try {
           addPerson.setString(1, name);
           addPerson.setTimestamp(2, t);
           addPerson.setString(3, address);
           addPerson.setString(4, phonenumber);
           int ret;
           ret = addPerson.executeUpdate();
           if (ret == 1) {
               ResultSet rs = addPerson.getGeneratedKeys();
               if (rs != null && rs.next()) {
                   return (int) rs.getLong(1);
               }
               else return -1;
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return -1;
    }
   
   public final int addSubscribe(String username, int id)  {
       try {
           addSubscribe.setString(1, username);
           addSubscribe.setInt(2, id);
           
           return addSubscribe.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return 0;
   }
   
   public final int addInvolve(int cid, int pid, String role)  {
       try {
           addInvolve.setInt(1, cid);
           addInvolve.setInt(2, pid);
           addInvolve.setString(3, role);
           addInvolve.setString(4, role);
           
           return addInvolve.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return -1;
   }
   
   public final int addToCat(String category, int id)  {
       try {
           addToCat.setInt(1, id);
           addToCat.setString(2, category);
           return addToCat.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return 0;
   }
   
   public final int removeFromCat(String category, int id)  {
       try {
           removeFromCat.setInt(1, id);
           removeFromCat.setString(2, category);
           return removeFromCat.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return 0;
   }
   
   public final int removeSubscribe(String username, int id)  {
       try {
           removeSubscribe.setInt(1, id);
           removeSubscribe.setString(2, username);
           return removeSubscribe.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return 0;
   }
   
   public final int removeInvolvement(int cid, int pid)  {
       try {
           removeInvolvement.setInt(1, cid);
           removeInvolvement.setInt(2, pid);
           return removeInvolvement.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return 0;
   }
   
   public final int removeNote(int nid)  {
       try {
           removeNote.setInt(1, nid);
           return removeNote.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return 0;
   }
   
   public final int remUser(String username)  {
       try {
           remUser.setString(1, username);
           return remUser.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return 0;
   }
   
	// SEARCH
   public final List<Case> searchBySummary(String name) {
       try {
           searchBySummary.setString(1, "%" + name + "%");
           searchBySummary.setString(2, "%" + name + "%");
           final List<Case> cases = new ArrayList<Case>();
           final ResultSet rs = searchBySummary.executeQuery();
           while (rs.next()){
               cases.add(new Case(rs));
           }
           return cases;
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
   }
   
   public final List<Case> searchByCategory(String category) {
       try {
            searchByCat.setString(1, "%" + category + "%");
            searchByCat.setString(2, "%" + category + "%");
            final List<Case> cases = new ArrayList<Case>();
            final ResultSet rs = searchByCat.executeQuery();
            while (rs.next()){
                cases.add(new Case(rs));
            }
            return cases;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
	}
	
	public final List<Case> searchByLocation(String name) {
        try {
            searchByLocation.setString(1, "%" + name + "%");
            final List<Case> cases = new ArrayList<Case>();
            final ResultSet rs = searchByLocation.executeQuery();
            while (rs.next()){
                cases.add(new Case(rs));
            }
            return cases;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
	
    public final List<Person> searchByName(String name) {
        try {
            searchByName.setString(1, "%" + name + "%");
            final List<Person> persons = new ArrayList<Person>();   
            final ResultSet rs = searchByName.executeQuery();
            while (rs.next()){
                persons.add(new Person(rs));
            }
            return persons;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public final List<Person> searchByRole(String role) {
        try {
            searchByRole.setString(1, "%" + role + "%");
            final List<Person> persons = new ArrayList<Person>();   
            final ResultSet rs = searchByRole.executeQuery();
            while (rs.next()){
                persons.add(new Person(rs));
            }
            return persons;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public final List<Result> searchByConviction(String conviction) {
        try {
            searchByConviction.setString(1, "%" + conviction + "%");
            final List<Result> results = new ArrayList<Result>();   
            final ResultSet rs = searchByConviction.executeQuery();
            while (rs.next()){
                results.add(new Result(rs));
            }
            return results;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public final List<Result> searchByEvict(String evict) {
        try {
            searchByEvict.setString(1, "%" + evict + "%");
            final List<Result> results = new ArrayList<Result>();   
            final ResultSet rs = searchByEvict.executeQuery();
            while (rs.next()){
                results.add(new Result(rs));
            }
            return results;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
	
	public final String getPassword(String name) {
        try {
            getPw.setString(1, name); 
            final ResultSet rs = getPw.executeQuery();
            while (rs.next()) return rs.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
	public final String getEmail(String name) {
        try {
            getEmail.setString(1, name); 
            final ResultSet rs = getEmail.executeQuery();
            while (rs.next()) return rs.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
	
	public final int regUser(String username, String pw, String email){
	    try {
            regUser.setString(1, username);
            regUser.setString(2, pw);
            regUser.setString(3, email);
            regUser.setString(4, email);
            regUser.setString(5, pw);
            final int rs = regUser.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
	}
	
	public final int setStatus(int id, boolean Status){
        try {
            setStatus.setBoolean(1, Status);
            setStatus.setInt(2, id);
            
            final int rs = setStatus.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
	
	public final int editCase(String caseSum, Timestamp t, String location, String caseDescr, int id) {
        try {
            editCase.setString(1, caseSum);
            editCase.setTimestamp(2, t);
            editCase.setString(3,  location);
            editCase.setString(4, caseDescr);
            editCase.setInt(5, id);
            
            final int rs = editCase.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
	
    public final int editPerson(String name, Timestamp t, String address, String phonenumber, int id) {
        try {
            editPerson.setString(1, name);
            editPerson.setTimestamp(2, t);
            editPerson.setString(3,  address);
            editPerson.setString(4, phonenumber);
            editPerson.setInt(5, id);
            
            final int rs = editPerson.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
    
    public final int editNote(String content, int id) {
        try {
            editNote.setString(1, content);
            editNote.setInt(2, id);
            
            final int rs = editNote.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int Conv(Integer pid, Integer cid, String convType, String evict,
            Timestamp tEvict, Timestamp tEnf, Timestamp tRelease) {
        try {
            Conv.setInt(1, pid);
            Conv.setInt(2, cid);
            Conv.setString(3,  convType);
            Conv.setString(4, evict);
            Conv.setTimestamp(5, tEvict);
            Conv.setTimestamp(6, tEnf);
            Conv.setTimestamp(7, tRelease);
            Conv.setString(8, convType);
            Conv.setString(9, evict);
            Conv.setTimestamp(10, tEvict);
            Conv.setTimestamp(11, tEnf);
            Conv.setTimestamp(12, tRelease);
            
            final int rs = Conv.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
    
    public int remConv(Integer pid, Integer cid) {
        try {
            remConv.setInt(1, pid);
            remConv.setInt(2, cid);
            return remConv.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
