package ch.ethz.inf.dbproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;

public final class Involvement {
    
    // Case Properties
    private final int cid;
    private final Date datetime;
    private final String location;
    private final boolean status;
    private final String summary;
    private final String category;
    private final String maincategory;
    
    // Involement Properties
    private final String role;
    
    // Person Properties
    private final int pid;
    private final String name;
    
    /**
     * Construct a new case.
     * 
     * @param description       The name of the case
     */
    public Involvement(final int cid, final Date datetime, final String location, final boolean status, final String summary,
            final String category, final String maincategory, String role, final int pid, final String name) {
        this.cid = cid;
        this.datetime = datetime;
        this.location = location;
        this.status = status;
        this.summary = summary;
        this.category = category;
        this.maincategory = maincategory;
        this.role = role;
        this.pid = pid;
        this.name = name;
    }
    
    public Involvement(final Tuple t) {
        this.cid = t.getInt("CID");
        this.datetime = t.getTimestamp("DateTime");
        this.location = t.getText("Location");
        this.status  = t.getBoolean("Status");
        this.summary = t.getText("Summary");
        this.category = t.getText("ConCatName") == null ? "" : t.getText("ConCatName");
        this.maincategory = t.getText("ConMainCat") == null ? "" : t.getText("ConMainCat");
        this.role = t.getText("Role");
        this.pid = t.getInt("PID");
        this.name = t.getText("Name");
    }
    
    public Involvement(final ResultSet rs) throws SQLException {
        this.cid = rs.getInt("CID");
        this.datetime = rs.getTimestamp("DateTime");
        this.location = rs.getString("Location");
        this.status  = rs.getBoolean("Status");
        this.summary = rs.getString("Summary");
        this.category = rs.getString("ConCatName") == null ? "" : rs.getString("ConCatName");
        this.maincategory = rs.getString("ConMainCat") == null ? "" : rs.getString("ConMainCat");
        this.role = rs.getString("Role");
        this.pid = rs.getInt("PID");
        this.name = rs.getString("Name");
    }

    public int getCid() {
        return cid;
    }

    public String getDatetime() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm, dd.MM.yyyy");
        if (datetime != null)
            return sdf.format(datetime);
        else return "";
    }

    public String getLocation() {
        return location;
    }

    public String getStatus() {
        return status ? "Closed" : "Opened";
    }
    public boolean getBoolStatus() {
        return status;
    }

    public String getSummary() {
        return summary;
    }

    public String getCategory() {
        return category;
    }

    public String getMaincategory() {
        return maincategory;
    }

    public String getRole() {
        return role;
    }

    public int getPid() {
        return pid;
    }
    
    public String getName() {
        return name;
    }
}