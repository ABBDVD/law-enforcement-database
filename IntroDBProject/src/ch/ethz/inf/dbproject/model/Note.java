package ch.ethz.inf.dbproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;

/**
 * Object that represents a conviction.
 */
public class Note {

    // Case Properties
    private final int cid;
    private final String summary;
    
    // Note Properties
    private final int nid;
    private final String content;
    private final Date timestamp;
    
    // Person Properties
    private final int pid;
    private final String name;
    
    // User Properties
    private final String username;
    
    public Note(final int cid, final String summary, final int nid, final String content, final Date timestamp,
            final int pid, final String name, final String username) {
        this.cid = cid;
        this.summary = summary;
        this.nid = nid;
        this.content = content;
        this.timestamp = timestamp;
        this.pid = pid;
        this.name = name;
        this.username = username;
    }
    
    public Note(final Tuple t) {
        this.cid = t.getInt("CID");
        this.summary = t.getText("Summary");
        this.nid = t.getInt("NID");
        this.content = t.getText("Content");
        this.timestamp = t.getTimestamp("Timestamp");
        this.pid = t.getInt("PID");
        this.name = t.getText("Name");
        this.username = t.getText("Username");
    }
    
    public Note(final ResultSet rs) throws SQLException {
        this.cid = rs.getInt("CID");
        this.summary = rs.getString("Summary");
        this.nid = rs.getInt("nid");
        this.content = rs.getString("content");
        this.timestamp = rs.getTimestamp("timestamp");
        this.pid = rs.getInt("PID");
        this.name = rs.getString("name");
        this.username = rs.getString("username");
    }

    public int getCid() {
        return cid;
    }

    public String getSummary() {
        return summary;
    }

    public int getNid() {
        return nid;
    }

    public String getContent() {
        return content;
    }

    public String getTimestamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm, dd-MM-yyyy");
        return sdf.format(timestamp);
    }

    public int getPid() {
        return pid;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return (username == null) ? "Nonexistant" : username;
    }
    
}
