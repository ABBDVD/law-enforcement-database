package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.util.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.util.Misc;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class of Case list page
 */
@WebServlet(description = "The home page of the project", urlPatterns = { "/Cases" })
public final class CasesServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CasesServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) 
			throws ServletException, IOException {

		final HttpSession session = request.getSession(true);

		/*******************************************************
		 * Construct a table to present all our results
		 *******************************************************/
		final BeanTableHelper<Case> table = new BeanTableHelper<Case>("cases", "casesTable", Case.class);
		table.addBeanColumn("Case Summary", "summary");
		table.addBeanColumn("Location", "location");
		table.addBeanColumn("Status", "status");
		table.addBeanColumn("Category", "category");
		table.addBeanColumn("Maincategory", "maincategory");
		table.addBeanColumn("Occured at", "datetime");
		table.addLinkColumn("", "View Case", "Case?id=", "id");

		// The filter parameter defines what to show on the Projects page
		final String filter = request.getParameter("filter");
		final String category = request.getParameter("category");

		if (filter == null && category == null) {

			// If no filter is specified, then we display all the cases!
			table.addObjects(dbInterface.getAllCases());

		} else if (category != null) {

			table.addObjects(dbInterface.searchByCategory(category));
			
		} else if (filter != null) {
		
			if(filter.equals("open")) {

				table.addObjects(dbInterface.getCasesByStatus(false));

			} else if (filter.equals("closed")) {

				table.addObjects(dbInterface.getCasesByStatus(true));

			} else if (filter.equals("recent")) {
				table.addObjects(dbInterface.getMostRecentCases());

			}
			
			else if (filter.equals("oldest")) {

			    table.addObjects(dbInterface.getOldestUnsolvedCases());

			}
		} else {
			throw new RuntimeException("Code should not be reachable!");
		}
		 // check if we actually found any cases
        String resultsStr = table.toString();
        if (!resultsStr.contains("<td>")) session.setAttribute("cases", "<h4>No cases found!</h4>");
        else session.setAttribute("cases", table);
        
		// Finally, proceed to the Projects.jsp page which will render the Projects
		this.getServletContext().getRequestDispatcher("/Cases.jsp").forward(request, response);
	}
	@Override
	protected final void doPost(final HttpServletRequest request, final HttpServletResponse response) 
            throws ServletException, IOException {

        final HttpSession session = request.getSession(true);
        final String loggedUser = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);
        
        session.setAttribute("newCaseFormError", null);
        final String action = request.getParameter("action");
        if (action != null && action.trim().equals("addCase") && loggedUser != null) {
            final String caseSum = request.getParameter("caseSum");
            final String location = request.getParameter("location");
            final String caseDescr = request.getParameter("caseDescr");
            final String time = request.getParameter("time");
            final String date = request.getParameter("date");
            
            if (caseSum.length() == 0 || time.length() == 0 || date.length() == 0)
                session.setAttribute("newCaseFormError", "Case summary, time and date are required fields.");
            else if (caseSum.length() > 90)
                session.setAttribute("newCaseFormError", "Case summary may be no longer than 90 characters (currently " + caseSum.length() + ").");  
            else if (location.length() > 90)
                session.setAttribute("newCaseFormError", "Location may be no longer than 90 characters (currently " + location.length() + ").");
            else if (caseDescr.length() > 60000)
                session.setAttribute("newCaseFormError", "Case description may be no longer than 60000 characters (currently " + caseDescr.length() + ").");
            else {
                // convert time and date to Timestamp
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm yyyy-MM-dd");
                Date parsedDate = new Date();
                try {
                    parsedDate = dateFormat.parse(time + " " + date);
                    Timestamp t = new Timestamp(parsedDate.getTime());
                    if (Misc.validDate(t)) {
                        int id = dbInterface.addCase(loggedUser, caseSum, t, location, caseDescr);
                        if (id == -1) session.setAttribute("newCaseFormError", "Unhandled SQL Error when submitting this case.");
                        else {
                            session.setAttribute("ChangeMess", "Case was successfully create and you were subscribed for it.");
                            response.sendRedirect("Case?id=" + id);
                            return;
                        }
                    }
                    else {
                        session.setAttribute("newCaseFormError", "Please make sure the date is in the correct range between 02.01.1970 and 18.01.2038 .");
                    }
                } catch (ParseException e) {
                    session.setAttribute("newCaseFormError", "Date / Time format is wrong.");
                    e.printStackTrace();
                }
            }
        }
        this.getServletContext().getRequestDispatcher("/Cases.jsp").forward(request, response);
        session.setAttribute("newCaseFormError", null);
	}
}