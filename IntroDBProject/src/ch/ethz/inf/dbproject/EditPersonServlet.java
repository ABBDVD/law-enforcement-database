package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.util.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Person;

/**
 * Servlet implementation class of EditPerson page
 */
@WebServlet(description = "Edit a specific person", urlPatterns = { "/EditPerson" })
public final class EditPersonServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private final DatastoreInterface dbInterface = new DatastoreInterface();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditPersonServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) 
            throws ServletException, IOException {
        
        doPost(request, response);
    }
    
    @Override
    protected final void doPost(final HttpServletRequest request, final HttpServletResponse response) 
            throws ServletException, IOException {

        final HttpSession session = request.getSession(true);
        final String loggedUser = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);
        
        final String idString = request.getParameter("id");
        if (idString == null || loggedUser == null) {
            response.sendRedirect("./Cases");
            return;
        }
        
        final Integer id = Integer.parseInt(idString);
        session.setAttribute("id", id);
        
        try {
            Person person = this.dbInterface.getPersonById(id);
            session.setAttribute("person", person);
        } catch (final Exception ex) {
            ex.printStackTrace();
            response.sendRedirect("./Cases");
            return;
        }
        
        session.setAttribute("newPersonFormError", null);
        final String action = request.getParameter("action");
        if (action != null && action.trim().equals("editPerson") && loggedUser != null) {
            final String name = request.getParameter("name");
            final String birthdate = request.getParameter("birthdate");
            final String address = request.getParameter("address");
            final String phonenumber = request.getParameter("phonenumber");
            
            if (name.length() == 0)
                session.setAttribute("newPersonFormError", "Name is a required field.");
            else if (name.length() > 45)
                session.setAttribute("newPersonFormError", "Name may be no longer than 45 characters (currently " + name.length() + ").");  
            else if (address.length() > 90)
                session.setAttribute("newPersonFormError", "Address may be no longer than 90 characters (currently " + address.length() + ").");
            else if (phonenumber.length() > 20)
                session.setAttribute("newPersonFormError", "Phone number may be no longer than 20 characters (currently " + phonenumber.length() + ").");
            else {
                // convert birthdatedate to Timestamp
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date parsedDate = new Date();
                try {
                    Timestamp t = null;
                    if (birthdate.length() > 0) {
                        parsedDate = dateFormat.parse(birthdate);
                        t = new Timestamp(parsedDate.getTime());
                    }
                    int result = dbInterface.editPerson(name, t, address, phonenumber, id);
                    if (result != 0 && result != 1) session.setAttribute("newPersonFormError", "Unhandled SQL Error when submitting this person.");
                    else {
                        session.setAttribute("ChangeMess", "Person was successfully edited.");
                        response.sendRedirect("Person?id=" + id);
                        return;
                    }
                } catch (ParseException e) {
                    session.setAttribute("newPersonFormError", "Date format is wrong.");
                    e.printStackTrace();
                }
            }
        }
        this.getServletContext().getRequestDispatcher("/EditPerson.jsp").forward(request, response);
    }
}