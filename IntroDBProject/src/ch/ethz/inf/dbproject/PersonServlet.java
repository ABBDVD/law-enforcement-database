package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Note;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Person;
import ch.ethz.inf.dbproject.model.Involvement;
import ch.ethz.inf.dbproject.model.Result;
import ch.ethz.inf.dbproject.util.Pair;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class of Case Details Page
 */
@WebServlet(description = "Displays a specific person.", urlPatterns = { "/Person" })
public final class PersonServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private final DatastoreInterface dbInterface = new DatastoreInterface();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PersonServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        final HttpSession session = request.getSession(true);
        
        String loggedUser = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);
        
        final String idString = request.getParameter("id");
        if (idString == null) {
            response.sendRedirect("./Cases");
            return;
        }

        try {
            
            final Integer id = Integer.parseInt(idString);
            session.setAttribute("id", id);
            
            final Person person = dbInterface.getPersonById(id);
            
            /*******************************************************
             * Construct a table to present all properties of a person
             *******************************************************/
            
            // Overview
            final BeanTableHelper<Person> table = new BeanTableHelper<Person>("cases", "casesTable", Person.class);
            table.addBeanColumn("Name", "name");
            table.addBeanColumn("Birthdate", "birthdate");
            table.addBeanColumn("Address", "address");
            table.addBeanColumn("Phone number", "phonenumber");
            table.addObject(person);
            table.setVertical(true);
            session.setAttribute("personTable", table);

            // Involved cases
            List<Involvement> involvements = dbInterface.getInvolvementByPID(id);
            if (!involvements.isEmpty()) {
                final BeanTableHelper<Involvement> involvedTable = new BeanTableHelper<Involvement>("cases", "casesTable", Involvement.class);
                involvedTable.addBeanColumn("Role", "role");
                involvedTable.addBeanColumn("Case Summary", "summary");
                involvedTable.addBeanColumn("Location", "location");
                involvedTable.addBeanColumn("Status", "status");
                involvedTable.addBeanColumn("Category", "category");
                involvedTable.addBeanColumn("Maincategory", "maincategory");
                involvedTable.addBeanColumn("Occured at", "datetime");
                involvedTable.addLinkColumn("", "View Case", "Case?id=", "cid");
                // If logged in: Add Note
                if (loggedUser != null) {
                    involvedTable.addLinkColumn("", "Add Case specific Note", "Note?pid=" + id + "&cid=", "cid");
                    involvedTable.addLinkColumn("", "Choose Conviction", "Conviction?pid=" + id + "&cid=", "cid");
                }
                involvedTable.addObjects(involvements);
                session.setAttribute("involvementTable", involvedTable);
            }
            else
                session.setAttribute("involvementTable", null);
            
            // Criminal Record
            List<Result> results = dbInterface.getResultByPID(id);
            if (!results.isEmpty()) {
                final BeanTableHelper<Result> resultsTable = new BeanTableHelper<Result>("cases", "casesTable", Result.class);
                resultsTable.addBeanColumn("Case Summary", "summary");
                resultsTable.addBeanColumn("Conviction Type", "convictionType");
                resultsTable.addBeanColumn("Evict", "evict");
                resultsTable.addBeanColumn("Date of Evict", "dateEvict");
                resultsTable.addBeanColumn("Date of Enforcement", "dateEnforcement");
                resultsTable.addBeanColumn("Date of Release", "dateRelease");
                resultsTable.addLinkColumn("", "View Case", "Case?id=", "cid");
                if (loggedUser != null) resultsTable.addLinkColumn("", "Edit Conviction", "Conviction?pid=" + id + "&cid=", "cid");
                resultsTable.addObjects(results);
                session.setAttribute("resultTable", resultsTable);
            }
            else
                session.setAttribute("resultTable", null);
            
            // Notes
            final String startString = request.getParameter("start");
            final int start;
            if (startString != null) start = Integer.parseInt(startString);
            else start = 0;
            Pair<List<Note>, Integer> p = dbInterface.getNoteByPID(id, start);
            List<Note> notes = p.a;
            int count = p.b;
            if (!notes.isEmpty()) {
                String noteTables = "";
                for (Note n: notes) {
                    BeanTableHelper<Note> t = new BeanTableHelper<Note>("notes", "notesTable", Note.class);
                    t.addBeanColumn("Author", "username");
                    t.addBeanColumn("Date", "timestamp");
                    if (n.getCid() != 0)
                        t.addBeanColumn("<a href=\"Case?id=" + n.getCid() + "\">Case</a>", "summary");
                    t.addBeanColumn("Note", "content");
                    t.addObject(n);
                    t.setVertical(true);
                    noteTables += t;
                    
                    if (loggedUser != null && loggedUser.equals(n.getUsername())) {
                        noteTables += "<a href=\"Note?pid=" + id + "&id=" + n.getNid() + "&action=editNote\">Edit Note</a>&nbsp;&nbsp;|"
                                    + "&nbsp;&nbsp;<a href=\"Note?pid=" + id + "&nid=" + n.getNid() + "&action=deleteNote\">Delete Note</a><br />";
                    }
                    
                    noteTables += "<br />";
                }
                session.setAttribute("noteTables", noteTables);
            }
            else
                session.setAttribute("noteTables", null);
            // Next and Previous button
            String pageButtons = "";
            if (start > 0) {
                // Previous button needed
                pageButtons += "<a href=\"?id=" + id + "&start=" + (start - 5 >= 0? start - 5: 0) + "#notes\">Previous</a>";
                if (start < count - 5)
                    // Both buttons needed
                    pageButtons += " | ";
            }
            if (start < count - 5)
                // Next button needed
                pageButtons += "<a href=\"?id=" + id + "&start=" + (start + 5) + "#notes\">Next</a>";
            session.setAttribute("pageButtons", pageButtons);
            
        } catch (final Exception ex) {
            ex.printStackTrace();
            response.sendRedirect("./Cases");
            return;
        }

        this.getServletContext().getRequestDispatcher("/Person.jsp").forward(request, response);
        session.setAttribute("Mess", null);
    }
}