package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Involvement;
import ch.ethz.inf.dbproject.model.Note;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.model.Result;
import ch.ethz.inf.dbproject.util.Pair;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class of Case Details Page
 */
@WebServlet(description = "Displays a specific case.", urlPatterns = { "/Case" })
public final class CaseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CaseServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

		final HttpSession session = request.getSession(true);
        
        String loggedUser = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);

		final String idString = request.getParameter("id");
		if (idString == null) {
		    response.sendRedirect("./Cases");
			return;
		}

		try {
		    
			final Integer id = Integer.parseInt(idString);
	        session.setAttribute("id", id);
	        session.setAttribute("canSubscribe", false);
	        
			Case aCase = this.dbInterface.getCaseById(id);
			session.setAttribute("caseStatus", aCase.getBoolStatus() ? "true" : "false");
			
            if (loggedUser != null){
                // subscribe "mechanic"
                session.setAttribute("canSubscribe", true);
                List<Case> subscribed = dbInterface.getSubscribedCases(loggedUser);
                for (Case c: subscribed) {
                    if (c.getId() == id) {
                        session.setAttribute("canSubscribe", false);
                        break;
                    }
                }
                if (request.getParameter("subscribe") != null) { // is he subscribing?
                    if (request.getParameter("subscribe").equals("true")){
                        int result = dbInterface.addSubscribe(loggedUser, id);
                        if (result == 1 || result == 0){ // subscribe!
                            session.setAttribute("ChangeMess", "You are now subscribed to this case.");
                            session.setAttribute("canSubscribe", false);
                        }
                        else {
                            session.setAttribute("ChangeMess", "Unknown error during subscription.");
                        }
                    }
                    else if (request.getParameter("subscribe").equals("false")) {
                        int result = dbInterface.removeSubscribe(loggedUser, id);
                        if (result == 1 || result == 0){ // subscribe!
                            session.setAttribute("ChangeMess", "You are now not subscribed to this case anymore.");
                            session.setAttribute("canSubscribe", true);
                        }
                        else {
                            session.setAttribute("ChangeMess", "Unknown error during subscription.");
                        }
                    }
                }
                // change category
                String categoryOptions = "";
                List<String> categories = dbInterface.getCategories(id);
                for (String s: categories) categoryOptions += "<option value=\"" + s + "\">" + s + "</option> ";
                session.setAttribute("categories", categoryOptions);
                
                categories = dbInterface.getRemCategories(id);
                categoryOptions = "";
                for (String s: categories) categoryOptions += "<option value=\"" + s + "\">" + s + "</option> ";
                session.setAttribute("remCategories", categoryOptions);
                // open/close case
                if (request.getParameter("changeStatusTo") != null && (request.getParameter("changeStatusTo").equals("true") || request.getParameter("changeStatusTo").equals("false"))){
                    boolean changeStatusTo = request.getParameter("changeStatusTo").equals("true");
                    int result = dbInterface.setStatus(id, changeStatusTo);
                    if (result==0) session.setAttribute("ChangeMess", "Status not changed.");
                    else if (result == 1) session.setAttribute("ChangeMess", "Status was changed.");
                    else session.setAttribute("ChangeMess", "Unknown error occured when changing the case status.");
                    session.setAttribute("caseStatus", changeStatusTo ? "true" : "false");
                    aCase = dbInterface.getCaseById(id);
                }
            }
			
			
			
			/*******************************************************
			 * Construct a table to present all properties of a case
			 *******************************************************/
			final BeanTableHelper<Case> table = new BeanTableHelper<Case>("cases", "casesTable", Case.class);
			table.addBeanColumn("Summary", "summary");
			table.addBeanColumn("Location", "location");
			table.addBeanColumn("Occured at", "datetime");
			table.addBeanColumn("Status", "status");
			table.addBeanColumn("Category", "category");
            table.addBeanColumn("Maincategory", "maincategory");
			table.addBeanColumn("Description", "description");
			table.addObject(aCase);
			table.setVertical(true);
			session.setAttribute("caseTable", table);
			
			// Involved persons
            List<Involvement> involvements = dbInterface.getInvolvementByCID(id);
            if (involvements != null && !involvements.isEmpty()) {
                final BeanTableHelper<Involvement> involvedTable = new BeanTableHelper<Involvement>(
                        "cases", "casesTable", Involvement.class);
                involvedTable.addBeanColumn("Role", "role");
                involvedTable.addBeanColumn("Name", "name");
                involvedTable.addLinkColumn("", "View Person", "Person?id=", "pid");
                if (loggedUser != null) {
                    involvedTable.addLinkColumn("", "Add Person specific Note", "Note?cid=" + id + "&pid=", "pid");
                    involvedTable.addLinkColumn("", "Choose Conviction", "Conviction?cid=" + id + "&pid=", "pid");
                    involvedTable.addLinkColumn("", "Edit Role", "AddPerson?cid=" + id + "&action=selectRole&pid=", "pid");
                }
                involvedTable.addObjects(involvements);
                session.setAttribute("involvementTable", involvedTable);
            }
            else
                session.setAttribute("involvementTable", null);
            
            // Criminal Record
            List<Result> results = dbInterface.getResultByCID(id);
            if (!results.isEmpty()) {
                final BeanTableHelper<Result> resultsTable = new BeanTableHelper<Result>("cases", "casesTable", Result.class);
                resultsTable.addBeanColumn("Convict's Name", "name");
                resultsTable.addBeanColumn("Conviction Type", "convictionType");
                resultsTable.addBeanColumn("Evict", "evict");
                resultsTable.addBeanColumn("Date of Evict", "dateEvict");
                resultsTable.addBeanColumn("Date of Enforcement", "dateEnforcement");
                resultsTable.addBeanColumn("Date of Release", "dateRelease");
                resultsTable.addLinkColumn("", "View Person", "Person?id=", "pid");
                if (loggedUser != null) resultsTable.addLinkColumn("", "Edit Conviction", "Conviction?cid=" + id + "&pid=", "pid");
                resultsTable.addObjects(results);
                session.setAttribute("resultTable", resultsTable);
            }
            else
                session.setAttribute("resultTable", null);
            
            // Notes
            final String startString = request.getParameter("start");
            final int start;
            if (startString != null) start = Integer.parseInt(startString);
            else start = 0;
            Pair<List<Note>, Integer> p = dbInterface.getNoteByCID(id, start);
            List<Note> notes = p.a;
            int count = p.b;
            if (!notes.isEmpty()) {
                String noteTables = "";
                for (Note n: notes) {
                    BeanTableHelper<Note> t = new BeanTableHelper<Note>("notes", "notesTable", Note.class);
                    t.addBeanColumn("Author", "username");
                    t.addBeanColumn("Date", "timestamp");
                    if (n.getPid() != 0)
                        t.addBeanColumn("<a href=\"Person?id=" + n.getPid() + "\">Person</a>", "name");
                    t.addBeanColumn("Note", "content");
                    t.addObject(n);
                    t.setVertical(true);
                    noteTables += t;
                    
                    if (loggedUser != null && loggedUser.equals(n.getUsername())) {
                        noteTables += "<a href=\"Note?cid=" + id + "&nid=" + n.getNid() + "&action=editNote\">Edit Note</a>&nbsp;&nbsp;|"
                                    + "&nbsp;&nbsp;<a href=\"Note?cid=" + id + "&nid=" + n.getNid() + "&action=deleteNote\">Delete Note</a><br />";
                    }
                    
                    noteTables += "<br />";
                }
                session.setAttribute("noteTables", noteTables);
            }
            else
                session.setAttribute("noteTables", null);
            // Next and Previous button
            String pageButtons = "";
            if (start > 0) {
                // Previous button needed
                pageButtons += "<a href=\"?id=" + id + "&start=" + (start - 5 >= 0? start - 5: 0) + "#notes\">Previous</a>";
                if (start < count - 5)
                    // Both buttons needed
                    pageButtons += " | ";
            }
            if (start < count - 5)
                // Next button needed
                pageButtons += "<a href=\"?id=" + id + "&start=" + (start + 5) + "#notes\">Next</a>";
			session.setAttribute("pageButtons", pageButtons);
            
		} catch (final Exception ex) {
			ex.printStackTrace();
            response.sendRedirect("./Cases");
            return;
		}

		this.getServletContext().getRequestDispatcher("/Case.jsp").forward(request, response);
		session.setAttribute("ChangeMess", "");
	}
	
	@Override
    protected final void doPost(final HttpServletRequest request, final HttpServletResponse response) 
            throws ServletException, IOException {

        final HttpSession session = request.getSession(true);
        final String loggedUser = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);
        
        String action = request.getParameter("action");
        if (action != null && loggedUser != null){
            action = action.trim();
            if (action.equals("addCategory")) {
                final String category = request.getParameter("category");
                int result = dbInterface.addToCat(category, (int) session.getAttribute("id"));
                if (result != 1){
                    session.setAttribute("ChangeMess", "Some error occured. Possibly due to umlauts in the category name (" + category + ").");
                }
                else {
                    session.setAttribute("ChangeMess", "Case was added to category.");
                }
            }
            else if(action.equals("remCategory")){
                final String category = request.getParameter("category");
                int result = dbInterface.removeFromCat(category, (int) session.getAttribute("id"));
                if (result != 1 && result != 0){
                    session.setAttribute("ChangeMess", "Some error occured. Possibly due to umlauts in the category name (" + category + ").");
                }
                else session.setAttribute("ChangeMess", "Case was removed from category.");
            }
        }
        
        
        doGet(request, response);
        //this.getServletContext().getRequestDispatcher("/Case.jsp").forward(request, response);
    }
}
