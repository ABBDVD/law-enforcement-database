package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.util.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.util.Misc;

/**
 * Servlet implementation class of EditCase page
 */
@WebServlet(description = "Edit a specific case", urlPatterns = { "/EditCase" })
public final class EditCaseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditCaseServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) 
			throws ServletException, IOException {

		final HttpSession session = request.getSession(true);

		/*******************************************************
		 * Construct a table to present all our results
		 *******************************************************/
		
		String loggedUser = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);

        final String idString = request.getParameter("id");
        if (idString == null || loggedUser == null) {
            response.sendRedirect("./Cases");
            return;
        }
        final Integer id = Integer.parseInt(idString);
        session.setAttribute("id", id);
        
        try {
            Case aCase = this.dbInterface.getCaseById(id);
            session.setAttribute("aCase", aCase);
        } catch (final Exception ex) {
            ex.printStackTrace();
            response.sendRedirect("./Cases");
            return;
        }
		// Finally, proceed to the Projects.jsp page which will render the Projects
		this.getServletContext().getRequestDispatcher("/EditCase.jsp").forward(request, response);
	}
	
	@Override
	protected final void doPost(final HttpServletRequest request, final HttpServletResponse response) 
            throws ServletException, IOException {

        final HttpSession session = request.getSession(true);
        final String loggedUser = (String) session.getAttribute(UserServlet.SESSION_USER_DETAILS);
        
        final String idString = request.getParameter("id");
        if (idString == null || loggedUser == null) {
            response.sendRedirect("./Cases");
            return;
        }
        final Integer id = Integer.parseInt(idString);
        session.setAttribute("id", id);
        
        try {
            Case aCase = this.dbInterface.getCaseById(id);
            session.setAttribute("aCase", aCase);
        } catch (final Exception ex) {
            ex.printStackTrace();
            response.sendRedirect("./Cases");
            return;
        }
        
        session.setAttribute("newCaseFormError", null);
        final String action = request.getParameter("action");
        if (action != null && action.trim().equals("editCase") && loggedUser != null) {
            final String caseSum = request.getParameter("caseSum");
            final String location = request.getParameter("location");
            final String caseDescr = request.getParameter("caseDescr");
            final String time = request.getParameter("time");
            final String date = request.getParameter("date");
            
            if (caseSum.length() == 0 || time.length() == 0 || date.length() == 0)
                session.setAttribute("newCaseFormError", "Case summary, time and date are required fields.");
            else if (caseSum.length() > 90)
                session.setAttribute("newCaseFormError", "Case summary may be no longer than 90 characters (currently " + caseSum.length() + ").");  
            else if (location.length() > 90)
                session.setAttribute("newCaseFormError", "Location may be no longer than 90 characters (currently " + location.length() + ").");
            else if (caseDescr.length() > 60000)
                session.setAttribute("newCaseFormError", "Case description may be no longer than 60000 characters (currently " + caseDescr.length() + ").");
            else {
                // convert time and date to Timestamp
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm yyyy-MM-dd");
                Date parsedDate = new Date();
                try {
                    parsedDate = dateFormat.parse(time + " " + date);
                    Timestamp t = new Timestamp(parsedDate.getTime());
                    if (Misc.validDate(t)) {
                        int result = dbInterface.editCase(caseSum, t, location, caseDescr, id);
                        if (result!=0 && result != 1) session.setAttribute("newCaseFormError", "Unhandled SQL Error when submitting this case.");
                        else {
                            session.setAttribute("ChangeMess", "Case was successfully edited.");
                            response.sendRedirect("Case?id=" + id);
                            return;
                        }
                    }
                    else {
                        session.setAttribute("newCaseFormError", "Please make sure the date is in the correct range between 02.01.1970 and 18.01.2038 .");
                    }
                    
                } catch (ParseException e) {
                    session.setAttribute("newCaseFormError", "Date / Time format is wrong.");
                    e.printStackTrace();
                }
            }
        }
        else session.setAttribute("newCaseFormError", "Some error occured. Please reload the page (without submitting the from data and try again).");
        this.getServletContext().getRequestDispatcher("/EditCase.jsp").forward(request, response);
        session.setAttribute("newCaseFormError", null);
	}
}