package ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator;

import java.util.ArrayList;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Column;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;

public class Avg extends Aggregator {

    private final String column;
    private final int columnIndex;
    
    public Avg(Operator op, String column) {
        super(op);
        this.column = column;
        columnIndex = schema.getIndex(column);
    }
    
    @Override protected String aggregate(ArrayList<Tuple> list) {
        if (list != null && list.size() > 0) {
            int value = 0;
            int count = 0;
            for (Tuple t: list) {
                value += t.getInt(columnIndex);
                count++;
            }
            
            return Integer.toString(value / count);
        }
        else
            return null;
    }

    @Override protected Column aggColumn() {
        return new Column("Avg", (short)2, null, (short)-1, (short)0);
    }
    
    @Override public Avg clone(Operator op) {
        return new Avg(op, column);
    }

}
