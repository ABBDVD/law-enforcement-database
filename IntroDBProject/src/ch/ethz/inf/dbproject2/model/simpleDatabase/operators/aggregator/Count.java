package ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator;

import java.util.ArrayList;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Column;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;

public class Count extends Aggregator {

    public Count(Operator op) {
        super(op);
    }

    @Override protected String aggregate(ArrayList<Tuple> list) {
        if (list == null || list.size() == 0)
            return String.format("%1$-" + 11 + "s", Integer.toString(0));
        else
            return String.format("%1$-" + 11 + "s", Integer.toString(list.size()));
    }

    @Override protected Column aggColumn() {
        return new Column("Count", (short)2, null, (short)-1, (short)0);
    }
    
    @Override public Count clone(Operator op) {
        return new Count(op);
    }

}
