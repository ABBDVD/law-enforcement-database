package ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator;

import java.util.ArrayList;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Column;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;

public class Sum extends Aggregator {

    private final String column;
    private final int columnIndex;
    
    public Sum(Operator op, String column) {
        super(op);
        this.column = column;
        columnIndex = schema.getIndex(column);
    }
    
    @Override protected String aggregate(ArrayList<Tuple> list) {
        int value = 0;
        if (list != null && list.size() > 0)
            for (Tuple t: list)
                value += t.getInt(columnIndex);
        
        return Integer.toString(value);
    }

    @Override protected Column aggColumn() {
        return new Column("Sum", (short)2, null, (short)-1, (short)0);
    }
    
    @Override public Sum clone(Operator op) {
        return new Sum(op, column);
    }

}
