package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Column;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.TupleSchema;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator.Aggregator;
import ch.ethz.inf.dbproject2.model.simpleDatabase.util.Pair;

public class Group extends Operator {
    
    private class Repeater extends Operator {
        private int offset;
        private final ArrayList<Tuple> buffer;
        public Repeater(ArrayList<Tuple> table) {
            schema = lowerSchema;
            offset = 0;
            buffer = table;
        }
        @Override
        protected Tuple computeNext() {
            while (!grouped && offset >= buffer.size())
                Thread.yield();
            if (offset < buffer.size()) {
                return buffer.get(offset++);
            }
            return null;
        }
    }
    
    private final int groupIndex;
    private final Aggregator[] aggs;
    private final LinkedHashMap<String, Pair<ArrayList<Tuple>, ArrayList<Aggregator>>> groupBuffer;
    private boolean grouped;
    private final TupleSchema lowerSchema;
    
    // Constructor for Distinct
    public Group(Operator op, String groupColumn) {
        this(op, groupColumn, null);
    }
    // Constructor for Aggregators
    public Group(Operator op, String groupColumn, Aggregator[] aggs) {
        super(op);
        groupIndex = schema.getIndex(groupColumn);
        this.aggs = aggs == null? new Aggregator[0]: aggs;
        groupBuffer = new LinkedHashMap<String, Pair<ArrayList<Tuple>, ArrayList<Aggregator>>>();
        grouped = false;
        lowerSchema = schema;
        
        if (aggs != null && aggs.length > 0) {
            final Column[] oldColumns = schema.getColumns();
            final Column[] newColumns = new Column[oldColumns.length + aggs.length];
            
            int i;
            for (i = 0; i < oldColumns.length; i++)
                newColumns[i] = oldColumns[i];
            for (Aggregator a: aggs) {
                // Aggregated value always in last column
                Column currCol = a.getSchema().getColumn(0).clone();
                currCol.setColumnName(currCol.getColumnName() + (i - oldColumns.length));
                newColumns[i] = currCol;
                i++;
            }
            schema = new TupleSchema(newColumns);
        }
    }

    @Override
    protected Tuple computeNext() {
        if (!grouped) {
            while (op.moveNext()) {
                Tuple t = op.current();
                final Pair<ArrayList<Tuple>, ArrayList<Aggregator>> pair = groupBuffer.get(t.get(groupIndex));
                ArrayList<Tuple> dataTable;
                ArrayList<Aggregator> aggTable;
                if (pair == null) {
                    dataTable = new ArrayList<Tuple>();
                    aggTable = new ArrayList<Aggregator>();
                    
                    // Add copies of all input aggregator
                    for (Aggregator a: aggs) {
                        Aggregator clone = a.clone(new Repeater(dataTable));
                        clone.start();
                        aggTable.add(clone);
                    }
                    
                    groupBuffer.put(t.get(groupIndex), new Pair<ArrayList<Tuple>, ArrayList<Aggregator>>(dataTable, aggTable));
                }
                else
                    dataTable = pair.a;

                dataTable.add(t);
            }
            
            grouped = true;
        }
        
        if (!groupBuffer.isEmpty()) {
            final String first = groupBuffer.entrySet().iterator().next().getKey();
            final Pair<ArrayList<Tuple>, ArrayList<Aggregator>> pair = groupBuffer.remove(first);
            final ArrayList<Tuple> dataTable = pair.a;
            final ArrayList<Aggregator> aggTable = pair.b;
            
            final Tuple t = dataTable.get(0);
            final String[] values = new String[schema.getColumns().length];
            
            int i;
            for (i = 0; i < values.length - aggs.length; i++)
                values[i] = t.get(i);
            for (Aggregator a: aggTable) {
                a.moveNext();
                Tuple curr = a.current();
                // Aggregated value always in last column
                values[i] = curr.get(curr.getValues().length - 1);
                i++;
            }
            
            return new Tuple(schema, values, 0, 0);
        }
        
        return null;
    }

}
