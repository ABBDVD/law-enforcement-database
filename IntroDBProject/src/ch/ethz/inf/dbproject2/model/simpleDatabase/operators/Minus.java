package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import java.util.ArrayList;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;

public class Minus extends Operator
{
    
    private final ArrayList<Tuple> right;
    private boolean collected;
    
    public Minus(Operator op, Operator op2) {
        super(op, op2);
        right = new ArrayList<Tuple>();
        collected = false;
    }

    @Override protected Tuple computeNext()
    {
        if (!collected) {
            collected = true;
            while (op2.moveNext())
                right.add(op2.current());
        }
        
        while (op.moveNext()) {
            Tuple t = op.current();
            boolean same = false;
            for (Tuple s: right)
                if (t.equiv(s)) {
                    same = true;
                    break;
                }
            
            if (!same)
                return t;
        }
        
        return null;
    }

}
