package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;

public class Union extends Operator{
    
    public Union(Operator op, Operator op2) {
        super(op, op2);
    }
    
    @Override protected Tuple computeNext()
    {
        if (op.moveNext()) {
            return op.current();
        }
        
        if (op2.moveNext()) {
            return op2.current();
        }
        
        return null;
    }

}
