package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;

public class Limit extends Operator {
    
    private final int limit;
    private final int start;
    private int offset;
    private boolean jumped;
    
    public Limit(Operator op, int limit) {
        this(op, limit, 0);
    }
    public Limit(Operator op, int limit, int start) {
        super(op);
        this.limit = limit;
        this.start = start;
        offset = 0;
        jumped = false;
    }

    @Override
    protected Tuple computeNext() {
        if (!jumped) {
            while (offset < start) {
                op.moveNext();
                offset++;
            }
            offset = 0;
            jumped = true;
        }
        
        if (offset < limit && op.moveNext()) {
            offset++;
            return op.current();
        }
        
        return null;
    }

}
