package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;

public class Rename extends Operator {
    
    public Rename(Operator op, String oldName, String newName) {
        this(op, new String[] { oldName }, new String[] { newName });
    }
    public Rename(Operator op, String[] oldNames, String[] newNames) {
        super(op);

        schema = schema.clone();        
        for (int i = 0; i < oldNames.length; i++)
            if (schema.getIndex(oldNames[i]) >= 0) {
                schema.getColumn(oldNames[i]).setColumnName(newNames[i]);
                schema.setColumnName(oldNames[i], newNames[i]);
            }
    }

    @Override
    protected Tuple computeNext() {
        if (op.moveNext()) {
            Tuple t = op.current();
            
            return new Tuple(schema, t.getValues(), t.getPageNumb(), t.getTupleNumb());
        }
        
        return null;
    }

}
