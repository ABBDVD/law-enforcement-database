package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;

/**
 * An empty sort operator
 * For the purposes of the project, you can consider only string comparisons of
 * the values.
 */
public class Sort extends Operator implements Comparator<Tuple> {

	private final int columnIndex;
	private final boolean ascending;
	private final LinkedList<Tuple> sortBuffer;
    private boolean sorted;
	
    public Sort(final Operator op, final String column) {
        this(op, column, true);
    }
	public Sort(final Operator op, final String column,	final boolean ascending) {
	    super(op);
		columnIndex = schema.getIndex(column);
		this.ascending = ascending;
		sortBuffer = new LinkedList<Tuple>();
		sorted = false;
	}
	
	@Override
	public final int compare(final Tuple l, final Tuple r) {
		final int result;
		
		String left = l.getText(columnIndex);
		String right = r.getText(columnIndex);
		if (left == null)
		    result = right == null ? 0: -1; // left value null? both values null?
		else if (right == null)
		    result = 1; // right value null only?
		else
		    result = left.compareToIgnoreCase(right); // normal comparison if both values aren't null
		
		if (ascending) {
			return result;
		} else {
			return -result;
		}
	}
	
	@Override
	protected Tuple computeNext() {
	    if (!sorted) {
            sorted = true;
	        while(op.moveNext())
	            sortBuffer.add(op.current());
	        Collections.sort(sortBuffer, this);
	    }
	    
        if (!sortBuffer.isEmpty())
            return sortBuffer.pollFirst();
        
        return null;
	}

	
}
