package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Column;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.TupleSchema;

public class Join extends Operator {
    
    private final int indexLeft;
    private final int indexRight;
    final int lengthLeft;
    final int lengthRight;
    private final HashMap<Integer, ArrayList<Tuple>> hashBuffer;
    private Iterator<Tuple> it;
    private boolean hashed;
    
    public Join(Operator op, Operator op2, String columnLeft, String columnRight) {
        super(op, op2);
        indexLeft = op.getSchema().getIndex(columnLeft);
        indexRight = op2.getSchema().getIndex(columnRight);
        lengthLeft = op.getSchema().getColumns().length;
        lengthRight = op2.getSchema().getColumns().length;
        hashBuffer = new HashMap<Integer, ArrayList<Tuple>>();
        it = null;
        hashed = false;
        
        final Column[] columnsLeft = op.getSchema().getColumns();
        final Column[] columnsRight = op2.getSchema().getColumns();
        final Column[] newColumns = new Column[columnsLeft.length + columnsRight.length];
        for (int i = 0; i < columnsLeft.length; i++)
            newColumns[i] = columnsLeft[i];
        for (int i = 0; i < columnsRight.length; i++)
            newColumns[columnsLeft.length + i] = columnsRight[i];
        schema = new TupleSchema(newColumns);
    }

    @Override
    protected Tuple computeNext() {
     // Get all elements of left operand and sort them by hash
        if (!hashed) {
            hashed = true;
            while (op.moveNext()) {
                Tuple t = op.current();
                if (t.get(indexLeft) != null) {
                    ArrayList<Tuple> table = hashBuffer.get(t.get(indexLeft).hashCode());
                    if (table == null) {
                        table = new ArrayList<Tuple>();
                        table.add(t);
                        hashBuffer.put(t.get(indexLeft).hashCode(), table);
                    }
                    else
                        table.add(t);
                }
            }
        }
        
        // Check if left operand had any elements and right operand has further elements
        if (!hashBuffer.isEmpty()) {
            do {
                while (it != null && it.hasNext()) {
                    final Tuple s = it.next();
                    final Tuple t = op2.current();
                    // TODO handle the case of null values
                    if (s.get(indexLeft) == null? t.get(indexRight) == null: s.get(indexLeft).equals(t.get(indexRight))) {
                        // Create merged tuple
                        final String[] values = new String[lengthLeft + lengthRight];
                        for (int i = 0; i < lengthLeft; i++)
                            values[i] = s.get(i);
                        for (int i = 0; i < lengthRight; i++)
                            values[lengthLeft + i] = t.get(i);
                        
                        return new Tuple(schema, values, s.getPageNumb(), s.getTupleNumb());
                    }
                }

                // List through -> Check next element
                if (it == null || !it.hasNext()) {
                    while (op2.moveNext()) {
                        String val = op2.current().get(indexRight);
                        if (val != null) {
                            final ArrayList<Tuple> table = hashBuffer.get(val.hashCode());
                            if (table != null) {
                                it = table.iterator();
                                break;
                            }
                        }
                    }
                }
            } while (it != null && it.hasNext());
        }
            
        return null;
    }

}
