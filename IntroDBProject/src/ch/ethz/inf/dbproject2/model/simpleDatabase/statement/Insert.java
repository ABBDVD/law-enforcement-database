package ch.ethz.inf.dbproject2.model.simpleDatabase.statement;

import java.io.IOException;
import java.util.LinkedList;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.TupleSchema;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;


/**
 * The scan operator reads tuples from a file. The lines in the file contain the
 * values of a tuple. The line a comma separated.
 */
public class Insert extends Statement {
    
	

	/**
	 * Insert values (append to the end of the file)
	 * @param tableName
	 */

    private final LinkedList<Integer> queue;
    
    public Insert(String tableName) {
        super(tableName);
        queue = new LinkedList<Integer>();
    }
    
    public int append(String[][] values) {
        int entriesWritten = 0;
        if (writer != null && writer.isOpen()) { // check that writer is not closed
            try {
                // TODO: shouldn't move the writer position dependent on file size but meta data instead!
                writer.position(writer.size());
            } catch (IOException e) {
                System.out.println("Setting writer position for Insert failed.");
            }
            for (String[] s: values) {
                // test if the number of values in s matches the number of columns the schema has
                if (s.length != meta.schema.getColumns().length) return -1;
                // overwrite the value of autoincrement columns, whatever there is in there.
                if (Operator.AutoIncOn && meta.getAutoIncIndex() != -1) {
                    s[meta.getAutoIncIndex()] = String.valueOf(meta.getAutoIncLast() + 1);
                    queue.add(meta.getAutoIncLast() + 1);
                }
                
                // try to write
                if (!write(s)) return -1;
                
                entriesWritten++;
            }
            close();
        }
        else return -1;
        return entriesWritten;
    }
    public int append(Operator op) {
        int entriesWritten = 0;
        if (writer != null && writer.isOpen()) { // check that writer is not closed
            try {
                writer.position(writer.size());
            } catch (IOException e) {
                System.out.println("Setting writer position for Insert failed.");
            }
            while(op.moveNext()) {
                // TODO improve this following check maybe?
                TupleSchema opSchema = op.current().getSchema(); // schema from Operator op
                if (opSchema.getColumns().length != meta.schema.getColumns().length) return -1;
                
                String [] values = op.current().getValues();
                
                // prefetch text values
                for (int i: meta.schema.getLinkedIndices()) {
                    values[i] = Tuple.getText(values[i], opSchema.getTableName(i)); // get text values from the "handed over" oprerator
                }
                
                // overwrite the value of autoincrement columns, whatever there is in there.
                if (Operator.AutoIncOn && meta.getAutoIncIndex() != -1) {
                    values[meta.getAutoIncIndex()] = String.valueOf(meta.getAutoIncLast() + 1);
                    queue.add(meta.getAutoIncLast() + 1);
                }
                
                write(values);
                entriesWritten++;
            }
            close();
        }
        else entriesWritten = -1;
        return entriesWritten;
        
    }
    
    public LinkedList<Integer> getAutoInc() {
        return queue;
    }

}
