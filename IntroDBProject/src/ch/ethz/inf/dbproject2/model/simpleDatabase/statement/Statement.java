package ch.ethz.inf.dbproject2.model.simpleDatabase.statement;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Arrays;

import ch.ethz.inf.dbproject2.model.simpleDatabase.TableMetadata;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;


/**
 * The scan operator reads tuples from a file. The lines in the file contain the
 * values of a tuple. The line a comma separated.
 */
public abstract class Statement {
    
	

	/**
	 * Insert values (append to the end of the file)
	 * @param tableName
	 */
    protected final int TuplePerPage;
    protected final TableMetadata meta;
    protected ByteBuffer textBuff; // for writing into the text file fileNr
    protected String tableName;
    protected final ByteBuffer buff[];
    protected FileChannel textWriter;
    protected final FileChannel writer;
    
    protected ByteBuffer validBuff;
    
    
    @SuppressWarnings("resource")
    protected Statement(String tableName) {
        this.tableName = tableName;
        meta = TableMetadata.getMetadata(tableName);
        FileChannel writer = null;
        try {
            File target = new File(TableMetadata.rootDirectory + tableName + ".txt");
            target.createNewFile(); // creates the file if it doesn't exist yet.
            writer = new RandomAccessFile(TableMetadata.rootDirectory + tableName + ".txt", "rw").getChannel();
        } catch (IOException e) {
            System.out.println("File not found or not able to move position to the end." + tableName);
        }
        this.writer = writer;
        TuplePerPage = (int) Math.floor((Operator.pageSize-1)/(meta.schema.getLength()));
        textBuff = null;
        
        // allocate buff
        buff = new ByteBuffer[meta.schema.getColumns().length];
        for (int i = 0; i < meta.schema.getColumns().length; i++) {
            buff[i]  =ByteBuffer.allocate(meta.schema.getColumn(i).getByteSize());
        }
        
        // ready the validBuff
        validBuff = ByteBuffer.allocate(1);
        validBuff.put( "1".getBytes()[0]);
        validBuff.flip();
    }
    
	private static final Charset cs = Charset.forName("UTF-8");
	
	protected boolean write(String[] values) {
	    // pad all values (if necessary) and add them in UTF-8 to buff (ignores linkedValues) 
        if (Tuple.adjustLength(meta.schema, values, buff) != 0) return false;
        
        // increment AutoIncrement counter if necessary
        if (Operator.AutoIncOn && meta.getAutoIncIndex() != -1) meta.incAutoIncr();
        
	    // page padding
	    ByteBuffer pad = null;
	    if (meta.getTupleNr() <= TuplePerPage-1)
            meta.setTupleNr(meta.getTupleNr()+1);
        else {
            // fill the remaining empty spaces and then add the page end character
            meta.setTupleNr(1);
            meta.incPage();
            byte[] fill = new byte[Operator.pageSize - TuplePerPage*(meta.schema.getLength())]; // create the padding
            Arrays.fill(fill, " ".getBytes(cs)[0]); // fill with empty spaces
            fill[fill.length-1] = "|".getBytes(cs)[0]; // replace last space by the "special character"
            pad = ByteBuffer.allocate(fill.length);
            pad.put(fill);
            pad.flip(); 
        }
        
        // handle linked Fields
        if (!meta.schema.getLinkedIndices().isEmpty()) {
            if (textBuff == null) textBuff = ByteBuffer.allocate(Operator.textFileSize*2); // allocate textBuff if necessary
            for (int i: meta.schema.getLinkedIndices()) {
                
                if (textWriterNeedsUpdate()) { // update textWriter if necessary
                    if (textWriter != null) { // do we have to write the last buffer?
                        textBuff.flip();
                        try {
                            textWriter.write(textBuff);
                        } catch (IOException e) {
                            System.out.println("Error writing the textFile!");
                        }
                        textBuff.clear();
                    }
                    updateTextWriter();
                }
                
                // convert the text value to utf-8 and put to textBuff
                byte[] textByte = values[i].getBytes(cs);
                textBuff.put(textByte);
                int textPosition = meta.getTextPosition();
                buff[i].put(Tuple.getLinkerValueString(meta.getFileNr(), textPosition, textByte.length).getBytes(cs)); // update the linker information and safe in buffer
                buff[i].flip();
                meta.setTextPosition(textPosition + textByte.length); // update the position for the next use
                
            }
        }
        try {
            if (pad!=null) writer.write(pad); // write page padding
	        writer.write(validBuff); // write valid Byte
            writer.write(buff); // write all values
            validBuff.position(0); // reset valid Byte Buffer

            // write textWrite if necessary
            if (textWriter != null) {
                textBuff.flip();
                textWriter.write(textBuff);
                textBuff.clear();
            }
            
            
        } catch (IOException e) {
            System.out.println("Error writing the file.");
        }
	    for (int i = 0; i < buff.length; i++) {
            buff[i].clear();
        }
	    return true;
	}
	
	protected boolean close() {
	    try {
            writer.close();
            if (textWriter != null) // close textWriter if existent
                textWriter.close();
            return true;
        } catch (IOException e) {
            System.out.println("Error closing the Insert text writers.");
        }
        return false;
	}
	
	@SuppressWarnings("resource")
    private void updateTextWriter() {
	    ByteBuffer BOM = ByteBuffer.wrap( "\ufeff".getBytes(cs));
	    int textPosition = meta.getTextPosition();
	    // update meta file if we need a new TextWriter as the last text File is full
        if (textPosition> Operator.textFileSize) {
            meta.incFileNr();
            textPosition = 0;
        }
        // get text Writer
        try {
            File textF = null;
            if (textWriter != null) {
                textF = new File(TableMetadata.rootDirectory + "texts/" + tableName + meta.getFileNr() + ".txt");
                textF.createNewFile(); // create the file if it doesn't exist yet
                if (textWriter.isOpen()) textWriter.close();
            }
            else textF = new File(TableMetadata.rootDirectory + "texts/" + tableName + meta.getFileNr() + ".txt");
            
            textWriter = new RandomAccessFile(textF, "rw").getChannel();
            if (textPosition == 0) {
                
                if (textWriter.write(BOM) != 3) // write the BOM to ensure the UTF-8 encoding
                        throw new IOException("Statement updateTextWriter: Writing of BOM failed for table " + tableName + ".");
                textPosition += 3;
                meta.setTextPosition(textPosition);
            }
            else textWriter.position(textPosition);
        } catch (IOException e) {
            System.out.println("Error creating the TextWriter for the Insert or writing the BOM to it.");
        }
	}
	
	private boolean textWriterNeedsUpdate() {
	    return textWriter == null || meta.getTextPosition()> Operator.textFileSize;
	}

}
