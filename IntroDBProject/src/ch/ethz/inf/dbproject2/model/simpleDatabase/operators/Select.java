package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;


/**
 * Selection in relational algebra. Only returns those tuple for which the given
 * column matches the value.
 * 
 * This class is a generic to allow comparing any types of values.
 * 
 * i.e. SELECT * FROM USERS WHERE USER_ID=1
 * would require these operators: 
 * 
 * Scan usersScanOperator = new Scan(filename, columnNames);
 * Select<Integer> selectOp = new Select<Integer>(
 * 		usersScanOperator, "user_id", 1);
 * 
 * Similarly, this query:
 * 
 * SELECT * FROM USERS WHERE USENAME=john
 * would require these operators:
 * 
 * Scan usersScanOperator = new Scan(filename, columnNames);
 * Select<String> selectOp = new Select<String>(
 * 		usersScanOperator, "username", "john");
 * 
 */
public class Select<T> extends Operator {

	protected final int columnIndex;
	protected final T compareValue;
	protected final boolean not;

	/**
	 * Contructs a new selection operator.
	 * @param op operator to pull from
	 * @param column column name that gets compared
	 * @param compareValue value that must be matched
	 */
	public Select(final Operator op, final String column, final T compareValue) {
	    this(op, column, compareValue, false);
	}
	public Select(final Operator op, final String column, final T compareValue, final boolean not) {
	    super(op);
		columnIndex = schema.getIndex(column);
		this.compareValue = compareValue;
		this.not = not;
	}

	protected boolean accept(final String value) {
	    return value.equals(compareValue.toString().trim());
	}
	
	@Override
	protected Tuple computeNext() {
	    while(op.moveNext()) {
            final Tuple t = op.current();
            
            boolean match = false;
            // make sure this column is actually existent in this schema - if not we return false
            if (columnIndex >= 0 && columnIndex < t.getValues().length) {
                String actualValue = t.getText(columnIndex);
                if ((actualValue == null && compareValue == null) ||
                    (actualValue != null && compareValue != null && accept(actualValue.trim()) ^ not))
                    match = true;
            }
            
            if (match)
                return t;
        }
	    
        return null;
	}

}
