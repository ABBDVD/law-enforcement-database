package ch.ethz.inf.dbproject2.model.simpleDatabase;

public class Column {
    private String columnName;
    private final short format;
    private final short size;
    /* 1 = varchar/string
     * 2 = integer
     * 3 = boolean (single character, 0 or 1)
     * 4 = TimeStamp, DateTime
     * 5 = Text (has (max) fixed size of 64'000 characters)
     */
    private final short offset;
    /* the offset from the start of the tuple
     * -1 if it's a linked column: linked columns have size .... characters and consist of page#, offset#, length#
     */
    private String tableName; // starts with
    
    /**
     * 
     * @param columnName
     * @param format
     * @param tableName
     * @param varcharSize (-1 if not applicable)
     * @param length (offset of this column)
     */
    public Column (String columnName, short format, String tableName, short varcharSize, short offset) {
        this.columnName = columnName;
        this.format = format;
        this.tableName = tableName;
        this.size = formatSize(varcharSize);
        this.offset = offset;
    }
    
    private short formatSize (short varcharSize) {
        // set the next offset depending on the length of the current column
        switch (this.format) {
            case 1:
                return varcharSize;
            case 2:
                return 11; // integer written as characters, including - in front if necessary
            case 3:
                return 1; // boolean
            case 4:
                return 14; // TimeStamp, DateTime yyyymmddhhmmss
            case 5:
                return 33; // file number with length of 11 chars, position in file (11 chars), length of text field (11 chars)
            case 6:
                return 11; // auto increment integer
            default:
                break;
        }
        return 0;
    }

    public final String getColumnName() {
        return columnName;
    }

    public final short getFormat() {
        return format;
    }

    public final short getOffset() {
        return offset;
    }

    public final String getTableName() {
        return tableName;
    }
    
    public final short getSize() {
        return size;
    }
    
    public final short getByteSize() {
        return (short) ((format==1) ? size*2 : size);
    }
    
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
    
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    
    
    /****
     *  cleanup of Tables
     * 
     * 
     ****/
    
    /**
     * constructor for making exact copy of an existing Column except changed tableName
     * @param old
     */
    private Column(Column old) {
        this.columnName = old.columnName;
        this.format = old.format;
        this.offset = old.offset;
        this.size = old.size;
        this.tableName = old.tableName;
    }
    @Override
    public Column clone() {
        return new Column(this);
    }
}
