package ch.ethz.inf.dbproject2.model.simpleDatabase.statement;

import java.io.RandomAccessFile;
import java.util.Date;

import ch.ethz.inf.dbproject2.model.simpleDatabase.TableMetadata;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;

/**
 * Projection in relational algebra. Returns tuples that contain on projected
 * columns. Therefore the new tuples conform to a new schema.
 */
public final class Delete {
    
    private static int deleteCounter; // count how many deletes were done. When deleteCounter >= deleteLimit clean up all tables
    private static int deleteLimit = 50; // every how many deletes should we do a table cleanup?
    
    /**
     * Deletes all tuples given in operator argument op<br>
     * op has to finally end up in a scan which reads from a file.<br>
     * op can have projections (case) and selections in between but despite projections the whole tuple will be deleted
     * @param op
     * @return int = some positive number if successful, 0 if no entries had to be deleted, -1 if unsuccessful
     */
    private final Operator op;
    
    public Delete(Operator op) {
        this.op = op;
    }
    
	public int deleteAll() {
	    int entriesDeleted = 0;
	    
        if (op.moveNext()) { // unrolled loop to change first execution
            Tuple t = op.current();
            if (t.getSchema().isMultiSource()) {
                    System.out.println("Tuple is \"MultiSource\"! Make sure this query has one originating table only.");
                    return -1;
            }
            String source = t.getSchema().getTableName(0);
            try {
                // as we write only 0 or 1 (which is the same in UTF-8 and ASCII), we can use RandomAccessFile directly and overwrite the single byte.
                RandomAccessFile file = new RandomAccessFile(TableMetadata.rootDirectory + source + ".txt", "rw");
                long position = t.getPosition();
                byte zeroByte = "0".getBytes()[0];
                file.seek(position);
                file.write(zeroByte);
                entriesDeleted++;
                while (op.moveNext()) {
                    t = op.current();
                    position = t.getPosition();
                    file.seek(position); // jump to the right position
                    file.write(zeroByte);
                    entriesDeleted++;
                }
                file.close();
                
            } catch (Exception e) {
                System.out.println("Problem reading / writing file in Delete.");
            }
        }
	    
	    deleteCounter+=entriesDeleted;
	    if (deleteCounter >= deleteLimit) {
	        boolean successful = TableMetadata.cleanupAll();
	        System.out.print(new Date().toString() + ": Table Cleanup was executed for all tables ");
	        if (successful) System.out.println("successfully!");
	        else System.out.println("not successfully! Table data might be corrupt!");
	        deleteCounter = 0;
	    }
		return entriesDeleted;
	}
}
