package ch.ethz.inf.dbproject2.model.simpleDatabase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Scan;
import ch.ethz.inf.dbproject2.model.simpleDatabase.statement.Insert;

/**
 * The schema contains meta data about a tuple. So far we only store the name of
 * each column. Other meta data, such cardinalities, indexes, etc. could be
 * specified here.
 */
public class TableMetadata {

	private static TableMetadata[] Metadata;
	private static HashMap<String, Integer> tableIdentifier;
	public static String rootDirectory;
	
	public TupleSchema schema;
	private int Page;
	private int TupleNr;
	private int textPosition;
	private int fileNr;
	private final int position; // in the meta file
	private String name;
	private static Charset cs = Charset.forName("UTF-8");
	private int autoIncIndex;
	private int autoIncLast;
	private int autoIncWriteOffset; // safes the offset in the meta file where the autoIncrement column is defined
	
	private TableMetadata(StringBuilder name, String data, Integer pos) {
	    position = pos;
	    
	    // split up the read data
        String[] parts = data.split("\\|"); // | has to be escaped in regex, and the regex escape character \ has to be escaped again....
        /* parts[0] = Table Name (variable length)
         * parts[1] = Number of pages (char length 10)
         * parts[2] = last Tuple Number in the last page (char length 10)
         * parts[3] = position in text file (char length 11)
         * parts[4] = fileNr (char length 11), how many textFiles there are for this table (starting with 0)
         * all following parts describe a column according to this format:
         * parts[5..] = Column format (char length 1), column varcharSize (char length 10), column name (rest of field)
         */
        name.append(parts[0].trim());
        this.name = name.toString();
        Page = Integer.parseInt(parts[1].trim());
        TupleNr = Integer.parseInt(parts[2].trim());
        textPosition = Integer.parseInt(parts[3].trim());
        fileNr = Integer.parseInt(parts[4].trim());
        String [] columnNames = new String[parts.length-5];
        short [] format = new short[parts.length-5];
        HashMap <Integer, Short> varcharSizes = new HashMap<Integer, Short>();
        autoIncIndex = -1;
        autoIncLast = -1;
        for (int i = 5; i<parts.length; i++) {
            format[i-5] = Short.parseShort(parts[i].substring(0, 1));
            
            //auto increment
            if (format[i-5] == 6) {
                autoIncIndex = i-5;
                autoIncLast = Integer.parseInt(parts[i].substring(1, 11).trim());
                autoIncWriteOffset = 1; // add one in advance for skipping the format
                for (int j = 0; j < i; j++) autoIncWriteOffset += parts[j].length() + 1;
            }
            else if (format[i-5] == 1) {
                varcharSizes.put(i-5, Short.parseShort(parts[i].substring(1, 11).trim()));
            }
            columnNames[i-5] = parts[i].substring(11);
        }
        if (autoIncIndex == -1) autoIncWriteOffset = -1;
        schema = new TupleSchema(columnNames, format, name.toString(), varcharSizes);
	    
	}
	
	@SuppressWarnings("resource")
    public static TableMetadata getMetadata(String TableName) {
	    if (Metadata == null) {
	        Operator.AutoIncOn = true; // set AutoIncOn initially
	        File root = new File(rootDirectory);
	        
	        
	        FilenameFilter notMeta = new FilenameFilter() {
	            public boolean accept(File dir, String name) {
	                String lowercaseName = name.toLowerCase();
	                if (lowercaseName.endsWith(".txt") && !lowercaseName.startsWith("meta")) {
	                    return true;
	                } else {
	                    return false;
	                }
	            }
	        };
	        String[] filteredFiles = root.list(notMeta);
	        List<String> fileNames = new LinkedList<String>();
	        if (filteredFiles != null) {
	            fileNames = Arrays.asList(filteredFiles); 
	        }
	        
	        BufferedReader metaR = null;
	        try {
	            metaR = new BufferedReader(new FileReader(rootDirectory+"meta.txt"));
	        } catch (FileNotFoundException e) {
	            System.out.println("File not found! " + rootDirectory + "meta.txt");
	        }
	        ArrayList<TableMetadata> metadataList = new ArrayList<TableMetadata>();
	        
	        /* ****
	         * create all TableMetadata objects
	         * ****/
	        tableIdentifier = new HashMap<String, Integer>();
	        Integer pos = 0;
	        String line = null;
	        
	        // for creating new data files (if necessary)
	        FileChannel FileWriter = null;
	        
	        try {
                while((line = metaR.readLine()) != null && line.length()!= 0) {
                    StringBuilder name = new StringBuilder(); // using a StringBuilder as we want to pass it to the TableMetadata constructor as object and have the actual value returned
                    
                    // create the Metadata object
                    metadataList.add(new TableMetadata(name, line, pos));
                    pos+=line.length() + 1; // +1 for the new line character
                    
                    // create the data file for the table if necessary & write the BOM to ensure UTF-8 encoding
                    if (!fileNames.contains(name + ".txt")) {
                        ByteBuffer BOM = ByteBuffer.wrap( "\ufeff".getBytes(cs));
                        System.out.println("creating non existing file:");
                        File f = new File(rootDirectory + name + ".txt");
                        f.createNewFile();
                        FileWriter = new RandomAccessFile(f, "rw").getChannel();
                        if (FileWriter.write(BOM) != 3)
                                throw new IOException("Get Metadata: Writing of BOM failed for table " + name + ".");
                        FileWriter.close();
                        
                        // ensure the metadata is correct and output a warning if it's not
                        TableMetadata temp = metadataList.get(metadataList.size()-1);
                        if (temp.TupleNr != 0 || temp.Page != 1 || temp.fileNr != 0) try {
                            throw new IOException("WARNING - Meta data file corrupt: Table " + temp.name + " has incorrect data. A new data file was allocated but other meta information is wrong.");
                        } catch (IOException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                    tableIdentifier.put(name.toString().toLowerCase(), metadataList.size()-1);
                }
            } catch (IOException e) {
                System.out.println("Meta data file corrupt.\n" + e.getMessage());
            }
	        // add one empty metadata spot for temporary cleanup metadata files
	        metadataList.add(null);
	        Metadata = metadataList.toArray(new TableMetadata[metadataList.size()]);
	        try {
                metaR.close();
            } catch (IOException e) {
                System.out.println("TableMetadata - getMetadata: Closing the meta file failed.");
            }
	    }
	    return Metadata[tableIdentifier.get(TableName.toLowerCase())];
	}
	
	public void incPage() {
	    setPage(Page+1);
	}
	
	@SuppressWarnings("resource")
    private void setPage(int oldPage) {
	    Page = oldPage;
	    try {
	        FileChannel meta = new RandomAccessFile(rootDirectory + "meta.txt", "rw").getChannel();
            meta.position(position + name.length() + 1);
            String s = String.format("%1$-" + 10 + "s", Integer.toString(Page));
            byte[] b = s.getBytes(cs);
            ByteBuffer buf = ByteBuffer.wrap(b);
            meta.write(buf);
            
            meta.close();
        } catch (FileNotFoundException e) {
            System.out.println("Meta file not found:\n" + e.getMessage());
        } catch (IOException e) {
            System.out.println("Seek/Write of meta.txt failed:\n" + e.getMessage());
        }
	}
	public void setTupleNr(int TupleNr) {
	    this.TupleNr = TupleNr;
	    try {
	        @SuppressWarnings("resource")
            FileChannel meta = new RandomAccessFile(rootDirectory + "meta.txt", "rw").getChannel();
            meta.position(position + name.length() + 12);
            String s = String.format("%1$-" + 10 + "s", Integer.toString(TupleNr));
            byte[] b = s.getBytes(cs);
            ByteBuffer buf = ByteBuffer.wrap(b);
            meta.write(buf);
            
            meta.close();
        } catch (FileNotFoundException e) {
            System.out.println("Meta file not found:\n" + e.getMessage());
        } catch (IOException e) {
            System.out.println("Seek/Write of meta.txt failed:\n" + e.getMessage());
        }
	}
	
	public void incFileNr() {
	    setFileNr(fileNr + 1);
	}
	
	public void setFileNr(int oldFileNr) {
        fileNr = oldFileNr;
        try {
            @SuppressWarnings("resource")
            FileChannel meta = new RandomAccessFile(rootDirectory + "meta.txt", "rw").getChannel();
            meta.position(position + name.length() + 35);
            String s = String.format("%1$-" + 11 + "s", Integer.toString(fileNr));
            byte[] b = s.getBytes(cs);
            ByteBuffer buf = ByteBuffer.wrap(b);
            meta.write(buf);
            
            meta.close();
        } catch (FileNotFoundException e) {
            System.out.println("Meta file not found:\n" + e.getMessage());
        } catch (IOException e) {
            System.out.println("Seek/Write of meta.txt failed:\n" + e.getMessage());
        }
    }
	
	public void setTextPosition(int TextPosition) {
        textPosition = TextPosition;
        try {
            @SuppressWarnings("resource")
            FileChannel meta = new RandomAccessFile(rootDirectory + "meta.txt", "rw").getChannel();
            meta.position(position + name.length() + 23);
            String s = String.format("%1$-" + 11 + "s", Integer.toString(textPosition));
            byte[] b = s.getBytes(cs);
            ByteBuffer buf = ByteBuffer.wrap(b);
            meta.write(buf);
            
            meta.close();
        } catch (FileNotFoundException e) {
            System.out.println("Meta file not found:\n" + e.getMessage());
        } catch (IOException e) {
            System.out.println("Seek/Write of meta.txt failed:\n" + e.getMessage());
        }
    }
	
	public void incAutoIncr() {
        autoIncLast++;
        try {
            @SuppressWarnings("resource")
            FileChannel meta = new RandomAccessFile(rootDirectory + "meta.txt", "rw").getChannel();
            meta.position(position + autoIncWriteOffset);
            String s = String.format("%1$-" + 10 + "s", Integer.toString(autoIncLast));
            byte[] b = s.getBytes(cs);
            ByteBuffer buf = ByteBuffer.wrap(b);
            meta.write(buf);
            meta.close();
        } catch (FileNotFoundException e) {
            System.out.println("Meta file not found:\n" + e.getMessage());
        } catch (IOException e) {
            System.out.println("Seek/Write of meta.txt failed:\n" + e.getMessage());
        }
    }
	
	public int getPage() {
	    return Page;
	}
	
	public int getTupleNr() {
	    return TupleNr;
	}
	
	public int getFileNr() {
	    return fileNr;
	}
	
	public int getTextPosition() {
	    return textPosition;
	}
	
	public int getAutoIncIndex() {
	    return autoIncIndex;
	}
	public int getAutoIncLast() {
	    return autoIncLast;
	}
	
	
	/****
	 *  cleanup of Tables
	 * 
	 * 
	 ****/
	
    /**
     * constructor for making exact copy of an existing TableMetadata except changed names
     * @param old
     */
    private TableMetadata(TableMetadata old) {
        autoIncIndex = old.autoIncIndex;
        autoIncLast = old.autoIncLast;
        autoIncWriteOffset = old.autoIncWriteOffset;
        fileNr = old.fileNr;
        name = old.name;
        Page = old.Page;
        position = old.position;
        textPosition = old.textPosition;
        TupleNr = old.TupleNr;
        schema = old.schema.clone();
    }
    @Override
	public TableMetadata clone() {
        return new TableMetadata(this);
    }
    
    /**
     * deletes all deleted entries in tableName
     * DO NOT USE while other operations are running on this table!
     * @param tableName
     * @return
     */
	public static boolean cleanupTable(String tableName)  {
	    // TODO: add checks which return false if this fails
	    if (Metadata==null) return false;
	    Integer ind = tableIdentifier.get(tableName);
	    if (ind == null) return false; // make sure the Metadata for table tableName exists before trying to clean it up
	    TableMetadata meta = Metadata[ind];
	    // copy of the TableMetadata which works for read only
	    TableMetadata old = meta.clone();
	    old.name += "CleanupTemp"; // rename the Table of TableMetadata
	    old.schema.setColumnTable(old.name); // rename the table which the columns are from
	    // link it in TableMetadata
	    Metadata[Metadata.length-1] = old;
	    tableIdentifier.put((tableName + "CleanupTemp").toLowerCase(), Metadata.length-1);
	    
	    // rename all files to work with the read only copy of TableMetadata
	    File temp = new File(rootDirectory + tableName + "CleanupTemp.txt");
	    new File(rootDirectory + tableName + ".txt").renameTo(temp);
	    LinkedList<File> textFileList = new LinkedList<File>();
        for (int i = 0; i <= meta.fileNr; i++) {
            File oldText = new File(rootDirectory + "texts/" + tableName + i + ".txt");
            File newText = new File(rootDirectory + "texts/" + tableName + "CleanupTemp" + i + ".txt");
            textFileList.add(newText);
            if (oldText.exists()) {
                oldText.renameTo(newText);
            }
        }
        
        // modify the original meta to be "empty"
        meta.fileNr = 0;
        meta.Page = 1;
        meta.TupleNr = 0;
        meta.textPosition = 0;
        
        // create the new data file in advance:
        ByteBuffer BOM = ByteBuffer.wrap( "\ufeff".getBytes(cs));
        FileChannel FileWriter = null;
        File newData = new File(rootDirectory + tableName + ".txt");
        try {
            newData.createNewFile();
            FileWriter = new RandomAccessFile(newData, "rw").getChannel();
            if (FileWriter.write(BOM)!=3) return false;
            FileWriter.close();
        } catch (IOException e) {
            System.out.println("TableMetadata Cleanup: Error creating the new table file or writing BOM.");
            return false;
        }
        
        /* read & write:
         * we write to the non existing files which will be recreated
         */
        Operator.AutoIncOn = false;
        new Insert(tableName).append(new Scan(tableName + "CleanupTemp"));
        Operator.AutoIncOn = true;
        
        // clean away the temporary (old things)
        Metadata[tableIdentifier.remove((tableName + "CleanupTemp").toLowerCase())] = null;
        if (!temp.delete()) {
            System.out.println("TableMetadata Cleanup: Error deleting the old db file of table " + tableName + ".");
            return false;
        }
        for (File f: textFileList)
            if (f.exists()) f.delete();
        
	    return true;
	}
	
	public static boolean cleanupAll() {
	    if (Metadata == null) return false;
	    boolean result = true;
	    for (int i = 0; i < Metadata.length-1; i++) { // don't attempt to cleanup the last spot in the Metadata array
	        if (!cleanupTable(Metadata[i].name)) result = false;
	    }
	    return result;
	}
	
	
	/**
	 * this is for testing purposes only
	 * it deletes all Metadata files if there are any
	 * @return true iff Metadata!=null in beforehand
	 */
	public static boolean resetMetadata() {
	    if (Metadata != null) {
	        Metadata = null;
	        return true;
	    }
	    else return false;
	}
}
