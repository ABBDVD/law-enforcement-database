package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import java.util.concurrent.ConcurrentLinkedQueue;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.TupleSchema;

/**
 * Base class of all operators. An operator processes one tuple at a time. It
 * allows an application to call moveNext() to move to the next tuple. After
 * moveNext() the application can retrieve the new tuple by a call to current().
 */
public abstract class Operator {

    private class threadLife implements Runnable {
        public void run() {
            Tuple t;
            while((t = computeNext()) != null)
                tuples.offer(t);
        }
    }
    
	/**
	 * The current tuple.
	 */
    protected TupleSchema schema;
    protected Operator op;
    protected Operator op2;
	protected Tuple current;
	private final Thread thread;
	private final ConcurrentLinkedQueue<Tuple> tuples;
	
	public Operator() {
	    this(null, null);
	}
	public Operator(Operator op) {
	    this(op, null);
	}
	public Operator(Operator op, Operator op2) {
        this.op = op;
        if (op == null)
            schema = null;
        else
            schema = op.getSchema();
	    this.op2 = op2;
	    current = null;
	    thread = new Thread(new threadLife());
	    tuples = new ConcurrentLinkedQueue<Tuple>();
	}
	
	protected final void start() {
	    if (op != null)
	        op.start();
	    if (op2 != null)
	        op2.start();
	    thread.start();
	}

	/**
	 * Moves forward to the next tuple. The next tuple can be retrieved by a
	 * call to current(). If there is no more tuple, this method returns false.
	 * @return true, if we advanced to next tuple
	 */
	public final boolean moveNext() {
	    if (thread.getState() == Thread.State.NEW)
	        start();
	    
	    // Wait for next element
	    while(thread.getState() != Thread.State.TERMINATED && tuples.isEmpty())
	        Thread.yield();
	    
	    if (!tuples.isEmpty()) {
	        current = tuples.poll();
	        return true;
	    }
	    
	    return false;
	}
	
	/**
	 * Return the next Tuple
	 * @return Next tuple or null if there are no more tuples
	 */
	protected abstract Tuple computeNext();

	/**
	 * @return the current tuple
	 */
	public final Tuple current() {
		return this.current;
	}
	
	public final TupleSchema getSchema() {
	    return schema;
	}
	
	public static char delimiter = ';';
	public static int pageSize = 8096;
	public static int textFileSize = 131072; // if a textFile is larger then textFileSize, the next insert will create a new textFile
	public static boolean AutoIncOn; // AutoIncOn = false is used for TableCleanup as we don't want new indices for that
	
}
