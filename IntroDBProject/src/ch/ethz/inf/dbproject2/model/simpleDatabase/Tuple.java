package ch.ethz.inf.dbproject2.model.simpleDatabase;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;

/**
 * A tuple in our database. A tuple consists of a schema (describing the names
 * of the columns) and values. A tuple is created and modified by operators.
 * 
 * You can use String to store the values. In case you need a specific type,
 * you can use the additional getType methods.
 */
public class Tuple {

	private final TupleSchema schema;
	private final String[] values;
	private int PageNumb;
	private int TupleNumb;
	
	/**Constructor with character array of all values
	 * character array will be split with the schema to a String array
	 * @param schema
	 * @param tupleLine (character array with all values)
	 * @param PageNumb
	 * @param TupleNumb
	 */
	public Tuple(final TupleSchema schema, final byte[] tupleLine, final int PageNumb, final int TupleNumb) {
		this(schema, splitTupleLine(tupleLine, schema), PageNumb, TupleNumb);
	}
	/**
	 * 
	 * @param schema
	 * @param values
	 * @param PageNumb
	 * @param TupleNumb
	 */
	public Tuple(final TupleSchema schema, final String[] values, final int PageNumb, final int TupleNumb) {
	        this.schema = schema;
	        this.values = values;
	        this.PageNumb = PageNumb;
	        this.TupleNumb = TupleNumb;
	    }

	public final TupleSchema getSchema() {
		return this.schema;
	}

	public final String get(final int index) {
        return this.values[index];
    }
	public final String get(final String column) {
        return get(schema.getIndex(column));
    }
	
	/**
	 * 
	 * @param index
	 * @return Trimmed string value if this column is not linked, the text which is linked in column index of the tuple otherwise
	 */
	public final String getText(final int index) {
	    if (schema.getFormat(index) != 5) return values[index] == null ? null : values[index].trim();
	    String tableName = schema.getTableName(index);
	    String link = this.values[index];
	    return getText(link, tableName);
	}
	@SuppressWarnings("resource")
    public static String getText(String link, String tableName) {
	    int fileNr = Integer.parseInt(link.substring(0, 11).trim());
        int position = Integer.parseInt(link.substring(11, 22).trim());
        int length = Integer.parseInt(link.substring(22).trim());
        if (length == 0) return "";
        try {
            FileChannel reader = new RandomAccessFile(TableMetadata.rootDirectory + "texts/" + tableName + fileNr + ".txt", "r").getChannel();
            reader.position(position);
            ByteBuffer buf = ByteBuffer.allocate(length);
            int check = reader.read(buf);
            if (length != check && -1 != check) System.out.println("getText: Length of read bytes != length of text");
            buf.flip();
            byte[] bytes = buf.array();
            String decoded;
            if (bytes[0] == 0) decoded = null; // set the string null if the first char is null
            else decoded = new String(bytes, cs);
            reader.close();
            return decoded;
        } catch (FileNotFoundException e) {
            System.out.println("getText: File " + TableMetadata.rootDirectory + "texts/" + tableName + fileNr + ".txt" + " not found.");
        } catch (IOException e) {
            System.out.println("Skip/Read of the getText lookup failed for fileNr " + fileNr + " of table " + tableName + " failed.");
        }
        return null;
	}
	public final String getText(final String column) {
	    return getText(schema.getIndex(column));
	}
	
	public final int getInt(final int index) {
	    if (this.values[index] == null)
	        return 0;
		return Integer.parseInt(this.values[index].trim());
	}
    public final int getInt(final String column) {
        return getInt(schema.getIndex(column));
    }
	
    public final boolean getBoolean(final int index) {
        return "1".equals(this.values[index].trim())? true: false;
    }
    public final boolean getBoolean(final String column) {
        return getBoolean(schema.getIndex(column));
    }
    
    public Date getTimestamp(final int index) { // TODO change to java.util.date format?
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
        try {
            if (values[index] == null)
                return null;
            return format.parse(values[index]);
        } catch (ParseException e) {
            System.out.println("Tuple.java: Parsing failed!");
        }
        return null;
    }
    public Date getTimestamp(final String column) {
        return getTimestamp(schema.getIndex(column));
    }
	
	public final int getTupleNumb() {
	    return this.TupleNumb;
	}
	
	public final int getPageNumb() {
	    return this.PageNumb;
	}
	
	public final String[] getValues() {
	    return values;
	}

	private static final Charset cs = Charset.forName("UTF-8");
	
	private final static String[] splitTupleLine(byte[] tupleLine, TupleSchema schema) {
	    String [] values = new String[schema.getColumns().length];
	    for(int i = 0; i < schema.getColumns().length; i++) {
	        if (tupleLine[schema.getOffset(i)] == 0) values[i] = null; // check if this whole field is null (indicate by the first character being null
	        else values[i] = new String(Arrays.copyOfRange(tupleLine, schema.getOffset(i), schema.getOffset(i)+schema.getByteSize(i)), cs);
	    }
	    return values;
	}
	
	/**
	 * Note: this converts to tuple to a string with the preset delimiter Operator.delimiter
	 */
	public final String toString() {
		final StringBuilder buf = new StringBuilder();
		for (int i = 0; i < values.length; i++) {
			buf.append(values[i]);
			if (i < values.length - 1) {
				buf.append(Operator.delimiter);
			}
		}
		return buf.toString();
	}
	
	/**
	 * converts to tuple to a string without adding a delimiter!
	 * @return
	 */
	public final String toStringPure() {
    	final StringBuilder buf = new StringBuilder();
        for (int i = 0; i < values.length; i++)
            buf.append(values[i]);
        return buf.toString();
    }
	
	public void setLinkerValue(int index, int fileNr, int position, int length) {
	    values[index] = getLinkerValueString(fileNr, position, length);
	}
	
	public static String getLinkerValueString(int fileNr, int position, int length) {
        StringBuilder sb = new StringBuilder(33);
        sb.append(String.format("%1$-" + 11 + "s", fileNr));
        sb.append(String.format("%1$-" + 11 + "s", position));
        sb.append(String.format("%1$-" + 11 + "s", length));
        return sb.toString();
	}
	
	private static byte padding = " ".getBytes()[0]; // = 32
	
	/**
	 * fills all ByteBuffers with the padded values (except linked values, which are ignored; their ByteBuffer remains allocated but empty)
	 * @param schema
	 * @param values
	 * @param buff (all not linked ByteBuffers must be allocated
	 * @return
	 */
	public static int adjustLength(TupleSchema schema, String[] values, ByteBuffer[] buff) {
	    for (int i = 0; i < values.length; i++) {
	        if (values[i]==null) values[i] = Character.toString('\u0000'); // overwrite null values by a null character
	        // get value as byte[]
	        if (schema.getColumn(i).getFormat() != 5) {
	            int length = schema.getColumn(i).getByteSize();
    	        byte[] bytes = values[i].getBytes(cs);
    	        if (bytes.length > length) return -1;
    	        buff[i] = ByteBuffer.allocate(length);
    	        buff[i].put(bytes); // write the value
    	        // get padding as byte[]
    	        bytes = new byte[length-bytes.length];
                Arrays.fill(bytes, padding);
    	        buff[i].put(bytes);
    	        buff[i].flip(); // ready for writing
	        }
	    }
	    return 0;
	}
	
	public long getPosition() {
	    if (getSchema().isMultiSource()) return -1;
	    else return (getPageNumb()-1)*Operator.pageSize + getTupleNumb()*getSchema().getLength()+3; // +3 for the BOM byte
	}

	public boolean equiv(Tuple other) {
	    if (values.length == other.getValues().length) {
	        for (int i = 0; i < values.length; i++) { // handle that a value can be null
	            // if one value is null but the other isn't or both values don't equal the same
	            if (values[i] != null || other.getValues()[i] != null)
	                if ((values[i] == null ^ other.getValues()[i] == null) || !values[i].equals(other.getValues()[i]))
	                    return false;
	        }
	        return true;
	    }
	    return false;
	}
	
}
