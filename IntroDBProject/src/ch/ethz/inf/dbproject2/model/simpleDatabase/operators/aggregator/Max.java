package ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator;

import java.util.ArrayList;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Column;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;

public class Max extends Aggregator {

    private final String column;
    private final int columnIndex;
    
    public Max(Operator op, String column) {
        super(op);
        this.column = column;
        columnIndex = schema.getIndex(column);
    }
    
    @Override protected String aggregate(ArrayList<Tuple> list) {
        if (list != null && list.size() > 0) {
            int value = list.get(0).getInt(columnIndex);
            for (Tuple t: list)
                if (value < t.getInt(columnIndex))
                    value = t.getInt(columnIndex);
            
            return Integer.toString(value);
        }
        else
            return null;
    }

    @Override protected Column aggColumn() {
        return new Column("Max", (short)2, null, (short)-1, (short)0);
    }
    
    @Override public Max clone(Operator op) {
        return new Max(op, column);
    }

}
