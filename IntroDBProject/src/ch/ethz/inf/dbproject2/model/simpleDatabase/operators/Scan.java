package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Arrays;

import ch.ethz.inf.dbproject2.model.simpleDatabase.TableMetadata;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.TupleSchema;


/**
 * The scan operator reads tuples from a file. The lines in the file contain the
 * values of a tuple. The line a comma separated.
 */
public class Scan extends Operator {

    private final FileChannel reader;
    
    private static int testFileCounter;
    /**
     * Contructs a new scan operator.
     * @param table, name of the table to read data from
     */
    public Scan(final String table) {
        // create schema
        TableMetadata meta = TableMetadata.getMetadata(table);
        this.schema = meta.schema;
        // read from file
        FileChannel reader = null;
        try {
            reader = new RandomAccessFile(TableMetadata.rootDirectory + table + ".txt", "r").getChannel();
        } catch (FileNotFoundException e) {
            System.out.println("File not found! " + TableMetadata.rootDirectory + table + ".txt");
        }
        this.reader = reader;
        try {
            this.reader.position(3);
        } catch (IOException e) {
            // skip the UTF-8 BOF characters
            System.out.println("Reader error in scan. No BOF characters could be skipped.");
        }
        TuplePerPage = (int) Math.floor((pageSize-1)/(schema.getLength())); // we add one byte at the end of the whole page
        this.tuple = -1;
        this.page = 0;
        status = true;
        lastRead = pageSize;
        pageBuffer = ByteBuffer.allocate(pageSize);
        lastPageTupleNr = meta.getTupleNr();
        MetaPageNr = meta.getPage();
    }

    /**
     * Constructs a new scan operator (mainly for testing purposes).
     * @param reader reader to read lines from
     * @param columns column names
     */
    public Scan(final String s, final String[] columnNames) {
        FileChannel reader = null;
        RandomAccessFile testRaf = null;
        try {
            // create temp file
            File fold = new File(TableMetadata.rootDirectory + "test/");
            fold.mkdir();
            File temp = new File(TableMetadata.rootDirectory + "test/" + testFileCounter + ".txt");
            temp.createNewFile();
            
            // write the string s to it
            byte[] bytes = s.getBytes("UTF-8");
            testRaf = new RandomAccessFile(temp, "rw");
            reader = testRaf.getChannel();
            ByteBuffer tempBuf = ByteBuffer.allocate(bytes.length);
            tempBuf.put(bytes);
            tempBuf.flip();
            reader.write(tempBuf);
            reader.close();
            testRaf = new RandomAccessFile(temp, "r");
            reader = testRaf.getChannel();
        } catch (IOException e) {
            System.out.println("Scan: Problem creating the temporary file.");
        }
        testFileCounter++;
        
        this.reader = reader;
        
        this.schema = new TupleSchema(columnNames);
        TuplePerPage = (int) Math.floor((pageSize-1)/(schema.getLength()));
        this.tuple = -1;
        this.page = 0;
        status = true;
        lastRead = pageSize;
        pageBuffer = ByteBuffer.allocate(pageSize);
        lastPageTupleNr = 10000; // some large number
        MetaPageNr = 100;
    }
    
    // saving the previous state of moveNext and some constants
    private int page; // which page are we reading
    private int tuple; // which tuple are we reading
    private final int TuplePerPage; // how many tuples are there per page
    private ByteBuffer pageBuffer;
    private int lastRead;
    private boolean status; // status = false -> we reached the end of the data
    private static final Charset cs = Charset.forName("UTF-8");
    private final int lastPageTupleNr; // how many tuples are on the last page?
    private final int MetaPageNr; // how many pages are there total of this table?
    
    @Override
    protected Tuple computeNext() {
        try {
            while (status) {
                while (status && tuple>= 0 && tuple<TuplePerPage) {
                    // check if we have reached the end of the file (exactly) after this last read
                    if (page >= MetaPageNr && tuple >= lastPageTupleNr)
                        status = false;
                    if (tuple==TuplePerPage-1) {
                        if (!new String(Arrays.copyOfRange(pageBuffer.array(), pageSize-1, pageSize), cs).equals("|")) {
                            status = false;
                        }
                    }
                    String validByte = new String(Arrays.copyOfRange(pageBuffer.array(), tuple*schema.getLength(), tuple*schema.getLength()+1), cs); 
                    if (validByte.equals("1")) {// valid?
                        final Tuple t = new Tuple(schema, Arrays.copyOfRange(pageBuffer.array(), tuple*schema.getLength()+1, (tuple+1)*schema.getLength()+1), page, tuple); // skip the valid character and the following delimiter
                        tuple++;
                        return t;
                    }
                    else if (!validByte.equals("0"))
                        status = false;
                     // increase tuple
                    tuple++;
                }
                if (status && lastRead == pageSize) {
                    tuple=0; // reset tuple counter
                    pageBuffer.clear(); // prepare Buffer for reading
                    lastRead = reader.read(pageBuffer); // read to buffer
                    // lastRead = reader.read(pageBuffer, 0, pageSize);
                    page++;
                }// increase page counter
                else {
                    reader.close();
                    return null;
                }
            }
            reader.close();
            return null;
        } catch (IOException e) {
            System.out.println("Read error:\n" + e.getMessage());
        }
        return null;
        
    }

}
