package ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator;

import java.util.ArrayList;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Column;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.TupleSchema;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;

public abstract class Aggregator extends Operator {
    
    protected boolean done;
    
    public Aggregator(Operator op) {
        super(op);
        done = false;
        
        if (op != null) {
            Column[] oldColumns = schema.getColumns();
            Column[] newColumns = new Column[oldColumns.length + 1];
            for (int i = 0; i < oldColumns.length; i++)
                newColumns[i] = oldColumns[i];
            newColumns[oldColumns.length] = aggColumn();
            schema = new TupleSchema(newColumns);
        }
        else {
            schema = new TupleSchema(new Column[] { aggColumn() });
        }
    }
    
    @Override
    protected Tuple computeNext() {
        if (!done) {
            final String[] values = new String[schema.getColumns().length];
            ArrayList<Tuple> list = new ArrayList<Tuple>();
            int pageNumb = 0;
            int tupleNumb = 0;
            done = true;
            if (op.moveNext()) {
                // Get old values
                Tuple t = op.current();
                
                // Copy old values
                for (int i = 0; i < values.length - 1; i++) {
                    values[i] = t.get(i);
                }
                
                list.add(t);
                while (op.moveNext())
                    list.add(op.current());
                
                pageNumb = t.getPageNumb();
                tupleNumb = t.getTupleNumb();
            }
            else {
                // Fill tuple with null values
                for (int i = 0; i < values.length - 1; i++) {
                    values[i] = null;
                }
            }
            
            // Aggregate
            values[values.length - 1] = aggregate(list);
            
            // Return Tuple
            return new Tuple(schema, values, pageNumb, tupleNumb);
        }
        return null;
    }

    protected abstract String aggregate(ArrayList<Tuple> list);
    protected abstract Column aggColumn();
    
    public abstract Aggregator clone(Operator op);
};
