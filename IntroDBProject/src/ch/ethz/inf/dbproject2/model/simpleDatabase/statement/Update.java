package ch.ethz.inf.dbproject2.model.simpleDatabase.statement;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;


/**
 * The scan operator reads tuples from a file. The lines in the file contain the
 * values of a tuple. The line a comma separated.
 */
public class Update extends Statement {
    
	

	/**
	 * Insert values (append to the end of the file)
	 * @param tableName
	 */

    
    
    public Update(String tableName) {
        super(tableName);
    }
    
    /**
     * 
     * @param op says which tuples have to be updated
     * @param ColVal contains Entries <Column name, Value>
     * @return
     */
    public int update(Operator op, HashMap<String, String> ColVal) {
        int entriesWritten = 0;
        if (writer != null && writer.isOpen()) { // check that writer is not closed
            while(op.moveNext()) {
                if (op.current().getSchema() != meta.schema) return -1;
                
                String [] values = op.current().getValues();
                
                // prefetch text values
                for (int i: meta.schema.getLinkedIndices()) {
                    values[i] = Tuple.getText(values[i], tableName);
                }
                // replace all "updated" values
                for (Entry<String, String> e: ColVal.entrySet()) {
                    int columnIndex = meta.schema.getIndex(e.getKey());
                    
                    // make sure we don't overwrite a auto increment column!
                    if (meta.getAutoIncIndex() != columnIndex)
                        values[meta.schema.getIndex(e.getKey())] = e.getValue();
                }
                
                // move to right position
                try {
                    writer.position(op.current().getPosition());
                } catch (IOException e) {
                    System.out.println("Setting writer position for Insert failed.");
                }
                write(values);
                entriesWritten++;
            }
            close();
        }
        else entriesWritten = -1;
        return entriesWritten;
        
    }

}
