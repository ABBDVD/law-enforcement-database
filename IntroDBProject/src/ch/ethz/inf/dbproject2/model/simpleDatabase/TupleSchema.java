package ch.ethz.inf.dbproject2.model.simpleDatabase;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

/**
 * The schema contains meta data about a tuple. So far we only store the name of
 * each column. Other meta data, such cardinalities, indexes, etc. could be
 * specified here.
 */
public class TupleSchema {

	
	private final HashMap<String, Integer> columnNamesMap;
	private final Column[] columns;
	private final short length; // length of the tuple in characters
	private final LinkedList<Integer> linkedIndices;
	
	/**
	 * Constructs a new tuple schema.
	 * @param columnNames column names
	 */
	public TupleSchema(
		final String[] columnNames,
		final short[] format,
		final String tableName,
		final HashMap<Integer, Short> varcharSizeMap
	) {
	    short length = 0;
	    this.columns = new Column[columnNames.length];
	    linkedIndices = new LinkedList<Integer>();
	    for (int i = 0; i < columnNames.length; i++) {
	        if (format[i]==5) linkedIndices.add(i);
	        short varcharSize = -1;
	        Short o = varcharSizeMap.get(i);
	        if ( o!= null) varcharSize = o.shortValue();
	        this.columns[i] = new Column(columnNames[i], format[i], tableName, varcharSize, length);
	        length = (short) (this.columns[i].getOffset() + this.columns[i].getByteSize());
	    }
	    this.length = ++length; // add 1 for the valid character
		this.columnNamesMap = new HashMap<String, Integer>();
		for (int i = 0; i < columnNames.length; ++i) {
			this.columnNamesMap.put(this.columns[i].getColumnName().toUpperCase(), i);
		}
		
	}
	
	/**
	 * for Schema of String input, simplified.
	 * Preset column size of 50 chars, no fileSource, 
	 * @param columnNames
	 */
	public TupleSchema(
	        final String[] columnNames
	    ) {
	        short length = 0;
	        this.columns = new Column[columnNames.length];
	        linkedIndices = new LinkedList<Integer>();
	        for (int i = 0; i < columnNames.length; i++) {
	            short varcharSize = 10;
	            this.columns[i] = new Column(columnNames[i], (short)1, null, varcharSize, length);
	            length += this.columns[i].getByteSize(); // calculate the offset for the next column (respectively the complete length if this is the last iteration
	        }
	        this.length = ++length; // add 1 for the valid character
	        this.columnNamesMap = new HashMap<String, Integer>();
	        for (int i = 0; i < columnNames.length; ++i) {
	            this.columnNamesMap.put(this.columns[i].getColumnName().toUpperCase(), i);
	        }
	    }
	
	public TupleSchema(final Column[] column) {
	    this.columns = column;
	    short length = 0;
	    this.columnNamesMap = new HashMap<String, Integer>();
	    linkedIndices = new LinkedList<Integer>();
	    for (int i = 0; i < column.length; i++) {
	        if (column[i].getFormat()==5) linkedIndices.add(i);
	        Column c = column[i];
	        length+=c.getByteSize();
	        this.columnNamesMap.put(this.columns[i].getColumnName().toUpperCase(), i);
	    }
	    this.length = ++length; // add 1 for the valid character
	}
	
	/**
	 * 
	 * @return length of the complete tuple
	 */
	public final int getLength() {
	    return (int)length;
	}
	
	// column information
	/**
	 * Given the name of a column, returns the index in the respective tuple.
	 * 
	 * @param column column name
	 * @return index of column in tuple
	 */
	public int getIndex(final String columnName) {

        final Integer index = this.columnNamesMap.get(columnName.toUpperCase());
        if (index == null) {
            return -1; // error
        } else {
            return index;
        }
    }
	
	// get column information
	public Column getColumn(final String columnName) {
	    return columns[getIndex(columnName)];
	}
	public Column getColumn(final int index) {
	    return columns[index];
	}
	
	public Column[] getColumns() {
	    return columns;
	}
	
	public String getTableName(final int i) {
	    return columns[i].getTableName();
	}
	public String getTableName(final String columnName){
	    return columns[getIndex(columnName)].getTableName();
	}
	/**
	 * 
	 * @param columnName
	 * @return ByteSize of this column
	 */
	public short getByteSize(final int index) {
	    return columns[index].getByteSize();
	}
	/**
	 * 
	 * @param index
	 * @return Size of this column
	 */
	public short getSize(final int index) {
	    return columns[index].getSize();
	}
	public short getFormat(final String columnName) {
	    return columns[getIndex(columnName)].getFormat();
	}
	public short getFormat(final int index) {
	    return columns[index].getFormat();
	}
	
	public int getOffset(final int index) {
	    return columns[index].getOffset();
	}
	
	public LinkedList<Integer> getLinkedIndices() {
	    return linkedIndices;
	}
	
	public boolean isMultiSource() {
	    if (columns.length  < 2) return false;
	    String last = columns[0].getTableName();
	    for (int i = 1; i < columns.length; i++)
	        if (columns[i].getTableName()!=last) return true;
	    return false;
	}
	
	public void setColumnName(String oldName, String newName) {
	    columnNamesMap.put(newName.toUpperCase(), columnNamesMap.remove(oldName.toUpperCase()));
	}
	
	/****
     *  cleanup of Tables
     * 
     * 
     ****/
    
    /**
     * constructor for making exact copy of an existing Schema except changed name
     * the following are only "linked" but not actually cloned (as they are 'read only' anyway and stay the same):
     *  - linkedIndices
     *  - columnNamesMap
     * @param old
     */
    private TupleSchema(TupleSchema old) {
        // create the hashmap newly
        columnNamesMap = new HashMap<String, Integer>();
        for (Entry<String, Integer> e: old.columnNamesMap.entrySet()) {
            columnNamesMap.put(e.getKey(), new Integer(e.getValue()));
        }
        columns = new Column[old.columns.length];
        for (int i = 0; i < columns.length; i++) {
            columns[i] = old.columns[i].clone();
        }
        length = old.length;
        linkedIndices = old.linkedIndices;
    }
    @Override
    public TupleSchema clone() {
        return new TupleSchema(this);
    }
    
    public TupleSchema setColumnTable(String TableName) {
        for (Column c: columns) c.setTableName(TableName);
        return this;
    }
}
