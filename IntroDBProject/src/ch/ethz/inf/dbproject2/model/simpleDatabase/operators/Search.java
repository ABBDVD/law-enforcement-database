package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

public class Search<T> extends Select<T>
{

    public Search(final Operator op, final String column, final T compareValue) {
        this(op, column, compareValue, false);
    }
    public Search(final Operator op, final String column, final T compareValue, final boolean not) {
        super(op, column, compareValue, not);
    }
    
    protected boolean accept(final String value) {
        return value.toUpperCase().contains(compareValue.toString().toUpperCase().trim());
    }
    
}
