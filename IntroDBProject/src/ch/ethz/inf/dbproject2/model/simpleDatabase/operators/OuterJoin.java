package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Column;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.TupleSchema;
import ch.ethz.inf.dbproject2.model.simpleDatabase.util.Pair;

public class OuterJoin extends Operator {
    
    private final int indexLeft;
    private final int indexRight;
    private final HashMap<Integer, ArrayList<Pair<Tuple, Boolean>>> hashBuffer;
    private final LinkedList<Pair<Tuple, Boolean>> queue;
    private Iterator<Pair<Tuple, Boolean>> it;
    private boolean hashed;
    private boolean queued;
    
    public OuterJoin(Operator op, Operator op2, String columnLeft, String columnRight) {
        super(op, op2);
        indexLeft = op.getSchema().getIndex(columnLeft);
        indexRight = op2.getSchema().getIndex(columnRight);
        hashBuffer = new HashMap<Integer, ArrayList<Pair<Tuple, Boolean>>>();
        queue = new LinkedList<Pair<Tuple, Boolean>>();
        it = null;
        hashed = false;
        queued = false;
        
        final Column[] columnsLeft = op.getSchema().getColumns();
        final Column[] columnsRight = op2.getSchema().getColumns();
        final Column[] newColumns = new Column[columnsLeft.length + columnsRight.length];
        for (int i = 0; i < columnsLeft.length; i++)
            newColumns[i] = columnsLeft[i];
        for (int i = 0; i < columnsRight.length; i++)
            newColumns[columnsLeft.length + i] = columnsRight[i];
        schema = new TupleSchema(newColumns);
    }

    @Override
    protected Tuple computeNext() {
        // Get all elements of left operand and sort them by hash
        if (!hashed) {
            hashed = true;
            while (op.moveNext()) {
                Tuple t = op.current();
                int key = t.get(indexLeft) == null? 0: t.get(indexLeft).hashCode();
                ArrayList<Pair<Tuple, Boolean>> table = hashBuffer.get(key);
                if (table == null) {
                    table = new ArrayList<Pair<Tuple, Boolean>>();
                    table.add(new Pair<Tuple, Boolean>(t, false));
                    hashBuffer.put(key, table);
                }
                else
                    table.add(new Pair<Tuple, Boolean>(t, false));
            }
        }
        
        // Check if left operand had any elements and right operand has further elements
        if (!hashBuffer.isEmpty()) {
            do {
                while (it != null && it.hasNext()) {
                    final Pair<Tuple, Boolean> pair = it.next();
                    final Tuple s = pair.a;
                    final Tuple t = op2.current();

                    if (s.get(indexLeft) == null? t.get(indexRight) == null: s.get(indexLeft).equals(t.get(indexRight))) {
                        pair.b = true;
                        final int lengthLeft = s.getSchema().getColumns().length;
                        final int lengthRight = t.getSchema().getColumns().length;
                        
                        // Create merged tuple
                        final String[] values = new String[lengthLeft + lengthRight];
                        for (int i = 0; i < lengthLeft; i++)
                            values[i] = s.get(i);
                        for (int i = 0; i < lengthRight; i++)
                            values[lengthLeft + i] = t.get(i);
                        return new Tuple(schema, values, s.getPageNumb(), s.getTupleNumb());
                    }
                }

                // List through -> Check next element
                if (it == null || !it.hasNext()) {
                    while (op2.moveNext()) {
                        String val = op2.current().get(indexRight);
                        if (val != null) {
                            final ArrayList<Pair<Tuple, Boolean>> table = hashBuffer.get(val.hashCode());
                            if (table != null) {
                                it = table.iterator();
                                break;
                            }
                        }
                    }
                }
            } while (it != null && it.hasNext());
        }
        
        // Merge remaining elements
        if (!queued) {
            queued = true;
            for (Map.Entry<Integer, ArrayList<Pair<Tuple, Boolean>>> e: hashBuffer.entrySet())
                for (Pair<Tuple, Boolean> p: e.getValue())
                    queue.add(p);
        }
        
        // Go through the remaining elements
        if (!queue.isEmpty()) {
            Pair<Tuple, Boolean> p;
            do {
                p = queue.poll();
            } while (p != null && p.b);
            
            // Build tuple
            if (p != null) {
                final Tuple t = p.a;
                final int lengthLeft = t.getSchema().getColumns().length;
                final int lengthTotal = schema.getColumns().length;
                final String[] values = new String[lengthTotal];
                for (int i = 0; i < lengthLeft; i++)
                    values[i] = t.get(i);
                for (int i = lengthLeft; i < lengthTotal; i++)
                    values[i] = null;
                return new Tuple(schema, values, t.getPageNumb(), t.getTupleNumb());
            }
        }

        return null;
    }

}
