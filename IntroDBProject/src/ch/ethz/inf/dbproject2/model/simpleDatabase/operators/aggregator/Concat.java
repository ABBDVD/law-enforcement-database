package ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator;

import java.util.ArrayList;
import java.util.LinkedList;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Column;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;

public class Concat extends Aggregator {

    private final String column;
    private final int columnIndex;
    private final String separator;
    private final boolean distinct;
    
    public Concat(Operator op, String column, String separator) {
        this(op, column, separator, false);
    }
    public Concat(Operator op, String column, String separator, boolean distinct) {
        super(op);
        this.column = column;
        columnIndex = schema.getIndex(column);
        this.separator = separator;
        this.distinct = distinct;
    }
    
    @Override protected String aggregate(ArrayList<Tuple> list) {
        String value = null;
        if (list != null && list.size() > 0) {
            value = "";
            LinkedList<String> valueList = new LinkedList<String>();
            for (Tuple t: list)
                valueList.add(t.getText(columnIndex));
            //TODO handle the case that one value is null ( we can't concatenate null to an existing string and do comparisons as usual
            boolean first = true;
            while (!valueList.isEmpty()) {
                String val = valueList.pollFirst();
                if (val != null) { // skip this tuple if the value is null
                    if (!distinct || !valueList.contains(val)) {
                        if (first)
                            first = false;
                        else
                            value += separator;
                        value += val;
                    }
                }
            }
        }
        
        return value;
    }

    @Override protected Column aggColumn() {
        return new Column("Concat", (short)1, null, (short)20, (short)0);
    }
    
    @Override public Concat clone(Operator op) {
        return new Concat(op, column, separator, distinct);
    }

}
