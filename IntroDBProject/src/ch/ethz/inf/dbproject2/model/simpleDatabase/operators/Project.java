package ch.ethz.inf.dbproject2.model.simpleDatabase.operators;

import ch.ethz.inf.dbproject2.model.simpleDatabase.Column;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.TupleSchema;

/**
 * Projection in relational algebra. Returns tuples that contain on projected
 * columns. Therefore the new tuples conform to a new schema.
 */
public final class Project extends Operator {

	private final String[] columns;

	/**
	 * Constructs a new projection operator.
	 * @param op operator to pull from
	 * @param column single column name that will be projected
	 */
	public Project(final Operator op, final String column) {
		this(op, new String[] { column });
	}

	/**
	 * Constructs a new projection operator.
	 * @param op operator to pull from
	 * @param columns column names that will be projected
	 */
	public Project(final Operator op, final String[] columns) {
		super(op);
		this.columns = columns;
		
		Column[] newCols = new Column[columns.length];
        for(int i = 0; i < columns.length; i++)
            newCols[i] = schema.getColumn(columns[i]);
        schema = new TupleSchema(newCols);
	}

	@Override
    protected Tuple computeNext() {
		// get next tuple from child operator
		// create new tuple by copying the appropriate columns
		// return if we were able to advance to the next tuple
	    if (op.moveNext()) {
	        Tuple t = op.current();
	        
	        // get values for the new Tuple
	        String [] values = new String[columns.length];
	        for (int i = 0; i < columns.length; i++)
	            values[i] = t.get(columns[i]);
	        
	        return new Tuple(schema, values, t.getPageNumb(), t.getTupleNumb());
	    }
	    
		return null;
	}
}
