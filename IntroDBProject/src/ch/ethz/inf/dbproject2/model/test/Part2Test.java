package ch.ethz.inf.dbproject2.model.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import junitx.framework.FileAssert;
import ch.ethz.inf.dbproject2.model.simpleDatabase.TableMetadata;
import ch.ethz.inf.dbproject2.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Group;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Join;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Limit;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Operator;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.OuterJoin;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Project;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Rename;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Scan;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Search;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Select;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Sort;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.Union;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator.Aggregator;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator.Concat;
import ch.ethz.inf.dbproject2.model.simpleDatabase.operators.aggregator.Count;
import ch.ethz.inf.dbproject2.model.simpleDatabase.statement.Delete;
import ch.ethz.inf.dbproject2.model.simpleDatabase.statement.Insert;
import ch.ethz.inf.dbproject2.model.simpleDatabase.statement.Update;

import org.apache.commons.io.FileUtils;

/**
 * Unit tests for Part 2.
 */
public class Part2Test {
    
	private String[] schema = new String[] { "id", "name", "status" };
	private String relation = "11                   Stolen car          open                12                   Fiscal fraud        closed              13                   High speed          open                )";
	
	private String[] schema2 = new String[] { "name", "case" };
	private String relation2 = "1Johnny Thunder      1                   1Johnny Thunder      2                   1Sam Sinister        2                   ";
	
	@BeforeClass
    public static void setUp() {
        // allocate working Dir the first time
        try {
            System.out.println("****\nNote that there's a Thread.sleep executed before each test as for example Windows will\nhave trouble with the fast allocation and deletion of directories repeatedly.\n****");
            FileUtils.copyDirectoryToDirectory(new File("test_db/"), new File("workingDirectory_tests/"));
            Thread.sleep(100);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        TableMetadata.rootDirectory = "workingDirectory_tests/test_db/";
    }
	
	@AfterClass
    public static void tearDown() {
        try {
            FileUtils.deleteDirectory(new File("workingDirectory_tests/"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	
	
	/* Unused atm
	@Before
	public void setWorkDir() {
	    // do something
	    
	}
	*/
	
	@After
	public void prepareWorkingDir() {
	    try {
	        // delete the working directory (recursively)
	        FileUtils.deleteDirectory(new File("workingDirectory_tests/"));
	        Thread.sleep(100);
	        // copy over the working directory
            FileUtils.copyDirectoryToDirectory(new File("test_db/"), new File("workingDirectory_tests/"));
            Thread.sleep(100);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } 
	    // for testing purpose required, so we don't use wrong meta data from the previous run with the "untouched" meta file together
	    TableMetadata.resetMetadata();
	}
	
	@Test
	public void testScan() {
		Operator op = new Scan(relation, schema);
		String expected = "1                   ;Stolen car          ;open                |2                   ;Fiscal fraud        ;closed              |3                   ;High speed          ;open                ";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testScanText() {
	    Operator op = new Scan("NameText");
        op.moveNext();
        String firstText = op.current().getText(1);
        op.moveNext();
        assertEquals("1234567890abcdefghij���?890\r\n56  90XX" + "This Text has 33 characters.\r\n1XX", firstText + op.current().getText(1));
	}
	
	@Test
	public void testScan3() {
        Operator op = new Scan("case");
        String expected = "1          ;20140414194700;Red Keep                                                                                  |2          ;19991212001200;King's Landing                                                                            |3          ;19991212001200;Clegane's Keep                                                                            |4          ;19880222001200;Craster's Keep, Beyond the Wall                                                           |5          ;19771212001200;King's Landing                                                                            |6          ;19880212001200;                                                                                          |8          ;19960606180600;The Twins, the Riverlands                                                                 |9          ;19881212001200;Winterfell                                                                                |10         ;19881212001200;King's Landing                                                                            |11         ;19881212001200;The Wall                                                                                  |12         ;19980215142200;City of Qarth                                                                             ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testSelect() {
        Operator op = new Select<String>(new Scan(relation, schema), "status", "closed");
        String expected = "2                   ;Fiscal fraud        ;closed              ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testSelectNot() {
        Operator op = new Select<String>(new Scan(relation, schema), "name", "High speed", true);
        String expected = "1                   ;Stolen car          ;open                |2                   ;Fiscal fraud        ;closed              ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testSearch() {
        Operator op = new Search<String>(new Scan(relation, schema), "name", "speed");
        String expected = "3                   ;High speed          ;open                ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testUpdate() {
        Operator op = new Select<String>(new Scan("case"), "CID", "6");
        HashMap<String, String> ColVal = new HashMap<String, String>();
        ColVal.put("Location", "Test Village\n ���**\"\" Wuh. The END");
        int num = new Update("case").update(op, ColVal);
        assertEquals(num, 1);
        FileAssert.assertEquals(new File(TableMetadata.rootDirectory + "solutions/testUpdate.txt"), new File(TableMetadata.rootDirectory + "case.txt"));
        
        // update with Text fields
        op = new Select<String>(new Scan("NameText"), "Name", "2ndEntry? ");
        ColVal = new HashMap<String, String>();
        ColVal.put("Name", "new2nd!");
        String expected = "This is the new text for the 2nd entry of Table NameText. Well that worked well apparently :D";
        ColVal.put("TextField", expected);
        num = new Update("NameText").update(op, ColVal);
        assertEquals(1, num);
        
        op = new Select<String>(new Scan("NameText"), "Name", "new2nd!");
        String actual = null;
        if (op.moveNext()) actual = op.current().getText(1);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testDelete() {
        System.out.println("TestDelete");
        int actual = new Delete(new Select<String>(new Scan("case"), "Location", "King's Landing")).deleteAll();
        int expected = 3;
        
        System.out.println("Delete done.");
        assertEquals(expected, actual);
    }
    
    
    @Test
    public void testInsert() {
        System.out.println("testInsert");
        int result = new Insert("case").append(new Select<String>(new Scan("case"), "CID", "3"));
        System.out.println("insert result: " + result);
        File testInsertActual = new File(TableMetadata.rootDirectory + "case.txt");
        assertEquals(result, 1);
        FileAssert.assertEquals(new File(TableMetadata.rootDirectory + "solutions/testInsert.txt"), testInsertActual);
        System.out.println("normal Insert done");
        
        // Insert of text:
        String expected = "Some loooooong ass text. &@asd�flkj    ----- Longer      ----- Longer ------- LOoOOOOOOOOOOOOONGER!";
        result = new Insert("NameText").append(new String[][]{{"T_ins", expected}});
        Operator op = new Select<String>(new Scan("NameText"), "Name", "T_ins");
        String actual = null;
        if (op.moveNext()) actual = op.current().getText(1);
        assertEquals(expected, actual);
        actual = Tuple.getText(op.current().get(1), "NameText");
        assertEquals(expected, actual);
    }
    
    
    
    @Test
    public void testProjectByName() {
        Operator op = new Project(new Scan(relation, schema), "name");
        String expected = "Stolen car          |Fiscal fraud        |High speed          ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }

    @Test
    public void testProjectByIdStatus() {
        String[] columns = new String[] { "status", "id" };
        Operator op = new Project(new Scan(relation, schema), columns);
        String expected = "open                ;1                   |closed              ;2                   |open                ;3                   ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }

    @Test
    public void testSelectProject() {
        Operator op = new Project(new Select<String>(new Scan(relation, schema), "status", "closed"), "name");
        String expected = "Fiscal fraud        ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testSortStatus() {
        Operator op = new Sort(new Scan(relation, schema),"Status", true);
        String expected = "2                   ;Fiscal fraud        ;closed              |1                   ;Stolen car          ;open                |3                   ;High speed          ;open                ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testJoin() {       
        Operator op = new Project(new Join(new Scan(relation, schema), new Rename(new Scan(relation2, schema2), "name", "person"), "id", "case"), new String[] { "name", "person" });
        String expected = "Stolen car          ;Johnny Thunder      |Fiscal fraud        ;Johnny Thunder      |Fiscal fraud        ;Sam Sinister        ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testOuterJoin() {
        Operator op = new Project(new OuterJoin(new Scan(relation, schema), new Rename(new Scan(relation2, schema2), "name", "person"), "id", "case"), new String[] { "name", "person" });
        String expected = "Stolen car          ;Johnny Thunder      |Fiscal fraud        ;Johnny Thunder      |Fiscal fraud        ;Sam Sinister        |High speed          ;null";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testCount() {       
        Operator op = new Project(new Count(new Select<String>(new Scan(relation, schema), "status", "open")), "Count");
        String expected = "2          ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testConcat() {       
        Operator op = new Project(new Concat(new Scan(relation, schema), "name", " / "), "Concat");
        String expected = "Stolen car / Fiscal fraud / High speed";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testGroupBy() {       
        Operator op = new Project(new Group(new Scan(relation, schema), "status", new Aggregator[] { new Count(null), new Concat(null, "name", ", ") }), new String[] { "status", "Count0", "Concat1" });
        String expected = "open                ;2          ;Stolen car, High speed|closed              ;1          ;Fiscal fraud";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testUnion() {       
        Operator op = new Group(new Union(new Select<String>(new Scan(relation, schema), "name", "Stolen car"), new Select<String>(new Scan(relation, schema), "status", "closed")), "id");
        String expected = "1                   ;Stolen car          ;open                |2                   ;Fiscal fraud        ;closed              ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testLimit() {
        Operator op = new Limit(new Scan(relation, schema), 1, 1);
        String expected = "2                   ;Fiscal fraud        ;closed              ";
        String actual = concatTuples(op);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testAutomaticFileCreating() {
        // copy and replace the original meta file
        try {
            Files.copy(Paths.get(TableMetadata.rootDirectory + "solutions/metaTest.txt"), Paths.get(TableMetadata.rootDirectory + "meta.txt"), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            System.out.println("AutomaticFileCreating: copying metaTest to meta failed!");
        }
        assertNotNull(TableMetadata.getMetadata("testPurpose"));
        // also test simple insertion
        int result = new Insert("testPurpose").append(new String[][]{{"TT"}});
        assertEquals(1, result);
        FileAssert.assertEquals(new File(TableMetadata.rootDirectory + "solutions/testPurpose.txt"), new File(TableMetadata.rootDirectory + "testPurpose.txt"));
    }
    
    @Test
    public void testTableCleanup() {
        // required to build the Metadata in the first place
        TableMetadata.getMetadata("case");
        // cleanup table case
        boolean actual = TableMetadata.cleanupTable("case");
        assertTrue(actual);
        FileAssert.assertEquals(new File(TableMetadata.rootDirectory + "solutions/caseCleanup.txt"), new File(TableMetadata.rootDirectory + "case.txt"));
    }
   
    @Test
    public void testNull() {
        // Insert of null as text && select of a null text:
        String expected = null;
        int result = new Insert("NameText").append(new String[][]{{"T_ins", expected}});
        assertEquals(1, result);
        Operator op = new Select<String>(new Scan("NameText"), "Name", "T_ins");
        String actual = "something";
        if (op.moveNext()) actual = op.current().getText(1);
        assertEquals(expected, actual);
        actual = Tuple.getText(op.current().get(1), "NameText");
        assertEquals(expected, actual);
        
        // Update:
        op = new Select<String>(new Scan("case"), "CID", "6");
        HashMap<String, String> ColVal = new HashMap<String, String>();
        ColVal.put("Location", null);
        int num = new Update("case").update(op, ColVal);
        assertEquals(num, 1);
        FileAssert.assertEquals(new File(TableMetadata.rootDirectory + "solutions/testNullUpdate.txt"), new File(TableMetadata.rootDirectory + "case.txt"));
    }

	/**
	 * Concatenates all tuples returned by the operator. The tuples are separated
	 * by a simple space.
	 * @param op operator to read from
	 * @return concatenated tuples
	 */
	private String concatTuples(Operator op) {
		StringBuilder buf = new StringBuilder();
		while (op.moveNext()) {
			buf.append(op.current().toString());
			buf.append("|");
		}
		if (buf.length()>=1)
		buf.deleteCharAt(buf.length() - 1);
		return buf.toString();
	}
	
	/*
	private String concatTuplesGetText(Operator op, String[][] Texts) {
        StringBuilder buf = new StringBuilder();
        int i = 0;
        int j = 0;
        while (op.moveNext()) {
            buf.append(op.current().toString());
            buf.append("|");
            for (int k: op.current().getSchema().getLinkedIndices()) {
                Texts[i][j] = op.current().getText(k);
                j++;
            }
            i++;
        }
        if (buf.length()>=1)
        buf.deleteCharAt(buf.length() - 1);
        return buf.toString();
    }
    */
}
