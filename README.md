# Readme - Code Gliederung #

### Eclipse Projekt Gliederung ###
Wir haben das 2. Projekt in das erste integriert, indem wir alle Sources vom 2. Projekt in ein Packet dbproject2.model verschoben haben. Das DatastoreInterface vom 1. Projekt haben wir ersetzt. Das mit MySQL funktionierende ist auch noch entsprechend beschriftet vorhanden, wird jedoch nicht mehr gebraucht.


### Libraries ###
Für die jUnitTests werden abgesehen von junit auch die beigefügten libraries junit-addons und Apache commons-io gebraucht. junit-addon um mit einem Assert Files zu vergleichen und das commons-io um ganze Ordner zu kopieren und zu löschen.


### jUnitTests ###
Die TestCases werden jeweils auf einer temporären Kopie vom Ordner test_db laufen gelassen, der zwischen jedem Test gelöscht und neu erstellt wird. Somit spielt die Reihenfolge der Tests auch keine Rolle. Damit das Betriebssystem nachkommt, wird der Vorgang verzögert, was die hohen Testlaufzeiten erklärt. Die Ausführung der Operatoren selbst dauert im Normalfall nur wenige Millisekunden. Wenn ein Test fehl schlägt kann es sein(meist bei Runtime Errors), dass der temporäre Ordner "workingDirectory_tests/" nicht gelöscht werden kann und somit folgende Tests unter Umständen auch fehlschlagen können. In dem Fall muss der temporäre Ordner vor der nächsten Ausführung der Tests unter Umständen noch gelöscht werden.


### DB Files ###
In der Applikation welche die DB nutzen möchte muss der Pfad zur DB gesetzt werden. So beispielsweise im Konstruktor von DatastoreInterface: TableMetadata.rootDirectory = "db/"; oder analog im Part2Test.java TableMetadata.rootDirectory = "workingDirectory_tests/test_db/";

meta.txt enthält alle Angaben zum DB Schema. Die Metadaten sowie der Unterordner texts reichen aus, um eine leere DB zu initialisieren. (Im Falle von unserer Website müssen jedoch die categories und maincategories bereits in der DB vorhanden sein.) Jede Tabelle erhält ein nach ihr benanntes DB File, z.bsp. case.txt und falls es referenzierte Typen enhält im Unterordner texts auch noch ein oder mehrere Files welche die referenzierten Objekte enthält.

Generell werden Felder mit fixer Länge mit Leerschlägen gefüllt um die richtige Länge zu haben.

#### Aufbau des Meta Files ####
Das Metafile sollte ANSI codiert sein (oder UTF-8 ohne BOM). Wobei Umlaute oder andere UTF-8 Zeichen die mehr als 1 Byte lang sind zu Problemen führen und hier nicht verwendet werden dürfen. Für jede Tabelle wird eine Zeile erstellt. Die Zeile ist mit einem vertikalen Trennstrich | zwischen jedem der Aufzählungspunkte wie folgt gegliedert:

* Tabellen Name (keine Leerzeichen vor dem | einfügen)
* Anzahl Page welche die Tabelle hat (fixe Länge 10 Chars)
* Letzte Tupel Nummer auf der letzten Page (fixe Länge 10 Chars)
* Byte Position im (letzten) Text File wo die referenzierten Objekte gespeichert werden (fixe Länge 11 Chars)
* Letzte Text File Nummer, mit 0 beginnend (fixe Länge 11 Chars)
* Alle weiteren mit | getrennten Felder beschreiben eine Kolumne in der Tabelle. Eine Kolumne ist wie folgt aufgebaut:
    * Format (fixe Länge 1 Char)
        * 1 = string (mit fixer Länge)
        * 2 = integer
        * 3 = boolean (0 oder 1)
        * 4 = DateTime
        * 5 = Text (Referenz Type)
        * 6 = Auto Increment Integer (max. 1 pro Tabelle)
    * String Länge / Auto Increment Fortschritt: ist -1 ausser für Format 1 und 6 bei denen es die entsprechende Zahl speichert. (Fixe Länge 10 Chars)
  * Kolumnen Namen (variable Länge, Rest vom Feld bis zum nächsten |)

Beispiel einer Zeile:

    case|1         |1         |516        |0          |61         CID|4-1        DateTime|190        Location|5-1        Description|3-1        Status|190        Summary

#### Aufbau eines normalen DB Files ####
* Alle Tupel werden nacheinander aufgereiht. Jede Spalte eines Tupels hat die seinem Format entsprechende fixe Länge an Bytes.
    * String: seine 2*(fixe Länge die im Meta File spezifiziert ist) -> siehe "Limitations"
    * Integer: 11 Byte (inkl. möglichem - davor)
    * Booelan: 1 Byte (0 oder 1)
    * DateTime: 14 Byte `yyyymmddhhmmss`
    * Text: Total 33 Byte, Referenz in der Form File Number (11 Byte), Position im File (11 Byte), Länge des referenzierten Textes (11 Byte)
* Vorangehend vor allen Feldern eines Tupels ist ein "Valid-Byte": 0 für gelöscht, 1 für gültig um zu kennzeichnen ob das Tupel als gelöscht markiert ist oder nicht.
* Geschrieben werden UTF-8 Charakter. Das File beginnt mit einem BOM.
* Falls der erste Charakter einer Spalte eines Tupels NUL ist, dann gilt das ganze Feld als NULL.

##### Paging #####
* Im Operator.java ist die Page Grösse als Anzahl Bytes definiert.
* Wenn das nächste Tupel die Page Grösse überschreiten würde, wird der Rest der Page mit Leerschlägen gefüllt bis auf den letzten Charakter der `|` ist. Dann wird direkt folgend mit der nächsten Page im gleichen File begonnen.
#### Text Files ####
* Ebenfalls UTF-8 codiert mit einem vorangehenden BOM.
* Jedes referenzierte Objekt folgt direkt dem Vorangehenden ohne irgendwelche Trennung.
* Die "ungefähre" maximale Text File Grösse wird in Operator.java gesetzt. Wenn ein Objekt geschrieben werden sollte und das letzte Text File schon die spezifizierte Grösse erreicht (oder überschritten) hat, wird es in einem neuen File geschrieben.

### DB Funktionalität ###
#### Statements ####
* Delete nimmt einen Operator als Argument löscht beim Aufruf von deleteAll() alle Tupel, die vom den Operator zurückgegeben werden (setzt das Valid-Byte zu 0). Die Tupel, welche Delete aus dem Operator liest dürfen nur aus einer Tabelle stammen (also nicht gejoint sein und auch keine Spalte mit Aggregatoren enthalten).
* Insert nimmt den Tabellen Namen als Konstruktor Argument. Die Methode append nimmt entweder einen Operator mit Tupeln die eingefügt werden sollen oder einen zweidimensionalen String-Array, welcher alle Spaltenwerte der einzufügenden Tuples beschreibt. Mit letzterem können also mit einem append mehrere neue Einträge hinzugefügt werden.
* Update nimmt analog zu Insert ebenfalls den Tabellen Namen als Argument. Die Methode update nimmt jedoch einen Operator und eine HashMap. Der Operator beschreibt welche Tupel aktualisiert werden müssen. Die HashMap enthält die Einträge <Spaltenname, Wert> welche beschreiben was im Tupel überschrieben werden soll.
#### Operators ####
Ähnlich aufgebaut sind die Operatoren welche für SQL-Select ähnliche Anfragen genutzt werden oder als Argument für ein Statement / einen anderen Operator.

* Jeder Operator besitzt einen Pointer auf die nächst tiefere Ebene sowie das derzeit verwendete Schema. Das Schema muss im Konstruktor berechnet werden.
* Jeder Operator muss mindestens die computeNext()-Funktion bereitstellen. Diese wird implizit in einem Thread aufgerufen und die Ergebnisse werden in einer Liste abgespeichert. Wird moveNext() auf den Operator angewendet, wird das erste Element aus der Liste entfernt und auf current abgespeichert. Vorallem bei Joins wirkt sich dies positiv auf die Laufzeit aus, da der zweite Operator bereits Vorarbeit leisten kann, während der erste noch am hashen ist. die start() Methode aktiviert rekursiv alle verschachtelten Threads und wird automatisch beim ersten aufruf von moveNext() aufgerufen.
* Die meisten Operatoren sind selbsterklärend. Der GroupBy Operator ist etwas komplexer. Neben dem Operator übergibt man eine Spalte, welche als Gruppierungsargument benutzt wird. Damit übernimmt es die Funktion eines Distincts. Zusätzlich kann noch ein Array von Aggregatsfunktionen mitgegeben werden, von denen jeder einzeln auf alle entstehenden Gruppen angewendet werden.

#### Tuple, Tuple Schema, Kolumne, Metadaten ####
* Für jede Tabelle wird ein TableMetadata Objekt erstellt welches die Metadaten speichert.
* Dazu wird ein Tuple Schema erstellt welches mit seinem Array an Column Objekten alle Spalten beschreibt welche ein Tupel in solch einer Tabelle hat.
* Jedes Tupel hat ein Tupel Schema. Nach einem Scan entspricht dies genau dem Schema von TableMetadata. Die Operatoren die ein Scan als Argument nehmen erstellen jedoch z.T. neue Schemas falls nötig. (z.B. Join vermischt zwei Schemas, Aggregatsfunktionen fügen eine neue Spalte hinzu, etc.)
* TableMetadata bietet auch die Methode cleanupTable welche als Argument einen Tabellen Namen nimmt. Alle Files der Tabelle werden neu geschrieben und alle erst durch das Valid-Byte gelöschte Tupel dabei endgültig entfernt. Die Datenbank führt dies von Zeit zu Zeit automatisch aus. WICHTIG: Es dürfen gleichzeitig keine anderen Operationen auf dieser Tabelle laufen.
